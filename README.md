GitHub Plugin
===============

[![CircleCI](https://circleci.com/bb/gradle-universum/github.svg?style=shield)](https://circleci.com/bb/gradle-universum/github)
[![Codecov](https://codecov.io/bb/gradle-universum/github/branch/main/graph/badge.svg)](https://codecov.io/bb/gradle-universum/github)
[![Codacy](https://api.codacy.com/project/badge/Grade/cd1dad31b351467db8be4b3409fdc01c)](https://www.codacy.com/app/universum-studios/github?utm_source=gradle-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=gradle-universum/github&amp;utm_campaign=Badge_Grade)
[![Gradle](https://img.shields.io/badge/gradle-6.3-blue.svg)](https://gradle.com)

## ! OBSOLETE ! ##

**> This project has become obsolete and will be no longer maintained. <**

---

Gradle plugin that may be used for automated publishing of releases along with their associated artifacts to the GitHub cloud.

For more information please visit the **[Wiki](https://bitbucket.org/gradle-universum/github/wiki)**.

## Apply ##

    buildscript {
        dependencies {
            classpath "universum.studios.gradle:github-plugin:${DESIRED_VERSION}"
        }
    }
    
    >>>

    apply plugin: 'universum.studios.github'

## [License](https://bitbucket.org/gradle-universum/github/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Plugin which may be used for management of some aspects of a project which is hosted on a GitHub
 * server.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@Suppress("unused")
class GitHubPlugin : Plugin<Project> {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Unique identifier of the GitHub Release plugin.
         */
        const val ID = "universum.studios.github"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Primary extension used as entry point for this plugin.
     */
    private lateinit var extension: GitHubExtension

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     */
    override fun apply(project: Project) {
        this.extension = project.extensions.create(GitHubExtension.NAME, GitHubExtension::class.java, project)
        this.extension.dispatchPluginApplied()
    }

    /*
     * Inner classes ===============================================================================
     */
}
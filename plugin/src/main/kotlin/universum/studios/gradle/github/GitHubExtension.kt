/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github

import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.plugins.ExtensionContainer
import universum.studios.gradle.github.extension.BaseProjectExtension
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.label.extension.LabelsExtension
import universum.studios.gradle.github.label.service.api.LabelsApi
import universum.studios.gradle.github.label.service.api.LabelsApiProvider
import universum.studios.gradle.github.release.extension.ReleasesExtension
import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.release.service.api.ReleasesApiProvider

/**
 * Primary extension used by [GitHubPlugin] as its entry point.
 *
 * This extension hots the following nested extensions that may be used to configure GitHub release
 * tasks provided by this plugin:
 *
 * @param project The project to which is the plugin applied.
 * @constructor Creates a new instance of GitHubExtension with the specified *project*.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class GitHubExtension(val project: Project) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Name of the closure block defining properties for [GitHubExtension].
         */
        const val NAME = "github"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Container holding all child extensions created by this parent extension.
     */
    private lateinit var extensions: ExtensionContainer

    /**
     * List of extensions which are dependent on the current project, and also on properties of
     * [repository] along with [defaultConfig] extensions.
     */
    private val projectDependentExtensions = mutableListOf<BaseProjectExtension>()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Dispatches a message informing that the plugin has been applied to the project.
     */
    fun dispatchPluginApplied() {
        this.extensions = (this as ExtensionAware).extensions
        this.extensions.create(RepositoryExtension.NAME, RepositoryExtension::class.java)
        this.extensions.create(DefaultConfigExtension.NAME, DefaultConfigExtension::class.java)
        this.projectDependentExtensions.add(extensions.create(ReleasesExtension.NAME, ReleasesExtension::class.java))
        this.projectDependentExtensions.add(extensions.create(LabelsExtension.NAME, LabelsExtension::class.java))
        this.projectDependentExtensions.forEach { it.dispatchPluginApplied(project) }
        this.dispatchRepositoryChange()
        this.dispatchDefaultConfigChange()
        this.project.afterEvaluate({ onProjectEvaluated() })
    }

    /**
     * Specifies a repository which should be used across the plugin.
     *
     * @param repository Repository to be used.
     */
    internal fun repository(repository: RepositoryExtension) {
        repository().copyFrom(repository)
        this.dispatchRepositoryChange()
    }

    /**
     * Dispatches change of the [repository] extension to all project dependent extensions.
     */
    private fun dispatchRepositoryChange() {
        this.projectDependentExtensions.forEach { it.setRepository(repository()) }
    }

    /**
     * Specifies a default configuration which should be used across the plugin.
     *
     * @param defaultConfig Default configuration to be used.
     */
    internal fun defaultConfig(defaultConfig: DefaultConfigExtension) {
        defaultConfig().copyFrom(defaultConfig)
        this.dispatchDefaultConfigChange()
    }

    /**
     * Dispatches change of the [defaultConfig] extension to all project dependent extensions.
     */
    private fun dispatchDefaultConfigChange() {
        this.projectDependentExtensions.forEach { it.setDefaultConfig(defaultConfig()) }
    }

    /**
     * Returns the repository of the plugin.
     *
     * @return Extension containing repository identifying properties for the plugin.
     */
    fun repository():RepositoryExtension = extensions.findByType(RepositoryExtension::class.java)!!

    /**
     * Returns the default extension of the plugin.
     *
     * @return Extension containing default configuration properties for the plugin.
     */
    fun defaultConfig():DefaultConfigExtension = extensions.findByType(DefaultConfigExtension::class.java)!!

    /**
     * Returns the releases extension which is responsible for GitHub releases related tasks.
     *
     * @return Releases extension hosted in this extension.
     */
    fun releases():ReleasesExtension = extensions.findByType(ReleasesExtension::class.java)!!

    /**
     * Returns the labels extension which is responsible for GitHub issue labels related tasks.
     *
     * @return Labels extension hosted in this extension.
     */
    fun labels():LabelsExtension = extensions.findByType(LabelsExtension::class.java)!!

    /**
     * Invoked whenever the project to which is the parent plugin applied has been evaluated.
     */
    private fun onProjectEvaluated() {
        val repository = repository()
        repository.registerApi(ReleasesApi::class, ReleasesApiProvider.getApi(repository))
        repository.registerApi(LabelsApi::class, LabelsApiProvider.getApi(repository))
        this.projectDependentExtensions.forEach { it.dispatchProjectEvaluated() }
    }

    /*
     * Inner classes ===============================================================================
     */
}
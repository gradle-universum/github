/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

/**
 * Child extension of the primary plugin extension allowing to specify default properties that may
 * be used  by all plugin extensions.
 *
 * @constructor Creates a new instance of DefaultConfigExtension.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class DefaultConfigExtension {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /**
     */
    companion object Contract {

        /**
         * Name of the closure block defining properties for [DefaultConfigExtension].
         */
        const val NAME = "defaultConfig"
    }

    /*
     * Members =====================================================================================
     */

    /**
     * Default id for all artifacts to be uploaded as assets for GitHub releases.
     *
     * This id is used to build name for artifacts to be uploaded.
     */
    var artifactId: String? = null

    /**
     * Default version for all artifacts to be uploaded as assets for GitHub release.
     *
     * This version is used to build name for artifacts to be uploaded.
     */
    var artifactVersion: String? = null

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Copies all the property values of the given *defaultConfig* to this configuration.
     *
     * @param defaultConfig The default config from which to copy property values.
     */
    internal fun copyFrom(defaultConfig: DefaultConfigExtension) {
        this.artifactId = defaultConfig.artifactId ?: artifactId
        this.artifactVersion = defaultConfig.artifactVersion ?: artifactVersion
    }

    /*
     * Inner classes ===============================================================================
     */
}
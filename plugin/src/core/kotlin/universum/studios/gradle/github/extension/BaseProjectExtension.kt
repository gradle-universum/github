/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import org.gradle.api.Project

/**
 * Extension that is used as base for all plugin extensions that need to have access to the **project**
 * to which is the GitHub plugin applied. This base extension class provides convenient dispatching
 * methods that are used to inform inheritance hierarchies about some of important events related
 * to the project like when plugin has been **applied** to the project or when the project has
 * been **evaluated**.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class BaseProjectExtension {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Extension with properties that identify a concrete GitHub repository on which should this
     * extension operate.
     */
    private var repository: RepositoryExtension = RepositoryExtension()

    /**
     * Extension with default configuration used across all project dependent extensions.
     *
     * Initialized when [setDefaultConfig] has been called for this extension.
     */
    private var defaultConfig: DefaultConfigExtension = DefaultConfigExtension()

    /**
     * Project to which is the GitHub plugin applied.
     *
     * Initialized when [dispatchPluginApplied] has been called for this extension.
     */
    private var project: Project? = null

    /**
     * Boolean flag indicating whether the project to which is this extension attached has been
     * already evaluated or not.
     */
    private var projectEvaluated = false

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Dispatches a message to inform this extension that the plugin that uses this extension has
     * been applied to the specified *project*.
     *
     * @param project The project to which has been the parent plugin applied.
     * @throws IllegalStateException If this extension is already attached to some project.
     */
    internal fun dispatchPluginApplied(project: Project) {
        if (this.project != null) {
            throw IllegalStateException("Already attached to project.")
        }
        this.project = project
        onPluginApplied(project)
    }

    /**
     * Invoked when the parent plugin using this extension has been applied to the specified *project*.
     *
     * This is invoked only once when this extension is not attached to any project yet.
     *
     * @param project The project to which has been the plugin applied.
     */
    protected open fun onPluginApplied(project: Project) {
        // Inheritance hierarchies may perform here operations related to event when the parent
        // plugin has been applied to the project.
    }

    /**
     * Checks whether this extension is already attached to any project.
     *
     * @return *True* if this extension is attached to project, *false* otherwise.
     */
    internal fun isAttachedToProject() = project != null

    /**
     * Returns the project to which is this extension attached.
     *
     * @return The attached project.
     * @throws IllegalStateException If this extension is not attached to any project.
     */
    internal fun getProject() = project ?: throw IllegalStateException("Not attached to any project!")

    /**
     * Dispatches a message to inform this extension that the project to which is the parent plugin
     * applied has been evaluated.
     */
    internal fun dispatchProjectEvaluated() {
        this.project ?: throw IllegalStateException("Extension is not attached to project!")
        this.projectEvaluated = true
        onProjectEvaluated(project!!)
    }

    /**
     * Invoked whenever the project to which is the parent plugin applied has been evaluated.
     *
     * This is invoked whenever [dispatchPluginApplied] has been called upon this extension.
     *
     * @param project The evaluated project to which is the parent plugin applied.
     *
     * @see Project.afterEvaluate
     */
    protected open fun onProjectEvaluated(project: Project) {
        // Inheritance hierarchies may perform here operations related to event when the project
        // to which is the parent plugin applied has been evaluated.
    }

    /**
     * Checks whether the project to which is this extension attached has been already evaluated.
     *
     * @return *True* if the project has been evaluated, *false* otherwise.
     *
     * @see onProjectEvaluated
     */
    protected fun isProjectEvaluated() = projectEvaluated

    /**
     * Sets a repository extension to be used by this dependent extension and all its child extensions.
     *
     * @param repository
     */
    internal fun setRepository(repository: RepositoryExtension) {
        this.repository = repository
        onRepositoryChanged(this.repository)
    }

    /**
     * Invoked whenever repository has been changed for this extension via [setRepository].
     *
     * Inheritance hierarchies should propagate the changed repository to all theirs child extensions
     * as needed.
     *
     * @param repository The changed repository.
     */
    internal open fun onRepositoryChanged(repository: RepositoryExtension) {
        // Inheritance hierarchies may dispatch the default configuration to theirs child extensions.
    }

    /**
     * Returns the repository attached to this extension.
     *
     * The repository is valid after [onRepositoryChanged] has been called for this extension.
     *
     * @return The attached repository.
     *
     * @see setRepository
     */
    internal fun getRepository() = repository

    /**
     * Sets an extension with default configuration to be used by this extension and all its child
     * extensions.
     *
     * @param defaultConfig The default configuration extension.
     *
     * @see getDefaultConfig
     */
    internal fun setDefaultConfig(defaultConfig: DefaultConfigExtension) {
        this.defaultConfig = defaultConfig
        onDefaultConfigChanged(defaultConfig)
    }

    /**
     * Invoked whenever default configuration has been changed for this extension via [setDefaultConfig].
     *
     * Inheritance hierarchies should propagate the changed default config to all theirs child
     * extensions as needed.
     *
     * @param defaultConfig The extension holding default configuration for this extension.
     */
    internal open fun onDefaultConfigChanged(defaultConfig: DefaultConfigExtension) {
        // Inheritance hierarchies may dispatch the default configuration to theirs child extensions.
    }

    /**
     * Returns the default configuration attached to this extension.
     *
     * The configuration is valid after [onDefaultConfigChanged] has been called for this extension.
     *
     * @return Extension holding default configuration for this extension.
     *
     * @see setDefaultConfig
     */
    internal fun getDefaultConfig() = defaultConfig

    /*
     * Inner classes ===============================================================================
     */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.util

import org.gradle.api.GradleException

/**
 * An exception that is used as parent for all exceptions thrown by the GitHub Plugin.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param message Name of the extension containing the property of which value is missing.
 * @constructor Creates a new instance of PluginException with the specified *message* prefixed by
 * name of the GitHub Plugin.
 */
open class PluginException(message: String) : GradleException("GitHubPlugin => $message")
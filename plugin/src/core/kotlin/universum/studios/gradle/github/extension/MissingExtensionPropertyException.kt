/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import universum.studios.gradle.github.util.PluginException

/**
 * An exception that is used to inform about fact that a required property is missing for a specific
 * **plugin extension**.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param extensionPath Path to the extension containing the property of which value is missing.
 * @param propertyName Name of the property for which is the value missing.
 * @constructor Creates a new instance of MissingExtensionPropertyException for the specified
 * *extensionPath* and *propertyName*.
 */
class MissingExtensionPropertyException(extensionPath: String, propertyName: String) : PluginException(
        "Missing value for '$extensionPath.$propertyName' property."
)
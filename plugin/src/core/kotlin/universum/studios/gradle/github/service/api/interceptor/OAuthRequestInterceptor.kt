/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * An [Interceptor] implementation which is used to add **Authorization** header with **OAuth** token
 * as header for all API requests. The OAuth token may be specified via [setToken].
 *
 * @author Martin Albedinsky
 */
class OAuthRequestInterceptor : Interceptor {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    private companion object {

        /**
         * Name of the **Authorization** header.
         */
        const val HEADER_NAME = "Authorization"

        /**
         * Format for value of the **Authorization** header.
         */
        const val HEADER_VALUE_FORMAT = "token %s"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * OAuth token specified via [setToken]. If not *null*, the request passed to [intercept] is
     * intercepted and the **Authorization** header is added to it with value of this token.
     */
    private var token: String? = null

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets a value of the OAuth token to be added into intercepted request as **Authorization** header.
     *
     * @param token The token to be used. If *null*, no interception will be performed.
     */
    fun setToken(token: String?) {
        this.token = token
    }

    /*
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        return if (token.isNullOrEmpty()) {
            chain.proceed(chain.request())
        } else {
            chain.proceed(chain.request().newBuilder().header(HEADER_NAME, String.format(HEADER_VALUE_FORMAT, token)).build())
        }
    }

    /*
     * Inner classes ===============================================================================
     */
}
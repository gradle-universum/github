/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task.specification

import org.gradle.api.Task

/**
 * Class describing a simple task used by the GitHub Release plugin.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property type Type of the task.
 * @property name Name of the task.
 * @property description Description of the task.
 * @property dependsOn List of the tasks that the plugin task depends on.
 * @constructor Creates a new instance of TaskSpecification with the specified name and description.
 */
data class TaskSpecification<T : Task> (
        val group: String = TaskSpecification.GROUP,
        val type: Class<T>,
        val name: String,
        val description: String = "",
        val dependsOn: List<Task> = emptyList()) {

    /**
     */
    companion object Contract {

        /**
         * Name of the **group** property of a task.
         */
        const val TASK_GROUP = "group"

        /**
         * Name of the **type** property of a task.
         */
        const val TASK_TYPE = "type"

        /**
         * Name of the **name** property of a task.
         */
        const val TASK_NAME = "name"

        /**
         * Name of the **description** property of a task.
         */
        const val TASK_DESCRIPTION = "description"

        /**
         * Name of the **dependsOn** property of a task.
         */
        const val TASK_DEPENDS_ON = "dependsOn"

        /**
         * Name of the group where all Gradle tasks provided by the GitHub plugin are placed by
         * default.
         */
        const val GROUP = "github"
    }
}
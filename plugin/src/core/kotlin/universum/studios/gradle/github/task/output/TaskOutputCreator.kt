/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task.output

/**
 * Interface declaring layer for creators which may be used by tasks to create theirs specific outputs.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface TaskOutputCreator {

    /**
     * Clears the current input specified for this creator.
     */
    fun clear()

    /**
     * Creates output string representing the input specified for this creator.
     *
     * @return Output which may be logged for information purpose.
     */
    fun createOutput(): String
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import groovy.lang.MissingPropertyException
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.util.PluginException

/**
 * A [DefaultTask] implementation that is used as base for all tasks provided by the GitHub Release
 * plugin.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class BaseTask : DefaultTask() {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Repository for which should this task perform its related operation.
     */
    private var repository: RepositoryExtension? = null

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets a repository for which should this task perform its related operation.
     *
     * @param repository The desired repository.
     */
    internal fun setRepository(repository: RepositoryExtension) {
        this.repository = repository
    }

    /**
     * Returns the repository specified for this task.
     *
     * @return The attached repository.
     * @throws IllegalStateException If there is no repository attached.
     */
    internal fun getRepository() = repository ?: throw IllegalStateException("No repository attached!")

    /**
     * Performs operation specific for this task.
     */
    @TaskAction
    @Suppress("unused")
    fun perform() {
        val propertiesReport = onValidateProperties()
        when (propertiesReport.isSuccess()) {
            true -> onPerform()
            false -> throw PluginException("Missing values for '$name.${propertiesReport.namesOfMissingProperties()}' properties!")
        }
    }

    /**
     * Invoked whenever [perform] is called for this task but before [onPerform] in order to perform
     * validation of the property values specified for this task.
     *
     * Inheritance hierarchies may perform checks for theirs required properties but should always
     * call super implementation.
     * @return The report describing which properties are missing theirs corresponding values.
     */
    protected open fun onValidateProperties(): PropertyValuesReport {
        val report = PropertyValuesReport()
        repository ?: throw IllegalStateException("No repository specified!")
        return report
    }

    /**
     * Creates a new exception informing that a value for the property with the specified name is
     * missing for this task.
     *
     * @return Exception ready to be thrown.
     */
    protected fun missingPropertyValueException(propertyName: String) = MissingPropertyException("Missing value for '$propertyName' property.")

    /**
     * Invoked due to call to [perform] in order to perform operation specific for this task. As of
     * now this task has access to the plugin's primary extension.
     */
    protected abstract fun onPerform()

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Simple object used by [BaseTask] for validation of property values before the task specific
     * operation is performed.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    class PropertyValuesReport {

        /**
         * List containing property names for which are missing theirs corresponding values.
         */
        private val missingNames = mutableSetOf<String>()

        /**
         * Reports that value for the property with the specified *propertyName* is missing.
         *
         * @param propertyName Name of the property of which value is missing.
         * @return This report to allow methods chaining.
         */
        fun missingValueFor(propertyName: String): PropertyValuesReport {
            missingNames.add(propertyName)
            return this
        }

        /**
         * Checks whether this report is a successful report.
         *
         * @return *True* if there are no missing property values reported, *false* otherwise.
         */
        fun isSuccess() = missingNames.isEmpty()

        /**
         * Returns the names of the missing properties reported.
         *
         * @return Names of the missing properties.
         */
        internal fun namesOfMissingProperties() = missingNames
    }
}
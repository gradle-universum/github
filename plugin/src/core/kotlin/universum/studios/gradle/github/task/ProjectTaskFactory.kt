/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import org.gradle.api.Project
import org.gradle.api.Task
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * Simple static factory that may be used to create [Task] instances for a specific [Project].
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see [ProjectTaskFactory.createTask]
 */
class ProjectTaskFactory private constructor() {

    /**
     */
    companion object {

        /**
         * Creates and returns a new task for the given *project* constructed from the specified
         * *taskSpecification*.
         *
         * @param project The project for which to create the desired task.
         * @param taskSpecification Specification for the desired task describing with which properties
         * should be the task created.
         * @return The created task.
         */
        fun <T : Task> createTask(project: Project, taskSpecification: TaskSpecification<T>): T {
            return taskSpecification.type.cast(project.tasks.create(LinkedHashMap<String, Any>().apply {
                put(TaskSpecification.TASK_GROUP, taskSpecification.group)
                put(TaskSpecification.TASK_TYPE, taskSpecification.type)
                put(TaskSpecification.TASK_NAME, taskSpecification.name)
                put(TaskSpecification.TASK_DESCRIPTION, taskSpecification.description)
                put(TaskSpecification.TASK_DEPENDS_ON, taskSpecification.dependsOn)
            }))
        }
    }
}
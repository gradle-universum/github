/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.data.model

/**
 * Interface that may be used to implement binders responsible for data binding of one model instance
 * to another one. The model instances may be of different types and structures. The binding logic
 * is left for a specific binder implementations.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface ModelBinder<in FromModel, ToModel> {

    /**
     * Binds current data of the given *from* model instance to the given *to* model instance.
     *
     * **Note** that if the specified models are collections, they should have the same count
     * of model instances presented.
     *
     * @param from The desired model of which data to bind to the second model.
     * @param to   The desired model to which to bind data from the first model.
     * @return The second model instance with data bound from the first model. This may be a copy
     * of the second model.
     */
    fun bind(from: FromModel, to: ToModel) : ToModel
}
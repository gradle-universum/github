/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service

import java.net.URL

/**
 * Utility class which may be used for operations with url-s.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
class UrlUtils private constructor() {

    /**
     */
    companion object {

        /**
         * Format for base url.
         */
        const val BASE_URL_FORMAT = "%s://%s/"

        /**
         * Parses base url of the specified *url*.
         *
         * @param url The url of which base url to obtain.
         * @return Extracted base url from the specified url. May be empty if the specified url is
         * also empty.
         */
        fun parseBaseUrl(url: String): String {
            if (url.isEmpty()) return ""
            val urlParser = URL(url)
            return String.format(BASE_URL_FORMAT, urlParser.protocol, urlParser.host)
        }
    }
}
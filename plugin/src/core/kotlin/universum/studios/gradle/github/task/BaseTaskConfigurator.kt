/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.task.configuration.TaskConfigurator

/**
 * A [TaskConfigurator] implementation that may be used as base for concrete task configurator
 * implementations. This configurator implementation does not perform any task configuration.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BaseTaskConfigurator.
 */
open class BaseTaskConfigurator<T : BaseTask> : TaskConfigurator<T> {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Repository on which should all task configured via [configureTask] perform theirs related
     * operations.
     */
    private var repository: RepositoryExtension = RepositoryExtension()

    /**
     * Default configuration to be attached to all tasks passed to [configureTask].
     */
    private var defaultConfig: DefaultConfigExtension = DefaultConfigExtension()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun setRepository(repository: RepositoryExtension) {
        this.repository = repository
    }

    /**
     * Returns the repository specified for this configurator.
     *
     * @return The attached repository.
     *
     * @see setRepository
     */
    fun getRepository() = repository

    /*
     */
    override fun setDefaultConfig(defaultConfig: DefaultConfigExtension) {
        this.defaultConfig = defaultConfig
    }

    /**
     * Returns the default configuration specified for this configurator.
     *
     * @return The attached default configuration.
     *
     * @see setDefaultConfig
     */
    fun getDefaultConfig() = defaultConfig

    /**
     */
    override fun configureTask(task: T): T {
        task.setRepository(repository)
        return task
    }

    /*
     * Inner classes ===============================================================================
     */
}
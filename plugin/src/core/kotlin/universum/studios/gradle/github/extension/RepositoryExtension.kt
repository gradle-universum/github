/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import universum.studios.gradle.github.service.api.Api
import kotlin.reflect.KClass

/**
 * Child extension of the primary plugin extension allowing to specify properties to identify a
 * concrete GitHub repository on which should the plugin operate.
 *
 * @author Martin Albedinsky
 * @since 1.1
 */
open class RepositoryExtension {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /**
     */
    companion object Contract {

        /**
         * Name of the closure block defining properties for [RepositoryExtension].
         */
        const val NAME = "repository"
    }

    /*
     * Members =====================================================================================
     */

    /**
     * Property holding name of the GitHub account owner (user or organization name) that should be
     * used by the plugin and its extensions.
     */
    var owner = ""

    /**
     * Property holding name of the GitHub repository that should be used by the plugin and its extensions.
     */
    var name = ""

    /**
     * Property holding access token to the GitHub API that should be used by the plugin and its
     * extensions for authorized requests to the GitHub API.
     */
    var accessToken = ""

    /**
     * Registry which contains API implementations mapped to theirs parent interfaces used across
     * the GitHub plugin.
     */
    private val apiRegistry = hashMapOf<Int, Api>()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the simple path describing relation between owner and name of this repository.
     *
     * @return Path created as 'OWNER/NAME'.
     */
    fun path() = "$owner/$name"

    /**
     * Registers an api implementation to be used across the GitHub plugin.
     *
     * If there is already registered api for the same class, the previous one will be replaced by
     * the given one.
     *
     * @param apiClass Class of the api to which to map the given api.
     * @param api The api to be registered.
     */
    internal fun <T : Api> registerApi(apiClass: KClass<T>, api: T) {
        this.apiRegistry[apiClass.java.hashCode()] = api
    }

    /**
     * Returns the api implementation of the specified class, registered for this repository.
     *
     * @param apiClass Class of the api to find.
     * @return Found api if its implementation is registered, *null* otherwise.
     */
    @Suppress("UNCHECKED_CAST")
    internal fun <T : Api> findApi(apiClass: KClass<T>): T? {
        val api = apiRegistry[apiClass.java.hashCode()]
        return if (api == null) null else api as T
    }

    /**
     * Copies all the property values of the given *repository* to this repository.
     *
     * @param repository The repository from which to copy property values.
     */
    internal fun copyFrom(repository: RepositoryExtension) {
        this.owner = if (repository.owner.isNotEmpty()) repository.owner else owner
        this.name = if (repository.name.isNotEmpty()) repository.name else name
        this.accessToken = if (repository.accessToken.isNotEmpty()) repository.accessToken else accessToken
        this.apiRegistry.putAll(repository.apiRegistry)
    }

    /*
     * Inner classes ===============================================================================
     */
}
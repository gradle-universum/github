/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api

import retrofit2.Response
import universum.studios.gradle.github.util.PluginException

/**
 * An exception that is used to inform about fact that a specific API call has failed.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param response The error response received from the server as result of API request.
 * @constructor Creates a new instance of ApiCallException with message composed for the specified
 * error *response*.
 */
class ApiCallException(response: Response<*>) : PluginException(message(response))

/**
 * Creates a message for [ApiCallException] describing the given *response*.
 *
 * @return Formatted message text with appropriate values from the response.
 */
private fun message(response: Response<*>): String {
    val rawResponse = response.raw()
    val errorBody = response.errorBody()
    val error = "${rawResponse.protocol.toString().replace("/", " ").toUpperCase()} ${response.code()} ${response.message()}\n\n${errorBody!!.string()}"
    return "GitHub API call '${rawResponse.request.url}' failed with an error:\n$error"
}
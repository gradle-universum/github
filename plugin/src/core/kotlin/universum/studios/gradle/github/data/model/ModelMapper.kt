/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.data.model

/**
 * Interface that may be used to implement mappers responsible for transforming one model instance
 * to another one. The model instances may be of different types and structures. The mapping logic
 * is left for a specific mapper implementations.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface ModelMapper<in FromModel, out ToModel> {

    /**
     * Maps the given *from* model instance to the model type specific for this mapper.
     *
     * @param from The desired model to be mapped. If this is a collection of model instances the
     * transformed collection should have the same count of models.
     * @return New model instance type of specific for this mapper with transformed data of the
     * given *model*.
     */
    fun map(from: FromModel): ToModel
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api

import universum.studios.gradle.github.extension.RepositoryExtension

/**
 * Interface representing basic api layer.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Api {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object Contract {

        /**
         * Base url for the GitHub API.
         */
        const val BASE_URL = "https://api.github.com/"
    }

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the repository for which this api provides service calls.
     *
     * @return This api's repository.
     */
    fun getRepository(): RepositoryExtension
}
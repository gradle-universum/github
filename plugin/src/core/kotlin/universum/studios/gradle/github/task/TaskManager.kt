/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import org.gradle.api.Project
import org.gradle.api.Task
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.task.configuration.TaskConfigurator
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * Simple manager which may be used to create tasks for a desired project and also to perform basic
 * configuration for those tasks using [TaskConfigurator].
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property project The project for which will be the new manager creating and configuring tasks.
 * @constructor Creates a new instance of TaskManager for the specified *project*.
 */
open class TaskManager(val project: Project) {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Repository specified for this manager to be used by all [TaskConfigurator]s registered for
     * this manager via [registerTaskConfigurator].
     */
    private var repository: RepositoryExtension = RepositoryExtension()

    /**
     * Default configuration specified for this manager to be used by all [TaskConfigurator]s registered
     * for this manager via [registerTaskConfigurator].
     */
    private var defaultConfig: DefaultConfigExtension = DefaultConfigExtension()

    /**
     * Map of [TaskConfigurator]s that are used for configuration of tasks created via this manager.
     *
     * @see [createAndConfigureTask]
     * @see [configureTask]
     */
    private val taskConfigurators: MutableMap<String, TaskConfigurator<*>> = mutableMapOf()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets a repository that should be attached to each [TaskConfigurator] registered for this
     * manager via [registerTaskConfigurator].
     *
     * @param repository The desired repository.
     *
     * @see TaskConfigurator.setRepository
     */
    internal fun setRepository(repository: RepositoryExtension) {
        this.repository = repository
        this.taskConfigurators.forEach { it.value.setRepository(repository) }
    }

    /**
     * Sets a default configuration that should be attached to each [TaskConfigurator] registered
     * for this manager via [registerTaskConfigurator].
     *
     * @param defaultConfig The desired default configuration.
     *
     * @see TaskConfigurator.setDefaultConfig
     */
    internal fun setDefaultConfig(defaultConfig: DefaultConfigExtension) {
        this.defaultConfig = defaultConfig
        this.taskConfigurators.forEach { it.value.setDefaultConfig(defaultConfig) }
    }

    /**
     * Registers a configurator instance which may be later used to perform configuration of a task
     * that is described/created for the specified *taskSpecification*.
     *
     * @param taskSpecification The specification of a task for which to register its associated configurator.
     * @param taskConfigurator The configurator responsible for configuration of the corresponding task.
     *
     * @see [configureTask]
     */
    internal fun <T : Task> registerTaskConfigurator(taskSpecification: TaskSpecification<T>, taskConfigurator: TaskConfigurator<T>) {
        this.taskConfigurators[taskSpecification.name] = taskConfigurator
        taskConfigurator.setRepository(repository)
        taskConfigurator.setDefaultConfig(defaultConfig)
    }

    /**
     * Creates a new task for the specified *taskSpecification* and performs configuration for it
     * using task configurator registered via [registerTaskConfigurator] for the specified *taskSpecification*.
     * If there is no such configurator registered, no configuration will be performed for the crated
     * task.
     *
     * The returned task will be already added into the project's tasks collection.
     *
     * @param taskSpecification Specification of the desired task to be created.
     * @return Created and configured task ready to be executed.
     *
     * @see [createTask]
     * @see [configureTask]
     */
    internal fun <T : Task> createAndConfigureTask(taskSpecification: TaskSpecification<T>): T {
        return configureTask(createTask(taskSpecification))
    }

    /**
     * Performs configuration of the given *task* if there is already registered configurator for it
     * via [registerTaskConfigurator]. If there is no such configurator registered, no configuration
     * will be performed.
     *
     * @param task The task to be configured.
     * @return The same instance of task already configured.
     *
     * @see [createTask]
     */
    @Suppress("UNCHECKED_CAST")
    internal fun <T : Task> configureTask(task: T): T {
        val configurator = taskConfigurators[task.name] ?: return task
        return (configurator as TaskConfigurator<T>).configureTask(task)
    }

    /**
     * Creates a new instance of task described by the specified *taskSpecification*. The created
     * task will be already added into the project's tasks collection.
     *
     * @param taskSpecification Specification of the desired task to be created.
     * @return Created task ready to be executed or configured.
     *
     * @see [configureTask]
     */
    internal fun <T : Task> createTask(taskSpecification: TaskSpecification<T>): T {
        return ProjectTaskFactory.createTask(project, taskSpecification)
    }

    /*
     * Inner classes ===============================================================================
     */
}
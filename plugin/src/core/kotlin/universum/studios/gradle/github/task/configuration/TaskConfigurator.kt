/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task.configuration

import org.gradle.api.Task
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension

/**
 * Task configurator is responsible for configuration of a specific [Task] implementation.
 *
 * @param T Type of the task that may be configured by this configurator.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface TaskConfigurator<T : Task> {

    /**
     * Specifies a repository that should be attached to tasks configured by this configurator.
     *
     * @param repository The repository to be attached to tasks configured by this configurator.
     */
    fun setRepository(repository: RepositoryExtension)

    /**
     * Specifies a default configuration that should be used by this configurator when configuring
     * tasks passed to [configureTask].
     *
     * @param defaultConfig The extension with default configuration to be used by the configurator.
     */
    fun setDefaultConfig(defaultConfig: DefaultConfigExtension)

    /**
     * Performs configuration of the given *task*.
     *
     * @param task The task to which to apply configuration of this configurator.
     * @return The task with configuration specified by this configurator.
     */
    fun configureTask(task: T): T
}
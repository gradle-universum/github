buildscript {
    repositories {
        jcenter()
    }
}

plugins {
    id 'universum.studios.github'
}

repositories {
    jcenter()
}

github {
    repository {
        owner 'universum-studios'
        name 'test'
        accessToken System.getenv('GITHUB_API_TOKEN')
    }
}
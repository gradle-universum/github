buildscript {
    repositories {
        jcenter()
    }
}

plugins {
    id 'universum.studios.github'
}

repositories {
    jcenter()
}

github {
    defaultConfig {
        artifactId 'test-artifact'
    }
}
buildscript {
    repositories {
        jcenter()
    }
}

plugins {
    id 'universum.studios.github'
}

repositories {
    jcenter()
}

github {
    repository {
        owner 'universum-studios'
        name 'test'
        accessToken System.getenv('GITHUB_API_TOKEN')
    }

    releases {
        types {
            test {
                tagName 'test'
                targetCommitish 'develop'
                releaseName 'test'
                releaseBody 'Test release body.'
            }
            beta {
                tagName 'beta'
                targetCommitish 'develop'
                releaseName 'beta'
                releaseBody 'Beta release body.'
            }
        }
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.gradle.github.extension.RepositoryExtension

/**
 * @author Martin Albedinsky
 */
class BaseApiTest {

    @Test fun testContract() {
        // Assert:
        assertThat(BaseApi.HEADER_ACCEPT_NAME, `is`("Accept"))
        assertThat(BaseApi.HEADER_ACCEPT_VALUE, `is`("application/vnd.github.v3+json"))
    }

    @Test fun testInstantiation() {
        // Arrange:
        val repository = RepositoryExtension()
        // Act:
        val api = TestApi(repository)
        // Assert:
        assertThat(api.getRepository(), `is`(repository))
    }

    interface TestServices

    internal class TestApi(repository: RepositoryExtension) : BaseApi<TestServices>(repository, TestServices::class.java)
}
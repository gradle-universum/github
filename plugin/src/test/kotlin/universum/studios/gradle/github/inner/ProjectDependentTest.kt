/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.inner

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import universum.studios.gradle.github.extension.RepositoryExtension
import java.io.File

fun resourceFile(resourceFilePath: String): File {
    val resourceUrl = ProjectDependentTest::class.java.getResource(resourceFilePath)
    resourceUrl ?: throw AssertionError("Resource at path '$resourceFilePath' not found!")
    return File(resourceUrl.path)
}

/**
 * @author Martin Albedinsky
 */
abstract class ProjectDependentTest {

    companion object {

        private const val OWNER = "universum-studios"
        private const val REPO = "test"
        val REPOSITORY = RepositoryExtension().apply {
            this.owner = OWNER
            this.name = REPO
            this.accessToken = System.getenv("GITHUB_API_TOKEN")
        }
    }

    lateinit var project: Project
    @Rule @JvmField val projectDir = TemporaryFolder()

    @Throws(Exception::class)
    @Before open fun beforeTest() {
        this.project = ProjectBuilder.builder().withProjectDir(projectDir.root).build()
    }

    protected fun <T : Task> createTask(type: Class<T>, group: String = "test-tasks", name: String) : T {
        return type.cast(project.tasks.create(LinkedHashMap<String, Any>().apply {
            put("group", group)
            put("name", name)
            put("type", type)
        }))
    }

    @Throws(Exception::class)
    @After open fun afterTest() {}
}
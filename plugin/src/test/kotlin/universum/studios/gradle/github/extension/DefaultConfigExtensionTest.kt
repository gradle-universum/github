/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class DefaultConfigExtensionTest : ExtensionBaseTest(extensionResourceFile("default_config.groovy")) {

    @Test fun testContract() {
        // Assert:
        assertThat(DefaultConfigExtension.NAME, `is`("defaultConfig"))
    }

    @Test fun testAccess() {
        // Arrange:
        applyPlugin()
        // Act:
        val extension = findPluginExtension(DefaultConfigExtension::class.java)
        // Assert:
        assertThat(extension, `is`(notNullValue()))
    }

    @Test fun testCopyFrom() {
        // Arrange:
        val extension = DefaultConfigExtension()
        val extensionToCopyFrom = DefaultConfigExtension().apply {
            this.artifactId = "test-artifact"
            this.artifactVersion = "1.0.0"
        }
        // Act:
        extension.copyFrom(extensionToCopyFrom)
        // Assert:
        assertThat(extension.artifactId, `is`(extensionToCopyFrom.artifactId))
        assertThat(extension.artifactVersion, `is`(extensionToCopyFrom.artifactVersion))
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.extension

import org.assertj.core.api.Assertions
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.extensionResourceFile
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.label.task.CreateDefaultLabelsTask
import universum.studios.gradle.github.label.task.CreateLabelsTask
import universum.studios.gradle.github.label.task.DeleteLabelsTask

/**
 * @author Martin Albedinsky
 */
class LabelsExtensionTest : BuildDependentTest(extensionResourceFile("labels.groovy")) {

    @Test fun testContract() {
        // Assert:
        assertThat(LabelsExtension.NAME, `is`("labels"))
    }

    @Test fun testOnPluginApplied() {
        // Arrange:
        val extension = LabelsExtension()
        // Act:
        extension.dispatchPluginApplied(project)
    }

    @Test fun testOnDefaultConfigChanged() {
        // Arrange:
        val extension = LabelsExtension()
        extension.dispatchPluginApplied(project)
        // Act:
        extension.setDefaultConfig(DefaultConfigExtension())
    }

    @Test fun testOnProjectEvaluated() {
        // Arrange + Act:
        val buildResult = prepareBuild().withArguments("tasks").build()
        // Assert:
        Assertions.assertThat(buildResult.output).contains(
                CreateDefaultLabelsTask.SPECIFICATION.name,
                CreateLabelsTask.SPECIFICATION.name,
                DeleteLabelsTask.SPECIFICATION.name
        )
    }

    @Test fun testItemsFile() {
        // todo: ...
    }

    @Test fun testItems() {
        // todo: ...
    }
}
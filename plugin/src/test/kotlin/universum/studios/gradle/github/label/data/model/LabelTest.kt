/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.data.model

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.not
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class LabelTest {

    @Test fun testInstantiation() {
        // Act:
        val label = Label(
                id = 1,
                name = "test-label",
                color = "ffffff"
        )
        // Assert:
        assertThat(label.id, `is`(1))
        assertThat(label.name, `is`("test-label"))
        assertThat(label.color, `is`("ffffff"))
    }

    @Test fun testComponents() {
        // Act:
        val label = Label(
                id = 1,
                name = "test-label",
                color = "ffffff"
        )
        // Assert:
        assertThat(label.component1(), `is`(1))
        assertThat(label.component2(), `is`("test-label"))
        assertThat(label.component3(), `is`("ffffff"))
    }

    @Test fun testCopy() {
        // Arrange:
        val label = Label(name = "test-label", color = "ff0000")
        // Act:
        val labelCopy = label.copy()
        // Assert:
        assertThat(labelCopy, `is`(label))
        assertThat(label.copy(name = "test-label-copy"), `is`(not(label)))
    }

    @Test fun testHashCode() {
        // Arrange:
        val label = Label(name = "test-label", color = "ff0000")
        // Act + Assert:
        assertThat(label.hashCode(), `is`(not(0)))
    }

    @Test fun testEquals() {
        // Arrange + Act + Assert:
        assertThat(Label(name = "test-label", color = "ff0000").equals(Label(name = "test-label", color = "ff0000")), `is`(true))
        assertThat(Label(name = "test-label-1", color = "ff0000").equals(Label(name = "test-label-2", color = "ff0000")), `is`(false))
    }

    @Test fun testToString() {
        // Arrange:
        val label = Label(
                id = 1,
                name = "test-label",
                color = "ffffff"
        )
        // Act + Assert:
        assertThat(label.toString(), `is`("Label(id=1, name=test-label, color=ffffff)"))
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.extension

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class LabelItemExtensionTest {

    companion object {

        const val LABEL_ITEM_NAME = "bug"
    }

    @Test fun testInstantiation() {
        // Act:
        val extension = LabelItemExtension(LABEL_ITEM_NAME)
        // Assert:
        assertThat(extension.name, `is`(LABEL_ITEM_NAME))
        assertThat(extension.name(), `is`(LABEL_ITEM_NAME))
        assertThat(extension.color(), `is`("ffffff"))
    }

    @Test fun testName() {
        // Arrange:
        val extension = LabelItemExtension(LABEL_ITEM_NAME)
        // Act + Assert:
        assertThat(extension.name("issue"), `is`(extension))
        assertThat(extension.name(), `is`("issue"))
    }

    @Test fun testColor() {
        // Arrange:
        val extension = LabelItemExtension(LABEL_ITEM_NAME)
        // Act + Assert:
        assertThat(extension.color("ff0000"), `is`(extension))
        assertThat(extension.color(), `is`("ff0000"))
    }
}
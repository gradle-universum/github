/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.extension

import org.assertj.core.api.Assertions
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Test
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.extensionResourceFile
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.release.task.*
import java.lang.reflect.InvocationTargetException

/**
 * @author Martin Albedinsky
 */
class ReleasesExtensionTest : BuildDependentTest(extensionResourceFile("releases.groovy")) {

    @Test fun testContract() {
        // Assert:
        assertThat(ReleasesExtension.NAME, `is`("releases"))
    }

    @Test(expected = IllegalAccessException::class)
    fun testConfigurationInstantiation() {
        // Act:
        ReleasesExtension.Configuration::class.java.newInstance()
    }

    @Test(expected = InvocationTargetException::class)
    fun testConfigurationInstantiationWithAccessibleConstructor() {
        // Arrange + Act:
        ReleasesExtension.Configuration::class.java.getDeclaredConstructor().apply { isAccessible = true }.newInstance()
    }

    @Test fun testConfiguration() {
        // Assert:
        assertThat(ReleasesExtension.Configuration.RELEASE_ASSET, `is`("githubReleaseAsset"))
        assertThat(ReleasesExtension.Configuration.RELEASE_ASSET_FORMAT, `is`("github%sReleaseAsset"))
    }

    @Test fun testConfigurationReleaseAsset() {
        // Act + Assert:
        assertThat(ReleasesExtension.Configuration.releaseAsset("test"), `is`("githubTestReleaseAsset"))
    }

    @Test fun testOnPluginApplied() {
        // Arrange:
        val extension = ReleasesExtension()
        // Act:
        extension.dispatchPluginApplied(project)
        // Assert:
        assertThat(project.configurations.findByName(ReleasesExtension.Configuration.RELEASE_ASSET), `is`(notNullValue()))
    }

    @Test fun testOnDefaultConfigChanged() {
        // Arrange:
        val extension = ReleasesExtension()
        extension.dispatchPluginApplied(project)
        // Act:
        extension.setDefaultConfig(DefaultConfigExtension())
    }

    @Test fun testOnProjectEvaluated() {
        // Arrange + Act:
        val buildResult = prepareBuild().withArguments("tasks", "--stacktrace").build()
        // Assert:
        Assertions.assertThat(buildResult.output).contains(
                ListAllReleasesTask.SPECIFICATION.name,
                ListLatestReleaseTask.SPECIFICATION.name,
                ListTaggedReleaseTask.SPECIFICATION.name,
                DeleteTaggedReleaseTask.SPECIFICATION.name,
                UploadReleaseTask.createSpecification(ReleaseTypeExtension("test")).name,
                UploadReleaseAssetsTask.createSpecification(ReleaseTypeExtension("test")).name,
                UploadReleaseTask.createSpecification(ReleaseTypeExtension("beta")).name,
                UploadReleaseAssetsTask.createSpecification(ReleaseTypeExtension("beta")).name
        )
    }

    @Test fun testAssets() {
        // todo: ...
    }

    @Test fun testTypes() {
        // todo: ...
    }
}
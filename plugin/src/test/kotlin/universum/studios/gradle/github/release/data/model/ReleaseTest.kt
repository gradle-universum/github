/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.not
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class ReleaseTest {

    @Test fun testContract() {
        // Assert:
        assertThat(Release.TARGET_COMMITISH, `is`("master"))
        assertThat(Release.DRAFT, `is`(false))
        assertThat(Release.PRE_RELEASE, `is`(false))
        assertThat(Release.OVERWRITE, `is`(false))
    }

    @Test fun testInstantiation() {
        // Act:
        val release = Release(
                id = 1,
                tagName = "test-tag",
                targetCommitish = "develop",
                name = "test-release",
                body = "Test release body.",
                draft = true,
                preRelease = false,
                overwrite = true
        )
        // Assert:
        assertThat(release.id, `is`(1))
        assertThat(release.tagName, `is`("test-tag"))
        assertThat(release.targetCommitish, `is`("develop"))
        assertThat(release.name, `is`("test-release"))
        assertThat(release.body, `is`("Test release body."))
        assertThat(release.draft, `is`(true))
        assertThat(release.preRelease, `is`(false))
        assertThat(release.overwrite, `is`(true))
    }

    @Test fun testInstantiationWithDefaultValues() {
        // Act:
        val release = Release(tagName = "test-tag")
        // Assert:
        assertThat(release.id, `is`(0))
        assertThat(release.tagName, `is`("test-tag"))
        assertThat(release.targetCommitish, `is`(Release.TARGET_COMMITISH))
        assertThat(release.name, `is`(""))
        assertThat(release.body, `is`(""))
        assertThat(release.draft, `is`(Release.DRAFT))
        assertThat(release.preRelease, `is`(Release.PRE_RELEASE))
        assertThat(release.overwrite, `is`(Release.OVERWRITE))
    }

    @Test fun testComponents() {
        // Arrange:
        val release = Release(
                id = 1,
                tagName = "test-tag",
                targetCommitish = "develop",
                name = "test-release",
                body = "Test release body.",
                draft = true,
                preRelease = false,
                overwrite = true
        )
        // Act + Assert:
        assertThat(release.component1(), `is`(1))
        assertThat(release.component2(), `is`("test-tag"))
        assertThat(release.component3(), `is`("develop"))
        assertThat(release.component4(), `is`("test-release"))
        assertThat(release.component5(), `is`("Test release body."))
        assertThat(release.component6(), `is`(true))
        assertThat(release.component7(), `is`(false))
        assertThat(release.component8(), `is`(true))
    }

    @Test fun testCopy() {
        // Arrange:
        val release = Release(tagName = "test-tag")
        // Act:
        val releaseCopy = release.copy()
        // Assert:
        assertThat(releaseCopy, `is`(release))
        assertThat(release.copy(tagName = "test-tag-copy"), `is`(not(release)))
    }

    @Test fun testHashCode() {
        // Arrange:
        val release = Release(tagName = "test-tag")
        // Act + Assert:
        assertThat(release.hashCode(), `is`(not(0)))
    }

    @Test fun testEquals() {
        // Arrange + Act + Assert:
        assertThat(Release(tagName = "test-tag").equals(Release(tagName = "test-tag")), `is`(true))
        assertThat(Release(tagName = "test-tag-1").equals(Release(tagName = "test-tag-2")), `is`(false))
    }

    @Test fun testToString() {
        // Arrange:
        val release = Release(
                id = 1,
                tagName = "test-tag",
                targetCommitish = "develop",
                name = "test-release",
                body = "Test release body.",
                draft = true,
                preRelease = false,
                overwrite = true
        )
        // Act + Assert:
        assertThat(release.toString(), `is`(
                "Release(id=1, tagName=test-tag, targetCommitish=develop, name=test-release, body=Test release body., draft=true, preRelease=false, overwrite=true)"
        ))
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.manage

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.*
import retrofit2.Call
import retrofit2.Response
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.label.data.model.Label
import universum.studios.gradle.github.label.service.api.LabelsApi
import universum.studios.gradle.github.label.service.model.RemoteLabel
import universum.studios.gradle.test.kotlin.KotlinArgumentMatchers

/**
 * @author Martin Albedinsky
 */
class LabelManagerTest : ProjectDependentTest() {

    companion object {

        val ERROR_RESPONSE_BODY: ResponseBody = "".toResponseBody("".toMediaTypeOrNull())
    }

    @Test fun testContract() {
        // Assert:
        assertThat(LabelManager.NO_RESULT, `is`(-1))
    }

    @Test fun testInstantiation() {
        // Arrange:
        val mockApi = mock(LabelsApi::class.java)
        // Act:
        val manager = LabelManager(project, mockApi)
        // Assert:
        assertThat(manager.project, `is`(project))
        assertThat(manager.api, `is`(mockApi))
    }

    @Test fun testCreateLabels() {
        // Arrange:
        val mockApi = mock(LabelsApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockCall = mock(RemoteLabelCall::class.java)
        `when`(mockCall.execute()).thenReturn(Response.success(RemoteLabel(name = "test-label")))
        `when`(mockApi.createLabel(KotlinArgumentMatchers.any())).thenReturn(mockCall)
        val manager = LabelManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.createLabels(listOf(
                createTestLabel(1),
                createTestLabel(2),
                createTestLabel(3),
                createTestLabel(4),
                createTestLabel(5),
                createTestLabel(6)
        )), `is`(6))
        verify(mockApi, times(6)).getRepository()
        verify(mockApi, times(6)).createLabel(KotlinArgumentMatchers.any())
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testCreateEmptyLabels() {
        // Arrange:
        val mockApi = mock(LabelsApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val manager = LabelManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.createLabels(emptyList()), `is`(0))
        verifyZeroInteractions(mockApi)
    }

    @Test fun testCreateLabelAsSuccess() {
        // Arrange:
        val mockApi = mock(LabelsApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockCall = mock(RemoteLabelCall::class.java)
        `when`(mockCall.execute()).thenReturn(Response.success(RemoteLabel(name = "test-label")))
        `when`(mockApi.createLabel(KotlinArgumentMatchers.any())).thenReturn(mockCall)
        val manager = LabelManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.createLabel(Label(
                name = "test-label",
                color = "ffffff"
        )), `is`(true))
        verify(mockApi).getRepository()
        verify(mockApi).createLabel(KotlinArgumentMatchers.eq(RemoteLabel(name = "test-label", color = "ffffff")))
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testCreateLabelAsFailure() {
        // Arrange:
        val mockApi = mock(LabelsApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockCall = mock(RemoteLabelCall::class.java)
        `when`(mockCall.execute()).thenReturn(Response.error(400, ERROR_RESPONSE_BODY))
        `when`(mockApi.createLabel(KotlinArgumentMatchers.any())).thenReturn(mockCall)
        val manager = LabelManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.createLabel(Label(
                name = "test-label",
                color = "ffffff"
        )), `is`(false))
        verify(mockApi).getRepository()
        verify(mockApi).createLabel(KotlinArgumentMatchers.eq(RemoteLabel(name = "test-label", color = "ffffff")))
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testDeleteAllLabels() {
        // Arrange:
        val labels = listOf(RemoteLabel(name = "test-label-1"), RemoteLabel(name = "test-label-2"))
        val mockApi = mock(LabelsApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockGetLabelsCall = mock(RemoteLabelsCall::class.java)
        `when`(mockGetLabelsCall.execute()).thenReturn(Response.success(labels))
        `when`(mockApi.getLabels()).thenReturn(mockGetLabelsCall)
        val mockDeleteCall = mock(VoidCall::class.java)
        `when`(mockDeleteCall.execute()).thenReturn(Response.success(null))
        `when`(mockApi.deleteLabel(KotlinArgumentMatchers.any())).thenReturn(mockDeleteCall)
        val manager = LabelManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.deleteAllLabels(), `is`(2))
        verify(mockApi).getLabels()
        verify(mockApi).getRepository()
        labels.forEach { verify(mockApi).deleteLabel(it)}
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testDeleteAllLabelsWhenThereAreNoLabels() {
        // Arrange:
        val mockApi = mock(LabelsApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockGetLabelsCall = mock(RemoteLabelsCall::class.java)
        `when`(mockGetLabelsCall.execute()).thenReturn(Response.success(emptyList()))
        `when`(mockApi.getLabels()).thenReturn(mockGetLabelsCall)
        val mockDeleteCall = mock(VoidCall::class.java)
        `when`(mockDeleteCall.execute()).thenReturn(Response.success(null))
        `when`(mockApi.deleteLabel(KotlinArgumentMatchers.any())).thenReturn(mockDeleteCall)
        val manager = LabelManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.deleteAllLabels(), `is`(LabelManager.NO_RESULT))
        verify(mockApi).getLabels()
        verifyNoMoreInteractions(mockApi)
    }

    private fun createTestLabel(identifier: Int) = Label(
            id = identifier,
            name = "test-label-$identifier",
            color = "ffffff"
    )

    abstract class RemoteLabelCall : Call<RemoteLabel>
    abstract class RemoteLabelsCall : Call<List<RemoteLabel>>
    abstract class VoidCall : Call<Void>
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.model

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class RemoteReleaseTest {

    // Arrange:
    // Act:
    // Assert:

    @Test
    fun testInstantiation() {
        val release = RemoteRelease(
                id = 1,
                tag_name = "test-tag",
                target_commitish = "develop",
                name = "test",
                body = "Test body.",
                draft = false,
                prerelease = true,
                created_at = "2017-05-23T14:05:22Z",
                published_at = "2017-05-23T14:05:22Z",
                upload_url = "test://url"
        )
        assertThat(release.id, `is`(1))
        assertThat(release.tag_name, `is`("test-tag"))
        assertThat(release.target_commitish, `is`("develop"))
        assertThat(release.name, `is`("test"))
        assertThat(release.body, `is`("Test body."))
        assertThat(release.draft, `is`(false))
        assertThat(release.prerelease, `is`(true))
        assertThat(release.created_at, `is`("2017-05-23T14:05:22Z"))
        assertThat(release.published_at, `is`("2017-05-23T14:05:22Z"))
        assertThat(release.upload_url, `is`("test://url"))
    }

    @Test
    fun testComponents() {
        val release = RemoteRelease(
                id = 1,
                tag_name = "test-tag",
                target_commitish = "develop",
                name = "test",
                body = "Test body.",
                draft = false,
                prerelease = true,
                created_at = "2017-05-23T14:05:22Z",
                published_at = "2017-05-23T14:05:22Z",
                upload_url = "test://url"
        )
        assertThat(release.component1(), `is`(1))
        assertThat(release.component2(), `is`("test-tag"))
        assertThat(release.component3(), `is`("develop"))
        assertThat(release.component4(), `is`("test"))
        assertThat(release.component5(), `is`("Test body."))
        assertThat(release.component6(), `is`(false))
        assertThat(release.component7(), `is`(true))
        assertThat(release.component8(), `is`("2017-05-23T14:05:22Z"))
        assertThat(release.component9(), `is`("2017-05-23T14:05:22Z"))
        assertThat(release.component10(), `is`("test://url"))
    }

    @Test
    fun testCopy() {
        assertThat(
                RemoteRelease(
                        id = 1,
                        tag_name = "tast-tag",
                        target_commitish = "develop",
                        name = "test",
                        body = "Test body.",
                        draft = false,
                        prerelease = true,
                        created_at = "2017-05-23T14:05:22Z",
                        published_at = "2017-05-23T14:05:22Z",
                        upload_url = "test://url"
                ).copy(),
                `is`(RemoteRelease(
                        id = 1,
                        tag_name = "tast-tag",
                        target_commitish = "develop",
                        name = "test",
                        body = "Test body.",
                        draft = false,
                        prerelease = true,
                        created_at = "2017-05-23T14:05:22Z",
                        published_at = "2017-05-23T14:05:22Z",
                        upload_url = "test://url"
                ))
        )
    }

    @Test
    fun testEquals() {
        assertThat(
                RemoteRelease(
                        id = 1,
                        tag_name = "tast-tag",
                        target_commitish = "develop",
                        name = "test",
                        body = "Test body.",
                        draft = false,
                        prerelease = true,
                        created_at = "2017-05-23T14:05:22Z",
                        published_at = "2017-05-23T14:05:22Z",
                        upload_url = "test://url"
                ).equals(RemoteRelease(
                        id = 1,
                        tag_name = "tast-tag",
                        target_commitish = "develop",
                        name = "test",
                        body = "Test body.",
                        draft = false,
                        prerelease = true,
                        created_at = "2017-05-23T14:05:22Z",
                        published_at = "2017-05-23T14:05:22Z",
                        upload_url = "test://url"
                )),
                `is`(true)
        )
    }

    @Test
    fun testHashCode() {
        assertThat(
                RemoteRelease(id = 1,
                        tag_name = "test-tag",
                        target_commitish = "develop",
                        name = "test",
                        body = "Test body.",
                        draft = false,
                        prerelease = true,
                        created_at = "2017-05-23T14:05:22Z",
                        published_at = "2017-05-23T14:05:22Z",
                        upload_url = "test://url"
                ).hashCode(),
                `is`(not(0))
        )
    }

    @Test
    fun testToString() {
        assertThat(
                RemoteRelease(id = 1,
                        tag_name = "test-tag",
                        target_commitish = "develop",
                        name = "test",
                        body = "Test body.",
                        draft = false,
                        prerelease = true,
                        created_at = "2017-05-23T14:05:22Z",
                        published_at = "2017-05-23T14:05:22Z",
                        upload_url = "test://url"
                ).toString(),
                `is`("RemoteRelease(" +
                        "id=1, " +
                        "tag_name=test-tag, " +
                        "target_commitish=develop, " +
                        "name=test, " +
                        "body=Test body., " +
                        "draft=false, " +
                        "prerelease=true, " +
                        "created_at=2017-05-23T14:05:22Z, " +
                        "published_at=2017-05-23T14:05:22Z, " +
                        "upload_url=test://url, " +
                        "assets=null" +
                        ")")
        )
    }
}
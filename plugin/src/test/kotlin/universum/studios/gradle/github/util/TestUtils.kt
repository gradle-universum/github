/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.util

fun readFromFile(file: java.io.File) : String {
    val reader = file.bufferedReader()
    val content = reader.readText()
    reader.close()
    return content
}

fun writeToFile(file: java.io.File, content: String) {
    if (content.isNotEmpty()) {
        val writer = file.bufferedWriter()
        writer.write(content)
        writer.flush()
        writer.close()
    }
}
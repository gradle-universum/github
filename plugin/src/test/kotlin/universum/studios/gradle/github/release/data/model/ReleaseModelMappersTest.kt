/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import org.gradle.api.internal.artifacts.publish.DefaultPublishArtifact
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Test
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension
import universum.studios.gradle.github.release.service.model.RemoteRelease
import java.io.File

/**
 * @author Martin Albedinsky
 */
class ReleaseModelMappersTest {

    @Test fun testMapperInstances() {
        // Assert:
        assertThat(ReleaseModelMappers.RELEASE_TYPE_TO_RELEASE, `is`(notNullValue()))
        assertThat(ReleaseModelMappers.RELEASE_TO_REMOTE_RELEASE, `is`(notNullValue()))
        assertThat(ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE, `is`(notNullValue()))
        assertThat(ReleaseModelMappers.PUBLISH_ARTIFACT_TO_ASSET, `is`(notNullValue()))
        assertThat(ReleaseModelMappers.ASSET_TO_REMOTE_ASSET, `is`(notNullValue()))
    }

    @Test fun testReleaseTypeToRelease() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("testReleaseType")
                .tagName("test-tag")
                .targetCommitish("develop")
                .releaseName("test-release")
                .releaseBody("Test release body.")
                .draft(true)
                .preRelease(false)
                .overwrite(true)
        // Act:
        val release = ReleaseModelMappers.RELEASE_TYPE_TO_RELEASE.map(releaseType)
        // Assert:
        assertThat(release, `is`(notNullValue()))
        assertThat(release.id, `is`(0))
        assertThat(release.tagName, `is`(releaseType.tagName()))
        assertThat(release.targetCommitish, `is`(releaseType.targetCommitish()))
        assertThat(release.name, `is`(releaseType.releaseName()))
        assertThat(release.body, `is`(releaseType.releaseBody()))
        assertThat(release.draft, `is`(releaseType.draft()))
        assertThat(release.preRelease, `is`(releaseType.preRelease()))
        assertThat(release.overwrite, `is`(releaseType.overwrite()))
    }

    @Test fun testReleaseTypeToReleaseWithDefaultValues() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("testReleaseType")
                .tagName("test-tag")
                .targetCommitish("develop")
                .releaseName("test-release")
                .releaseBody("Test release body.")
        // Act:
        val release = ReleaseModelMappers.RELEASE_TYPE_TO_RELEASE.map(releaseType)
        // Assert:
        assertThat(release, `is`(notNullValue()))
        assertThat(release.id, `is`(0))
        assertThat(release.tagName, `is`(releaseType.tagName()))
        assertThat(release.targetCommitish, `is`(releaseType.targetCommitish()))
        assertThat(release.name, `is`(releaseType.releaseName()))
        assertThat(release.body, `is`(releaseType.releaseBody()))
        assertThat(release.draft, `is`(false))
        assertThat(release.preRelease, `is`(false))
        assertThat(release.overwrite, `is`(false))
    }

    @Test(expected = IllegalArgumentException::class)
    fun testReleaseTypeToReleaseWithoutTagNameValue() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("testReleaseType")
                .targetCommitish("develop")
                .releaseName("test-release")
                .releaseBody("Test release body.")
                .draft(true)
                .preRelease(false)
                .overwrite(true)
        // Act:
        ReleaseModelMappers.RELEASE_TYPE_TO_RELEASE.map(releaseType)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testReleaseTypeToReleaseWithoutTargetCommitishValue() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("testReleaseType")
                .tagName("test-tag")
                .releaseName("test-release")
                .releaseBody("Test release body.")
                .draft(true)
                .preRelease(false)
                .overwrite(true)
        // Act:
        ReleaseModelMappers.RELEASE_TYPE_TO_RELEASE.map(releaseType)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testReleaseTypeToReleaseWithoutReleaseNameValue() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("testReleaseType")
                .tagName("test-tag")
                .targetCommitish("develop")
                .releaseBody("Test release body.")
                .draft(true)
                .preRelease(false)
                .overwrite(true)
        // Act:
        ReleaseModelMappers.RELEASE_TYPE_TO_RELEASE.map(releaseType)
    }

    @Test fun testReleaseToRemoteReleaseMapper() {
        // Arrange:
        val release = Release(
                id = 1,
                tagName = "test-tag",
                targetCommitish = "develop",
                name = "test-release",
                body = "Test release body.",
                draft = true,
                preRelease = false,
                overwrite = true
        )
        // Act:
        val remoteRelease = ReleaseModelMappers.RELEASE_TO_REMOTE_RELEASE.map(release)
        // Assert:
        assertThat(remoteRelease, `is`(notNullValue()))
        assertThat(remoteRelease.id, `is`(release.id))
        assertThat(remoteRelease.tag_name, `is`(release.tagName))
        assertThat(remoteRelease.target_commitish, `is`(release.targetCommitish))
        assertThat(remoteRelease.name, `is`(release.name))
        assertThat(remoteRelease.body, `is`(release.body))
        assertThat(remoteRelease.draft, `is`(release.draft))
        assertThat(remoteRelease.prerelease, `is`(release.preRelease))
    }

    @Test fun testRemoteReleaseToRelease() {
        // Arrange:
        val remoteRelease = RemoteRelease(0).apply {
            this.tag_name = "test-tag"
            this.target_commitish = "master"
            this.name = "test-release"
            this.body = "Test release body."
            this.draft = false
            this.prerelease = true
        }
        // Act:
        val release = ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
        // Assert:
        assertThat(release, `is`(notNullValue()))
        assertThat(release.id, `is`(remoteRelease.id))
        assertThat(release.tagName, `is`(remoteRelease.tag_name))
        assertThat(release.targetCommitish, `is`(remoteRelease.target_commitish))
        assertThat(release.name, `is`(remoteRelease.name))
        assertThat(release.body, `is`(remoteRelease.body))
        assertThat(release.draft, `is`(remoteRelease.draft))
        assertThat(release.preRelease, `is`(remoteRelease.prerelease))
        assertThat(release.overwrite, `is`(Release.OVERWRITE))
    }

    @Test(expected = IllegalArgumentException::class)
    fun testRemoteReleaseToReleaseWithoutTargetCommitishValue() {
        // Arrange:
        val remoteRelease = RemoteRelease(0).apply {
            this.tag_name = "test-tag"
            this.name = "test-release"
            this.body = "Test release body."
            this.draft = false
            this.prerelease = true
        }
        // Act:
        ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testRemoteReleaseToReleaseWithoutNameValue() {
        // Arrange:
        val remoteRelease = RemoteRelease(0).apply {
            this.tag_name = "test-tag"
            this.target_commitish = "master"
            this.body = "Test release body."
            this.draft = false
            this.prerelease = true
        }
        // Act:
        ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testRemoteReleaseToReleaseWithoutBodyValue() {
        // Arrange:
        val remoteRelease = RemoteRelease(0).apply {
            this.tag_name = "test-tag"
            this.target_commitish = "master"
            this.name = "test-release"
            this.draft = false
            this.prerelease = true
        }
        // Act:
        ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testRemoteReleaseToReleaseWithoutDraftValue() {
        // Arrange:
        val remoteRelease = RemoteRelease(0).apply {
            this.tag_name = "test-tag"
            this.target_commitish = "master"
            this.name = "test-release"
            this.body = "Test release body."
            this.prerelease = true
        }
        // Act:
        ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testRemoteReleaseToReleaseWithoutPreReleaseValue() {
        // Arrange:
        val remoteRelease = RemoteRelease(0).apply {
            this.tag_name = "test-tag"
            this.target_commitish = "master"
            this.name = "test-release"
            this.body = "Test release body."
            this.draft = false
        }
        // Act:
        ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
    }

    @Test fun testPublishArtifactToAsset() {
        // Arrange:
        val artifact = DefaultPublishArtifact(
                "test-name",
                "test-extension",
                "test-type",
                "test-classifier",
                null,
                File("test-artifact.jar")
        )
        // Act:
        val asset = ReleaseModelMappers.PUBLISH_ARTIFACT_TO_ASSET.map(artifact)
        // Assert:
        assertThat(asset, `is`(notNullValue()))
        assertThat(asset.id, `is`(0))
        assertThat(asset.name, `is`(artifact.name))
        assertThat(asset.file, `is`(File("test-artifact.jar")))
        assertThat(asset.fileContentType, `is`(Asset.CONTENT_TYPE))
    }

    @Test fun testPublishArtifactToAssetWithFileTypeOfTxt() {
        // Arrange:
        val artifact = DefaultPublishArtifact(
                "test-name",
                "test-extension",
                "test-type",
                "test-classifier",
                null,
                File("test-artifact.txt")
        )
        // Act:
        val asset = ReleaseModelMappers.PUBLISH_ARTIFACT_TO_ASSET.map(artifact)
        // Assert:
        assertThat(asset, `is`(notNullValue()))
        assertThat(asset.id, `is`(0))
        assertThat(asset.name, `is`(artifact.name))
        assertThat(asset.file, `is`(File("test-artifact.txt")))
        assertThat(asset.fileContentType, `is`("text/plain"))
    }

    @Test fun testAssetToRemoteAsset() {
        // Arrange:
        val asset = Asset(
                id = 1,
                name = "test-name",
                file = File("test-artifact.jar"),
                fileContentType = "test-type"
        )
        // Act:
        val remoteAsset = ReleaseModelMappers.ASSET_TO_REMOTE_ASSET.map(asset)
        // Assert:
        assertThat(remoteAsset, `is`(notNullValue()))
        assertThat(remoteAsset.id, `is`(1))
        assertThat(remoteAsset.name, `is`(remoteAsset.name))
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.extension

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.core.IsNull.nullValue
import org.junit.Test
import universum.studios.gradle.github.inner.ProjectDependentTest

/**
 * @author Martin Albedinsky
 */
class ReleaseTypeExtensionTest : ProjectDependentTest() {

    companion object {

        private const val RELEASE_TYPE_NAME = "releaseType"
    }

    @Test fun testInstantiation() {
        // Act:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Assert:
        assertThat(extension.name, `is`(RELEASE_TYPE_NAME))
        assertThat(extension.tagName(), `is`(nullValue()))
        assertThat(extension.targetCommitish(), `is`(nullValue()))
        assertThat(extension.releaseName(), `is`(nullValue()))
        assertThat(extension.releaseBody(), `is`(nullValue()))
        assertThat(extension.draft(), `is`(nullValue()))
        assertThat(extension.preRelease(), `is`(nullValue()))
        assertThat(extension.overwrite(), `is`(nullValue()))
        assertThat(extension.artifactId(), `is`(nullValue()))
        assertThat(extension.artifactVersion(), `is`(nullValue()))
    }

    @Test fun testTagName() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.tagName("test-tag"), `is`(extension))
        assertThat(extension.tagName(), `is`("test-tag"))
    }

    @Test fun testTargetCommitish() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.targetCommitish("develop"), `is`(extension))
        assertThat(extension.targetCommitish(), `is`("develop"))
    }

    @Test fun testReleaseName() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.releaseName("test-release"), `is`(extension))
        assertThat(extension.releaseName(), `is`("test-release"))
    }

    @Test fun testReleaseBody() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.releaseBody("Test release body."), `is`(extension))
        assertThat(extension.releaseBody(), `is`("Test release body."))
    }

    @Test fun testDraft() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.draft(true), `is`(extension))
        assertThat(extension.draft(), `is`(true))
    }

    @Test fun testPreRelease() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.preRelease(true), `is`(extension))
        assertThat(extension.preRelease(), `is`(true))
    }

    @Test fun testOverwrite() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.overwrite(true), `is`(extension))
        assertThat(extension.overwrite(), `is`(true))
    }

    @Test fun testAssets() {
        // todo: ...
    }

    @Test fun testArtifactId() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.artifactId("test-artifact"), `is`(extension))
        assertThat(extension.artifactId(), `is`("test-artifact"))
    }

    @Test fun testArtifactVersion() {
        // Arrange:
        val extension = ReleaseTypeExtension(RELEASE_TYPE_NAME)
        // Act + Assert:
        assertThat(extension.artifactVersion("1.0.0"), `is`(extension))
        assertThat(extension.artifactVersion(), `is`("1.0.0"))
    }

    @Test fun testFactoryCreate() {
        // Arrange:
        val factory = ReleaseTypeExtension.Factory(project)
        // Act:
        val releaseType = factory.create("testTypeName")
        // Assert:
        assertThat(releaseType.name, `is`("testTypeName"))
        assertThat(project.configurations.findByName(ReleasesExtension.Configuration.releaseAsset("testTypeName")), `is`(notNullValue()))
    }

    @Test(expected = IllegalArgumentException::class)
    fun testFactoryCreateWithIllegalName() {
        // Arrange:
        val factory = ReleaseTypeExtension.Factory(project)
        // Act:
        factory.create(null)
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.gradle.github.service.api.Api

/**
 * @author Martin Albedinsky
 */
class RepositoryExtensionTest : ExtensionBaseTest(extensionResourceFile("repository.groovy")) {

    @Test fun testContract() {
        // Assert:
        assertThat(RepositoryExtension.NAME, `is`("repository"))
    }

    @Test fun testAccess() {
        // Arrange:
        applyPlugin()
        // Act:
        val extension = findPluginExtension(RepositoryExtension::class.java)
        // Assert:
        assertThat(extension, `is`(notNullValue()))
    }

    @Test fun testPath() {
        // Arrange:
        val extension = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_name"
        }
        // Act + Assert:
        assertThat(extension.path(), `is`("test-owner/test_name"))
    }

    @Test fun testRegisterApi() {
        // Arrange:
        val api = TestApi()
        val extension = RepositoryExtension()
        // Act:
        extension.registerApi(TestApi::class, api)
        // Assert:
        assertThat(extension.findApi(TestApi::class), `is`(api))
    }

    @Test fun testCopyFrom() {
        // Arrange:
        val extension = RepositoryExtension()
        val extensionToCopyFrom = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_repo"
            this.accessToken = "access-token"
        }
        // Act:
        extension.copyFrom(extensionToCopyFrom)
        // Assert:
        assertThat(extension.owner, `is`(extensionToCopyFrom.owner))
        assertThat(extension.name, `is`(extensionToCopyFrom.name))
        assertThat(extension.accessToken, `is`(extensionToCopyFrom.accessToken))
    }

    class TestApi : Api {

        override fun getRepository() = RepositoryExtension()
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import org.gradle.api.plugins.ExtensionAware
import universum.studios.gradle.github.GitHubExtension
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.inner.resourceFile

fun extensionResourceFile(extensionFileName: String): java.io.File {
    return resourceFile("/extension/$extensionFileName")
}

/**
 * @author Martin Albedinsky
 */
abstract class ExtensionBaseTest : BuildDependentTest {

    constructor(buildFile: java.io.File) : super(buildFile)
    constructor(buildFileContent: String = "") : super(buildFileContent)

    fun <T> findPluginExtension(extensionType: Class<T>) : T? {
        val primaryExtension = project.extensions.findByName(GitHubExtension.Companion.NAME)
        primaryExtension ?: throw AssertionError("Primary extension was not found in the test project!")
        return (primaryExtension as ExtensionAware).extensions.findByType(extensionType)
    }
}
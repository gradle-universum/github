/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import org.assertj.core.api.Assertions
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import universum.studios.gradle.github.extension.extensionResourceFile
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * @author Martin Albedinsky
 */
class DeleteTaggedReleaseTaskTest : BuildDependentTest(extensionResourceFile("releases.groovy")) {

    companion object {

        val UPLOAD_RELEASE_TASK_SPECIFICATION = UploadReleaseTask.createSpecification(ReleaseTypeExtension("test").apply {
            tagName("test")
            releaseName("test")
            releaseBody("Test release body.")
            overwrite(true)
        })
    }

    @Test fun testSpecification() {
        // Arrange:
        val specification = DeleteTaggedReleaseTask.SPECIFICATION
        // Act + Assert:
        assertThat(specification, `is`(notNullValue()))
        assertThat(specification.group, `is`(TaskSpecification.GROUP))
        assertEquals(specification.type, DeleteTaggedReleaseTask::class.java)
        assertThat(specification.name, `is`("githubDeleteTaggedRelease"))
        assertThat(specification.description, `is`("Deletes a single release associated with a specific tag for the project's GitHub repository."))
        assertThat(specification.dependsOn, `is`(emptyList()))
    }

    @Ignore("Improperly configured...")
    @Test fun testPerform() {
        // Arrange:
        applyPlugin()
        // Create release.
        // fixme: creation fails with the following api error ...
        //      HTTP 1.1 422 Unprocessable Entity
        //      {"message":"Validation Failed","errors":[{"resource":"Release","code":"invalid","field":"target_commitish"}],"documentation_url":"https://developer.github.com/v3/repos/releases/#create-a-release"}
        prepareBuild().withArguments("tasks", UPLOAD_RELEASE_TASK_SPECIFICATION.name).build()
        // Act:
        val buildResult = prepareBuild().withArguments("tasks", DeleteTaggedReleaseTask.SPECIFICATION.name, "-Ptag=test").build()
        // Assert:
        Assertions.assertThat(buildResult.output).contains(
                "Release with tag 'test' for '${REPOSITORY.path()}' has been successfully deleted."
        )
    }

    @Test fun testPerformForNotExistingRelease() {
        // Arrange:
        applyPlugin()
        // Act:
        val buildResult = prepareBuild().withArguments("tasks", DeleteTaggedReleaseTask.SPECIFICATION.name, "-Ptag=notexisting").build()
        // Assert:
        Assertions.assertThat(buildResult.output).contains(
                "No such release with tag 'notexisting' for '${REPOSITORY.path()}' found to be deleted."
        )
    }
}
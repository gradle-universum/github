/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.api

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Test
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.label.service.model.RemoteLabel
import universum.studios.gradle.github.service.HttpCode

/**
 * @author Martin Albedinsky
 */
class LabelsApiImplTest : ProjectDependentTest() {

    private companion object {

        val LABEL = RemoteLabel(
                name = "test-label",
                color = "000000"
        )
    }

    @Test fun testGetLabels() {
        // Arrange:
        val api = LabelsApiImpl(REPOSITORY)
        // Act + Assert:
        val response = api.getLabels().execute()
        assertThatResponseIsSuccessful(response)
        val responseBody = response.body() ?: throw AssertionError("Received response without body!")
        assertThat(responseBody.isNotEmpty(), `is`(true))
    }

    @Test fun testCreateDeleteLabel() {
        // Arrange:
        val api = LabelsApiImpl(REPOSITORY)
        // Act + Assert:
        // CREATE ----------------------------------------------------------------------------------
        val createResponse = api.createLabel(LABEL).execute()
        assertThatResponseIsSuccessful(createResponse)
        assertThat(createResponse.body()?.name, `is`(LABEL.name))
        assertThat(createResponse.body()?.color, `is`(LABEL.color))
        // Assert that is created:
        val verifyCreationResponse = api.getNamedLabel(LABEL.name).execute()
        assertThatResponseIsSuccessful(verifyCreationResponse)
        // DELETE ----------------------------------------------------------------------------------
        val deleteResponse = api.deleteLabel(createResponse.body()!!).execute()
        assertThatResponseIsSuccessful(deleteResponse)
        // Assert that is deleted:
        val verifyDeletionResponse = api.getNamedLabel(LABEL.name).execute()
        assertThatResponseIsFailure(verifyDeletionResponse)
        assertThat(verifyDeletionResponse.code(), `is`(HttpCode.NOT_FOUND))
    }

    private fun assertThatResponseIsSuccessful(response: retrofit2.Response<*>) {
        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.isSuccessful, `is`(true))
    }

    private fun assertThatResponseIsFailure(response: retrofit2.Response<*>) {
        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.isSuccessful, `is`(false))
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api

import okhttp3.*
import java.util.concurrent.TimeUnit

/**
 * @author Martin Albedinsky
 */
class TestChain(
    private val request: Request,
    private val responseBuilder: Response.Builder = Response.Builder()
        .protocol(Protocol.HTTP_1_1)
        .code(200)
        .message("Response message")
) : Interceptor.Chain {

    override fun request(): Request = request

    override fun proceed(request: Request): Response = responseBuilder.request(request).build()

    override fun connection(): Connection? = throw UnsupportedOperationException()

    override fun withConnectTimeout(timeout: Int, unit: TimeUnit): Interceptor.Chain = throw UnsupportedOperationException()

    override fun connectTimeoutMillis(): Int = throw UnsupportedOperationException()

    override fun withWriteTimeout(timeout: Int, unit: TimeUnit): Interceptor.Chain = throw UnsupportedOperationException()

    override fun writeTimeoutMillis(): Int = throw UnsupportedOperationException()

    override fun withReadTimeout(timeout: Int, unit: TimeUnit): Interceptor.Chain = throw UnsupportedOperationException()

    override fun readTimeoutMillis(): Int = throw UnsupportedOperationException()

    override fun call(): Call = throw UnsupportedOperationException()
}
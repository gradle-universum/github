/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.extension

import org.gradle.api.Project
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class BaseProjectExtensionTest : ExtensionBaseTest() {

    @Test fun testInstantiation() {
        // Act:
        val extension = TestExtension()
        // Assert:
        assertThat(extension.getRepository(), `is`(notNullValue()))
        assertThat(extension.getDefaultConfig(), `is`(notNullValue()))
    }

    @Test fun testRepository() {
        // Arrange:
        val repository = RepositoryExtension()
        val extension = TestExtension()
        // Act + Assert:
        extension.setRepository(repository)
        assertThat(extension.getRepository(), `is`(repository))
        assertThat(extension.repositoryChanged, `is`(true))
    }

    @Test fun testDefaultConfig() {
        // Arrange:
        val defaultConfig = DefaultConfigExtension()
        val extension = TestExtension()
        // Act + Assert:
        extension.setDefaultConfig(defaultConfig)
        assertThat(extension.getDefaultConfig(), `is`(defaultConfig))
        assertThat(extension.defaultConfigChanged, `is`(true))
    }

    @Test fun testDispatchPluginApplied() {
        // Arrange:
        val extension = TestExtension()
        // Act:
        extension.dispatchPluginApplied(project)
        // Assert:
        assertThat(extension.pluginApplied, `is`(true))
        assertThat(extension.getProject(), `is`(project))
    }

    @Test(expected = IllegalStateException::class)
    fun testDispatchPluginAppliedWhenAlreadyAttachedToProject() {
        // Arrange:
        val extension = TestExtension()
        extension.dispatchPluginApplied(project)
        // Act:
        extension.dispatchPluginApplied(project)
    }

    @Test(expected = IllegalStateException::class)
    fun testGetProjectWhenNotAttachedToAnyProject() {
        // Arrange:
        val extension = TestExtension()
        // Act:
        extension.getProject()
    }

    @Test fun testDispatchProjectEvaluated() {
        // Arrange:
        val extension = TestExtension()
        extension.dispatchPluginApplied(project)
        // Act:
        extension.dispatchProjectEvaluated()
        // Assert:
        assertThat(extension.projectEvaluated, `is`(true))
    }

    @Test(expected = IllegalStateException::class)
    fun testDispatchProjectEvaluatedWhenNotAttachedToProject() {
        // Arrange:
        val extension = TestExtension()
        // Act:
        extension.dispatchProjectEvaluated()
    }

    class TestExtension : BaseProjectExtension() {

        var repositoryChanged = false
        var defaultConfigChanged = false
        var pluginApplied = false
        var projectEvaluated = false

        override fun onRepositoryChanged(repository: RepositoryExtension) {
            super.onRepositoryChanged(repository)
            this.repositoryChanged = true
        }

        override fun onDefaultConfigChanged(defaultConfig: DefaultConfigExtension) {
            super.onDefaultConfigChanged(defaultConfig)
            this.defaultConfigChanged = true
        }

        override fun onPluginApplied(project: Project) {
            super.onPluginApplied(project)
            this.pluginApplied = true
        }

        override fun onProjectEvaluated(project: Project) {
            super.onProjectEvaluated(project)
            this.projectEvaluated = true
        }
    }
}
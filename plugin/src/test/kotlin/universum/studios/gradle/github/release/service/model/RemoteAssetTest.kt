/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.model

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class RemoteAssetTest {

    // Arrange:
    // Act:
    // Assert:

    @Test
    fun testCompanion() {
        assertThat(RemoteAsset.Companion, `is`(notNullValue()))
        assertThat(RemoteAsset.UPLOAD_URL_FORMAT, `is`("%srepos/%s/%s/releases/%d/assets?name=%s"))
    }

    @Test
    fun testInstantiation() {
        val asset = RemoteAsset(id = 1, name = "test-asset.jar")
        assertThat(asset.id, `is`(1))
        assertThat(asset.name, `is`("test-asset.jar"))
    }

    @Test
    fun testCopy() {
        assertThat(RemoteAsset(id = 1, name = "test-asset.jar").copy(), `is`(RemoteAsset(id = 1, name = "test-asset.jar")))
    }

    @Test
    fun testEquals() {
        assertThat(RemoteAsset(id = 1, name = "test-asset.jar").equals(RemoteAsset(id = 1, name = "test-asset.jar")), `is`(true))
    }

    @Test
    fun testHashCode() {
        assertThat(RemoteAsset(id = 1, name = "test-asset.jar").hashCode(), `is`(not(0)))
    }

    @Test
    fun testToString() {
        assertThat(
                RemoteAsset(id = 1, name = "test-asset.jar").toString(),
                `is`("RemoteAsset(id=1, name=test-asset.jar)")
        )
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.manage

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.*
import retrofit2.Call
import retrofit2.Response
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.release.data.model.Asset
import universum.studios.gradle.github.release.data.model.Release
import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.release.service.model.RemoteAsset
import universum.studios.gradle.github.release.service.model.RemoteRelease
import universum.studios.gradle.test.kotlin.KotlinArgumentMatchers
import java.io.File

/**
 * @author Martin Albedinsky
 */
class AssetManagerTest : ProjectDependentTest() {

    companion object {

        private val ERROR_RESPONSE_BODY: ResponseBody = "".toResponseBody("".toMediaTypeOrNull())
    }

    @Test fun testContract() {
        // Assert:
        assertThat(AssetManager.NO_RESULT, `is`(-1))
    }

    @Test fun testInstantiation() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        // Act:
        val manager = AssetManager(project, mockApi)
        // Assert:
        assertThat(manager.project, `is`(project))
        assertThat(manager.api, `is`(mockApi))
    }

    @Test fun testUploadAssets() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockCall = mock(RemoteAssetCall::class.java)
        `when`(mockCall.execute()).thenReturn(Response.success(RemoteAsset(name = "test")))
        `when`(mockApi.uploadAsset(anyString(), KotlinArgumentMatchers.any(), anyString())).thenReturn(mockCall)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.uploadAssets(Release(tagName = "test"), listOf(
                createTestAsset(1),
                createTestAsset(2),
                createTestAsset(3),
                createTestAsset(4),
                createTestAsset(5),
                createTestAsset(6)
        )), `is`(6))
        verify(mockApi, times(6)).getRepository()
        verify(mockApi, times(6)).uploadAsset(anyString(), KotlinArgumentMatchers.any(), anyString())
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testUploadEmptyAssets() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.uploadAssets(Release(tagName = "test"), emptyList()), `is`(0))
        verifyZeroInteractions(mockApi)
    }

    @Test fun testUploadAssetAsSuccess() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockCall = mock(RemoteAssetCall::class.java)
        `when`(mockCall.execute()).thenReturn(Response.success(RemoteAsset(name = "test")))
        `when`(mockApi.uploadAsset(anyString(), KotlinArgumentMatchers.any(), anyString())).thenReturn(mockCall)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.uploadAsset(Release(tagName = "test"), Asset(
                id = 1,
                name = "test",
                file = File("test-asset.jar")
        )), `is`(true))
        verify(mockApi).getRepository()
        verify(mockApi).uploadAsset(anyString(), KotlinArgumentMatchers.any(), anyString())
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testUploadAssetAsFailure() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockCall = mock(RemoteAssetCall::class.java)
        `when`(mockCall.execute()).thenReturn(Response.error(400, ERROR_RESPONSE_BODY))
        `when`(mockApi.uploadAsset(anyString(), KotlinArgumentMatchers.any(), anyString())).thenReturn(mockCall)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.uploadAsset(Release(tagName = "test"), createTestAsset(1)), `is`(false))
        verify(mockApi).getRepository()
        verify(mockApi).uploadAsset(anyString(), KotlinArgumentMatchers.any(), anyString())
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testDeleteAssets() {
        // Arrange:
        val assets = listOf(RemoteAsset(name = "test-asset-1"), RemoteAsset(name = "test-asset-2"))
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val release = RemoteRelease(id = 1, assets = assets)
        val mockReleaseCall = mock(RemoteReleaseCall::class.java)
        `when`(mockReleaseCall.execute()).thenReturn(Response.success(release))
        `when`(mockApi.getRelease(1)).thenReturn(mockReleaseCall)
        val mockDeleteCall = mock(VoidCall::class.java)
        `when`(mockDeleteCall.execute()).thenReturn(Response.success(null))
        `when`(mockApi.deleteAsset(KotlinArgumentMatchers.any())).thenReturn(mockDeleteCall)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.deleteAllAssets(Release(id = release.id, tagName = "test")), `is`(2))
        verify(mockApi).getRelease(1)
        assets.forEach { verify(mockApi).deleteAsset(it) }
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testDeleteAssetsOfReleaseWithoutAssets() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val release = RemoteRelease(id = 1)
        val mockReleaseCall = mock(RemoteReleaseCall::class.java)
        `when`(mockReleaseCall.execute()).thenReturn(Response.success(release))
        `when`(mockApi.getRelease(1)).thenReturn(mockReleaseCall)
        val mockDeleteCall = mock(VoidCall::class.java)
        `when`(mockDeleteCall.execute()).thenReturn(Response.success(null))
        `when`(mockApi.deleteAsset(KotlinArgumentMatchers.any())).thenReturn(mockDeleteCall)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.deleteAllAssets(Release(id = 1, tagName = "test")), `is`(AssetManager.NO_RESULT))
        verify(mockApi).getRelease(1)
        verifyNoMoreInteractions(mockApi)
    }

    @Test fun testDeleteAssetsOfNotExistingRelease() {
        // Arrange:
        val mockApi = mock(ReleasesApi::class.java)
        `when`(mockApi.getRepository()).thenReturn(REPOSITORY)
        val mockReleaseCall = mock(RemoteReleaseCall::class.java)
        `when`(mockReleaseCall.execute()).thenReturn(Response.error(404, ERROR_RESPONSE_BODY))
        `when`(mockApi.getRelease(1)).thenReturn(mockReleaseCall)
        val manager = AssetManager(project, mockApi)
        // Act + Assert:
        assertThat(manager.deleteAllAssets(Release(id = 1, tagName = "test")), `is`(AssetManager.NO_RESULT))
        verify(mockApi).getRelease(1)
        verifyNoMoreInteractions(mockApi)
    }

    private fun createTestAsset(identifier: Int) = Asset(
            id = identifier,
            name = "test$identifier",
            file = File("test-asset-$identifier.jar")
    )

    abstract class RemoteReleaseCall : Call<RemoteRelease>

    abstract class RemoteAssetCall : Call<RemoteAsset>

    abstract class VoidCall : Call<Void>
}
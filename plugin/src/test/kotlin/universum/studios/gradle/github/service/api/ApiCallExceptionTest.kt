/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api

import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import okio.BufferedSource
import org.assertj.core.api.Assertions
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import universum.studios.gradle.test.kotlin.KotlinArgumentMatchers
import java.nio.charset.Charset

/**
 * @author Martin Albedinsky
 */
class ApiCallExceptionTest {

    @Test fun testInstantiation() {
        // Arrange:
        val mockErrorBody = mock(ResponseBody::class.java)
        val mockErrorBodySource = mock(BufferedSource::class.java)
        `when`(mockErrorBodySource.readString(KotlinArgumentMatchers.any(Charset::class.java))).thenReturn("{ Authorization failed. }")
        `when`(mockErrorBody.source()).thenReturn(mockErrorBodySource)
        val request = Request.Builder().url("https://github.plugin.test/").build()
        val rawResponse = Response.Builder().request(request).protocol(Protocol.HTTP_1_1).code(401).message("Unauthorized").build()
        // Act:
        val exception = ApiCallException(retrofit2.Response.error<Unit>(mockErrorBody, rawResponse))
        // Assert:
        Assertions.assertThat(exception).hasMessage(
                "GitHubPlugin => GitHub API call 'https://github.plugin.test/' failed with an error:\n" +
                        "HTTP 1.1 401 Unauthorized\n\n" +
                        "{ Authorization failed. }"
        )
    }
}
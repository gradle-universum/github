/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.api

import okhttp3.RequestBody
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Before
import org.junit.Test
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.inner.resourceFile
import universum.studios.gradle.github.release.data.model.Asset
import universum.studios.gradle.github.release.service.model.RemoteAsset
import universum.studios.gradle.github.release.service.model.RemoteRelease
import universum.studios.gradle.github.service.HttpCode

/**
 * @author Martin Albedinsky
 */
class ReleasesApiImplTest : ProjectDependentTest() {

    private companion object {

        val RELEASE = RemoteRelease(
                tag_name = "test",
                target_commitish = "master",
                name = "test",
                body = "Test body."
        )
    }

    // Arrange:
    // Act:
    // Assert:

    private val api = ReleasesApiImpl(REPOSITORY)

    @Before
    override fun beforeTest() {
        super.beforeTest()
        // Ensure that the 'test' release does not exist.
        val existsResponse = api.getTaggedRelease(RELEASE.tag_name).execute()
        if (existsResponse.isSuccessful) {
            api.deleteRelease(RELEASE).execute()
        }
    }

    @Test
    @Throws(Exception::class)
    fun testGetReleases() {
        val response = api.getReleases().execute()
        assertThatResponseIsSuccessful(response)
        val responseBody = response.body() ?: throw AssertionError("Received response without body!")
        assertThat(responseBody.isEmpty(), `is`(false))
    }

    @Test
    @Throws(Exception::class)
    fun testGetRelease() {
        val response = api.getRelease(6558494).execute()
        assertThatResponseIsSuccessful(response)
        val responseBody = response.body() ?: throw AssertionError("Received response without body!")
        assertThat(responseBody.name, `is`("0.0.1"))
    }

    @Test
    @Throws(Exception::class)
    fun testGetTaggedRelease() {
        val response = api.getTaggedRelease("v0.0.1").execute()
        assertThatResponseIsSuccessful(response)
        val responseBody = response.body() ?: throw AssertionError("Received response without body!")
        assertThat(responseBody.tag_name, `is`("v0.0.1"))
        assertThat(responseBody.name, `is`("0.0.1"))
    }

    @Test
    @Throws(Exception::class)
    fun testGetLatestRelease() {
        val response = api.getLatestRelease().execute()
        assertThatResponseIsSuccessful(response)
        val responseBody = response.body() ?: throw AssertionError("Received response without body!")
        assertThat(responseBody.tag_name, `is`("v1.0.0-android-app"))
    }

    @Test
    @Throws(Exception::class)
    fun testCreateDeleteRelease() {
        // CREATE ----------------------------------------------------------------------------------
        val createResponse = api.createRelease(RELEASE).execute()
        assertThatResponseIsSuccessful(createResponse)
        assertThat(createResponse.body()?.tag_name, `is`(RELEASE.tag_name))
        assertThat(createResponse.body()?.target_commitish, `is`(RELEASE.target_commitish))
        assertThat(createResponse.body()?.name, `is`(RELEASE.name))
        assertThat(createResponse.body()?.body, `is`(RELEASE.body))
        // Assert that is created:
        val verifyCreationResponse = api.getTaggedRelease(RELEASE.tag_name).execute()
        assertThatResponseIsSuccessful(verifyCreationResponse)
        // DELETE ----------------------------------------------------------------------------------
        val deleteResponse = api.deleteRelease(createResponse.body()!!).execute()
        assertThatResponseIsSuccessful(deleteResponse)
        // Assert that is deleted:
        val verifyDeletionResponse = api.getTaggedRelease(RELEASE.tag_name).execute()
        assertThatResponseIsFailure(verifyDeletionResponse)
        assertThat(verifyDeletionResponse.code(), `is`(HttpCode.NOT_FOUND))
    }

    @Test
    @Throws(Exception::class)
    fun testEditRelease() {
        // Create if does not exist (test precondition):
        val createResponse = api.createRelease(RELEASE).execute()
        assertThatResponseIsSuccessful(createResponse)
        // EDIT ------------------------------------------------------------------------------------
        val editResponse = api.editRelease(createResponse.body()!!.copy(body = "Updated test body.")).execute()
        assertThatResponseIsSuccessful(editResponse)
        assertThat(editResponse.body()!!.body, `is`("Updated test body."))
        // Assert that is edited:
        val verifyEditionResponse = api.getTaggedRelease(RELEASE.tag_name).execute()
        assertThatResponseIsSuccessful(verifyEditionResponse)
        // Delete the test release (cleanup):
        assertThatResponseIsSuccessful(api.deleteRelease(createResponse.body()!!).execute())
    }

    @Test
    @Throws(Exception::class)
    fun testUploadDeleteAsset() {
        // Create release (test precondition):
        val createResponse = api.createRelease(RELEASE).execute()
        assertThatResponseIsSuccessful(createResponse)
        // UPLOAD ----------------------------------------------------------------------------------
        val uploadResponse = api.uploadAsset(
                String.format(RemoteAsset.UPLOAD_URL_FORMAT,
                        Asset.BASE_UPLOAD_URL,
                        REPOSITORY.owner,
                        REPOSITORY.name,
                        createResponse.body()!!.id,
                        "sources.jar"
                ),
                RequestBody.create(null, resourceFile("/artifact/sources.jar")),
                Asset.CONTENT_TYPE
        ).execute()
        assertThatResponseIsSuccessful(uploadResponse)
        // Verify that is uploaded:
        // -> isn't it enough that the upload response has been successful ???
        // DELETE ----------------------------------------------------------------------------------
        assertThatResponseIsSuccessful(api.deleteAsset(uploadResponse.body()!!).execute())
        // Delete also the test release (cleanup):
        assertThatResponseIsSuccessful(api.deleteRelease(createResponse.body()!!).execute())
    }

    private fun assertThatResponseIsSuccessful(response: retrofit2.Response<*>) {
        assertThat(response, `is`(notNullValue()))
        assertThat(response.isSuccessful, `is`(true))
    }

    private fun assertThatResponseIsFailure(response: retrofit2.Response<*>) {
        assertThat(response, `is`(notNullValue()))
        assertThat(response.isSuccessful, `is`(false))
    }
}
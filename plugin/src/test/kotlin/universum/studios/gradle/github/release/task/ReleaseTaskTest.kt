/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.junit.Test
import org.mockito.Mockito.mock
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.release.service.api.ReleasesApiProvider

/**
 * @author Martin Albedinsky
 */
class ReleaseTaskTest : ProjectDependentTest() {

    @Test fun testApi() {
        // Arrange:
        val task = createTask(TestTask::class.java, "test-group", "testTask")
        val mockApi = mock(ReleasesApi::class.java)
        // Act + Assert:
        task.setApi(mockApi)
        assertThat(task.getApi(), `is`(mockApi))
    }

    @Test(expected = IllegalStateException::class)
    fun testGetApiWhenNotSpecified() {
        // Arrange:
        val task = createTask(TestTask::class.java, "test-group", "testTask")
        // Act:
        task.getApi()
    }

    @Test fun testConfiguratorConfigureTask() {
        // Arrange:
        val repository = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_repo"
            this.accessToken = "test-access-token"
        }
        repository.registerApi(ReleasesApi::class, ReleasesApiProvider.getApi(repository))
        val defaultConfig = DefaultConfigExtension()
        val task = createTask(TestTask::class.java, "test-group", "testTask")
        val configurator = ReleaseTask.BasicConfigurator<TestTask>()
        configurator.setRepository(repository)
        configurator.setDefaultConfig(defaultConfig)
        // Act:
        configurator.configureTask(task)
        // Assert:
        assertThat(task.getRepository(), `is`(repository))
        assertThat(task.getApi(), `is`(ReleasesApiProvider.getApi(repository)))
    }

    open class TestTask : ReleaseTask() {

        override fun onPerform() {}
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.inner

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Before
import universum.studios.gradle.github.GitHubPlugin
import universum.studios.gradle.github.util.readFromFile
import universum.studios.gradle.github.util.writeToFile
import java.io.File

/**
 * @author Martin Albedinsky
 */
abstract class BuildDependentTest : ProjectDependentTest {

    companion object {

        const val GRADLE_VERSION = "6.3"
    }

    private val buildFileContent: String
    lateinit var gradleBuildFile: File
    private val pluginClasspath = resourceFile("/plugin-classpath.txt").readLines().map { File(it) }

    constructor(buildFile: File) : this(readFromFile(buildFile))
    constructor(buildFileContent: String = "") {
        this.buildFileContent = buildFileContent
    }

    @Throws(Exception::class)
    @Before override fun beforeTest() {
        super.beforeTest()
        this.gradleBuildFile = projectDir.newFile("build.gradle")
        writeToFile(gradleBuildFile, buildFileContent)
    }

    fun applyPlugin() {
        this.project.pluginManager.apply(GitHubPlugin.ID)
    }

    fun performSimpleBuild(): BuildResult {
        return prepareBuild().withArguments("tasks").build()
    }

    fun prepareBuild(): GradleRunner {
        return GradleRunner.create()
                .withGradleVersion(GRADLE_VERSION)
                .withProjectDir(projectDir.root)
                .withPluginClasspath(pluginClasspath)
                .withDebug(true)
    }
}
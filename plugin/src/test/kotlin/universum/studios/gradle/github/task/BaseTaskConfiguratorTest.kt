/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.junit.Test
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.inner.ProjectDependentTest

/**
 * @author Martin Albedinsky
 */
class BaseTaskConfiguratorTest : ProjectDependentTest() {

    @Test fun testConfigureTask() {
        // Arrange:
        val repository = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_repo"
            this.accessToken = "test-access-token"
        }
        val defaultConfig = DefaultConfigExtension()
        val configurator = BaseTaskConfigurator<TestTask>()
        configurator.setRepository(repository)
        configurator.setDefaultConfig(defaultConfig)
        // Act:
        val task = configurator.configureTask(createTask(type = TestTask::class.java, name = "testTask"))
        // Assert:
        assertThat(task.getRepository(), `is`(repository))
    }

    open class TestTask : BaseTask() {

        override fun onPerform() {}
    }
}
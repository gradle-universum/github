/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.model

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.IsNull.nullValue
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class RemoteLabelTest {

    @Test fun testInstantiation() {
        // Act:
        val label = RemoteLabel(
                id = 1,
                url = "url",
                name = "test-label",
                color = "ff0000",
                default = false
        )
        // Assert:
        assertThat(label.id, `is`(1))
        assertThat(label.url, `is`("url"))
        assertThat(label.name, `is`("test-label"))
        assertThat(label.color, `is`("ff0000"))
        assertThat(label.default, `is`(false))
    }

    @Test fun testInstantiationWithDefaultValues() {
        // Act:
        val label = RemoteLabel(
                id = 1,
                name = "test-label",
                color = "ff0000"
        )
        // Assert:
        assertThat(label.id, `is`(1))
        assertThat(label.url, `is`(nullValue()))
        assertThat(label.name, `is`("test-label"))
        assertThat(label.color, `is`("ff0000"))
        assertThat(label.default, `is`(nullValue()))
    }

    @Test fun testComponents() {
        // Arrange:
        val label = RemoteLabel(
                id = 1,
                url = "url",
                name = "test-label",
                color = "ff0000",
                default = false
        )
        // Act + Assert:
        assertThat(label.component1(), `is`(1))
        assertThat(label.component2(), `is`("url"))
        assertThat(label.component3(), `is`("test-label"))
        assertThat(label.component4(), `is`("ff0000"))
        assertThat(label.component5(), `is`(false))
    }

    @Test fun testCopy() {
        // Arrange:
        val label = RemoteLabel(
                id = 1,
                url = "url",
                name = "test-label",
                color = "ff0000",
                default = false
        )
        // Act:
        val labelCopy = label.copy()
        // Assert:
        assertThat(labelCopy, `is`(label))
        assertThat(label.copy(id = 2), `is`(not(label)))
    }

    @Test fun testHashCode() {
        // Arrange:
        val label = RemoteLabel(
                id = 1,
                url = "url",
                name = "test-label",
                color = "ff0000",
                default = false
        )
        // Act + Assert:
        assertThat(label.hashCode(), `is`(not(0)))
    }

    @Test fun testEquals() {
        // Arrange + Act + Assert:
        assertThat(
                RemoteLabel(
                        id = 1,
                        url = "url",
                        name = "test-label",
                        color = "ff0000",
                        default = false
                ).equals(RemoteLabel(
                        id = 1,
                        url = "url",
                        name = "test-label",
                        color = "ff0000",
                        default = false
                )),
                `is`(true)
        )
        assertThat(
                RemoteLabel(
                        id = 1,
                        url = "url",
                        name = "test-label",
                        color = "ff0000",
                        default = false
                ).equals(RemoteLabel(
                        id = 2,
                        url = "url",
                        name = "test-label",
                        color = "ff0000",
                        default = false
                )),
                `is`(false)
        )
    }

    @Test fun testToString() {
        // Arrange:
        val label = RemoteLabel(
                id = 1,
                url = "url",
                name = "test-label",
                color = "ff0000",
                default = false
        )
        // Act + Assert:
        assertThat(label.toString(), `is`("RemoteLabel(id=1, url=url, name=test-label, color=ff0000, default=false)"))
    }
}
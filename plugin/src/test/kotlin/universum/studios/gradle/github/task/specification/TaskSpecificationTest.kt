/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task.specification

import org.gradle.api.Task
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class TaskSpecificationTest {

    @Test fun testContract() {
        // Assert:
        assertThat(TaskSpecification.TASK_GROUP, `is`("group"))
        assertThat(TaskSpecification.TASK_TYPE, `is`("type"))
        assertThat(TaskSpecification.TASK_NAME, `is`("name"))
        assertThat(TaskSpecification.TASK_DESCRIPTION, `is`("description"))
        assertThat(TaskSpecification.TASK_DEPENDS_ON, `is`("dependsOn"))
    }

    @Test fun testInstantiation() {
        // Act:
        val specification = TaskSpecification("test-group", Task::class.java, "test-name", "test-description", listOf())
        // Assert:
        assertThat(specification.group, `is`("test-group"))
        assertEquals(specification.type, Task::class.java)
        assertThat(specification.name, `is`("test-name"))
        assertThat(specification.description, `is`("test-description"))
        assertThat(specification.dependsOn, `is`(listOf()))
    }

    @Test fun testInstantiationWithDefaultValues() {
        // Act:
        val specification = TaskSpecification("test-group", Task::class.java, "test-name")
        // Assert:
        assertThat(specification.group, `is`("test-group"))
        assertEquals(specification.type, Task::class.java)
        assertThat(specification.name, `is`("test-name"))
        assertThat(specification.description, `is`(""))
        assertThat(specification.dependsOn, `is`(emptyList()))
    }

    @Test fun testComponents() {
        // Arrange:
        val specification = TaskSpecification("test-group", Task::class.java, "test-name", "test-description", emptyList())
        // Act + Assert:
        assertThat(specification.component1(), `is`("test-group"))
        assertEquals(specification.component2(), Task::class.java)
        assertThat(specification.component3(), `is`("test-name"))
        assertThat(specification.component4(), `is`("test-description"))
        assertThat(specification.component5(), `is`(emptyList()))
    }

    @Test fun testCopy() {
        // Arrange:
        val specification = TaskSpecification("test-group", Task::class.java, "test-name")
        // Act:
        val specificationCopy = specification.copy()
        // Assert:
        assertThat(specificationCopy, `is`(specification))
        assertThat(specification.copy(name = "test-name-copy"), `is`(not(specification)))
    }

    @Test fun testHashCode() {
        // Arrange:
        val specification = TaskSpecification("test-group", Task::class.java, "test-name")
        // Act + Assert:
        assertThat(specification.hashCode(), `is`(not(0)))
    }

    @Test fun testEquals() {
        // Arrange + Act + Assert:
        assertThat(
                TaskSpecification(
                        "test-group",
                        Task::class.java,
                        "test-name"
                ).equals(TaskSpecification(
                        "test-group",
                        Task::class.java,
                        "test-name"
                )),
                `is`(true)
        )
        assertThat(
                TaskSpecification(
                        "test-group",
                        Task::class.java,
                        "test-name-1"
                ).equals(TaskSpecification(
                        "test-group",
                        Task::class.java,
                        "test-name-2"
                )),
                `is`(false)
        )
    }

    @Test fun testToString() {
        // Arrange:
        val specification = TaskSpecification("test-group", Task::class.java, "test-name")
        // Act + Assert:
        assertThat(specification.toString(), `is`(
                "TaskSpecification(group=test-group, type=interface org.gradle.api.Task, name=test-name, description=, dependsOn=[])"
        ))
    }
}
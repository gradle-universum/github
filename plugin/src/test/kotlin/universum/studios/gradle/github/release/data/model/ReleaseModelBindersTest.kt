/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Test
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension

/**
 * @author Martin Albedinsky
 */
class ReleaseModelBindersTest{

    @Test fun testBinderInstances() {
        // Assert:
        assertThat(ReleaseModelBinders.RELEASE_TYPE_TO_RELEASE, `is`(notNullValue()))
    }

    @Test fun testReleaseTypeToRelease() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("testReleaseType")
                .tagName("test-type-tag")
                .targetCommitish("develop")
                .releaseName("testTypeReleaseName")
                .releaseBody("Test type release body.")
                .draft(true)
                .preRelease(false)
                .overwrite(true)
        val release = Release(
                tagName = "test-tag",
                name = "test-release",
                body = "Test release body."
        )
        // Act:
        val boundRelease = ReleaseModelBinders.RELEASE_TYPE_TO_RELEASE.bind(releaseType, release)
        // Assert:
        assertThat(boundRelease, `is`(not(release)))
        assertThat(boundRelease.tagName, `is`(releaseType.tagName()))
        assertThat(release.tagName, `is`("test-tag"))
        assertThat(boundRelease.name, `is`(releaseType.releaseName()))
        assertThat(release.name, `is`("test-release"))
        assertThat(boundRelease.body, `is`(releaseType.releaseBody()))
        assertThat(release.body, `is`("Test release body."))
        assertThat(boundRelease.draft, `is`(releaseType.draft()))
        assertThat(release.draft, `is`(false))
        assertThat(boundRelease.preRelease, `is`(releaseType.preRelease()))
        assertThat(release.preRelease, `is`(false))
        assertThat(boundRelease.overwrite, `is`(releaseType.overwrite()))
        assertThat(release.overwrite, `is`(false))
    }

    @Test fun testReleaseTypeToReleaseWithEmptyExtensionValues() {
        // Arrange:
        val release = Release(
                tagName = "test-tag",
                targetCommitish = "develop",
                name = "testReleaseName",
                body = "Test release body.",
                draft = true,
                preRelease = false,
                overwrite = true
        )
        // Act:
        val boundRelease = ReleaseModelBinders.RELEASE_TYPE_TO_RELEASE.bind(ReleaseTypeExtension("testReleaseType"), release)
        // Assert:
        assertThat(boundRelease, `is`(release))
    }
}
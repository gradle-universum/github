/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.not
import org.junit.Test
import java.io.File

/**
 * @author Martin Albedinsky
 */
class AssetTest {

    @Test fun testContract() {
        // Assert:
        assertThat(Asset.CONTENT_TYPE, `is`("application/octet-stream"))
        assertThat(Asset.BASE_UPLOAD_URL, `is`("https://uploads.github.com/"))
    }

    @Test fun testInstantiation() {
        // Act:
        val asset = Asset(
                id = 1,
                name = "test-asset",
                file = File("test-asset.txt"),
                fileContentType = "test/content",
                baseUploadUrl = "https://api.github.com"
        )
        // Assert:
        assertThat(asset.id, `is`(1))
        assertThat(asset.name, `is`("test-asset"))
        assertThat(asset.file, `is`(File("test-asset.txt")))
        assertThat(asset.fileContentType, `is`("test/content"))
        assertThat(asset.baseUploadUrl, `is`("https://api.github.com"))
    }

    @Test fun testInstantiationWithDefaultValues() {
        // Act:
        val asset = Asset(name = "test-asset", file = File("test-asset.txt"))
        // Assert:
        assertThat(asset.id, `is`(0))
        assertThat(asset.name, `is`("test-asset"))
        assertThat(asset.file, `is`(File("test-asset.txt")))
        assertThat(asset.fileContentType, `is`(Asset.CONTENT_TYPE))
        assertThat(asset.baseUploadUrl, `is`(Asset.BASE_UPLOAD_URL))
    }

    @Test fun testComponents() {
        // Arrange:
        val asset = Asset(
                id = 1,
                name = "test-asset",
                file = File("test-asset.txt"),
                fileContentType = "test/content",
                baseUploadUrl = "https://api.github.com"
        )
        // Act + Assert:
        assertThat(asset.component1(), `is`(1))
        assertThat(asset.component2(), `is`("test-asset"))
        assertThat(asset.component3(), `is`(File("test-asset.txt")))
        assertThat(asset.component4(), `is`("test/content"))
        assertThat(asset.component5(), `is`("https://api.github.com"))
    }

    @Test fun testCopy() {
        // Arrange:
        val asset = Asset(name = "test-asset", file = File("test-asset.txt"))
        // Act:
        val assetCopy = asset.copy()
        // Assert:
        assertThat(assetCopy, `is`(asset))
        assertThat(asset.copy(name = "test-asset-copy"), `is`(not(asset)))
    }

    @Test fun testHashCode() {
        // Arrange:
        val asset = Asset(name = "test-asset", file = File("test-asset.txt"))
        // Act + Assert:
        assertThat(asset.hashCode(), `is`(not(0)))
    }

    @Test fun testEquals() {
        // Arrange + Act + Assert:
        assertThat(
                Asset(
                        name = "test-asset",
                        file = File("test-asset.txt")
                ).equals(Asset(
                        name = "test-asset",
                        file = File("test-asset.txt"))
                ),
                `is`(true)
        )
        assertThat(
                Asset(
                        name = "test-asset-1",
                        file = File("test-asset.txt")
                ).equals(Asset(
                        name = "test-asset-2",
                        file = File("test-asset.txt"))
                ),
                `is`(false)
        )
    }

    @Test fun testToString() {
        // Arrange:
        val asset = Asset(
                id = 1,
                name = "test-asset",
                file = File("test-asset.txt"),
                fileContentType = "test/content",
                baseUploadUrl = "https://api.github.com"
        )
        // Act + Assert:
        assertThat(asset.toString(), `is`(
                "Asset(id=1, name=test-asset, file=test-asset.txt, fileContentType=test/content, baseUploadUrl=https://api.github.com)"
        ))
    }

    @Test fun testNameBuilderBuild() {
        // Arrange:
        val builder = Asset.NameBuilder()
                .name("test-artifact")
                .version("1.0.0")
                .classifier("sources")
                .type("jar")
        // Act + Assert:
        assertThat(builder.build(), `is`("test-artifact-1.0.0-sources.jar"))
    }

    @Test fun testNameBuilderBuildWithoutVersion() {
        // Arrange:
        val builder = Asset.NameBuilder()
                .name("test-artifact")
                .classifier("sources")
                .type("jar")
        // Act + Assert:
        assertThat(builder.build(), `is`("test-artifact-sources.jar"))
    }

    @Test fun testNameBuilderBuildWithoutClassifier() {
        // Arrange:
        val builder = Asset.NameBuilder()
                .name("test-artifact")
                .version("1.0.0")
                .type("jar")
        // Act + Assert:
        assertThat(builder.build(), `is`("test-artifact-1.0.0.jar"))
    }

    @Test fun testNameBuilderBuildWithoutType() {
        // Arrange:
        val builder = Asset.NameBuilder()
                .name("test-artifact")
                .version("1.0.0")
                .classifier("sources")
        // Act + Assert:
        assertThat(builder.build(), `is`("test-artifact-1.0.0-sources"))
    }

    @Test fun testNameBuilderBuildWithoutAnyProperty() {
        // Arrange:
        val builder = Asset.NameBuilder()
        // Act + Assert:
        assertThat(builder.build(), `is`(""))
    }
}
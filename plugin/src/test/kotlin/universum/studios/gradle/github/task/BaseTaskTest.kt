/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.junit.Test
import universum.studios.gradle.github.inner.ProjectDependentTest

/**
 * @author Martin Albedinsky
 */
class BaseTaskTest : ProjectDependentTest() {

    @Test(expected = IllegalStateException::class)
    fun testGetRepositoryWhenNotSpecified() {
        // Arrange:
        val task = createTask(type = TestTask::class.java, name = "testTask")
        // Act:
        task.getRepository()
    }

    @Test fun testPerform() {
        // Arrange:
        val task = createTask(type = TestTask::class.java, name = "testTask")
        task.setRepository(REPOSITORY)
        // Act:
        task.perform()
        // Assert:
        assertThat(task.onValidatePropertiesCalled, `is`(true))
        assertThat(task.onPerformCalled, `is`(true))
    }

    @Test(expected = IllegalStateException::class)
    fun testPerformWithoutSpecifiedRepository() {
        // Arrange:
        val task = createTask(type = TestTask::class.java, name = "testTask")
        // Act:
        task.perform()
    }

    @Test fun testPropertyValuesReportInstantiation() {
        // Act:
        val valuesReport = BaseTask.PropertyValuesReport()
        // Assert:
        assertThat(valuesReport.isSuccess(), `is`(true))
    }

    @Test fun testPropertyValuesReportIsSuccess() {
        // Arrange:
        val valuesReport = BaseTask.PropertyValuesReport().missingValueFor("property1")
        // Act + Assert:
        assertThat(valuesReport.isSuccess(), `is`(false))
    }

    @Test fun testPropertyValuesReportNamesOfMissingProperties() {
        // Arrange:
        val valuesReport = BaseTask.PropertyValuesReport().missingValueFor("property1").missingValueFor("property2")
        // Act + Assert:
        assertThat(valuesReport.namesOfMissingProperties(), `is`(setOf("property1", "property2")))
    }

    @Test fun testPropertyValuesReportNamesOfMissingPropertiesOnEmptyReport() {
        // Arrange:
        val valuesReport = BaseTask.PropertyValuesReport()
        // Act + Assert:
        assertThat(valuesReport.namesOfMissingProperties(), `is`(emptySet<String>()))
    }

    open class TestTask : BaseTask() {

        var onValidatePropertiesCalled = false
        var onPerformCalled = false

        override fun onValidateProperties(): PropertyValuesReport {
            this.onValidatePropertiesCalled = true
            return super.onValidateProperties()
        }

        override fun onPerform() {
            this.onPerformCalled = true
        }
    }
}
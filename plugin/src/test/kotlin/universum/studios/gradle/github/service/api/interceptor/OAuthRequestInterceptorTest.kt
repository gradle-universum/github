/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service.api.interceptor

import okhttp3.Request
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsNull.nullValue
import org.junit.Test
import universum.studios.gradle.github.service.api.TestChain

/**
 * @author Martin Albedinsky
 */
class OAuthRequestInterceptorTest {

    @Test fun testIntercept() {
        // Arrange:
        val interceptor = OAuthRequestInterceptor()
        interceptor.setToken("OAuthToken")
        val request = Request.Builder().url("https://github.plugin.test/").build()
        val chain = TestChain(request)
        // Act:
        val response =  interceptor.intercept(chain)
        // Assert:
        assertThat(response, `is`(notNullValue()))
        val responseRequest = response.request
        assertThat(responseRequest, `is`(not(request)))
        val requestHeaders = responseRequest.headers
        assertThat(requestHeaders, `is`(notNullValue()))
        assertThat(requestHeaders["Authorization"], `is`("token OAuthToken"))
    }

    @Test fun testInterceptWithoutToken() {
        // Arrange:
        val interceptor = OAuthRequestInterceptor()
        val request = Request.Builder().url("https://github.plugin.test/").build()
        val chain = TestChain(request)
        // Act:
        val response =  interceptor.intercept(chain)
        // Assert:
        assertThat(response, `is`(notNullValue()))
        val responseRequest = response.request
        assertThat(responseRequest, `is`(request))
        val requestHeaders = responseRequest.headers
        assertThat(requestHeaders, `is`(notNullValue()))
        assertThat(requestHeaders["Authorization"], `is`(nullValue()))
    }
}
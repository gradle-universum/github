/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import org.gradle.api.DefaultTask
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.core.IsInstanceOf.instanceOf
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Test
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * @author Martin Albedinsky
 */
class ProjectTaskFactoryTest : ProjectDependentTest() {

    @Test fun testCreateTask() {
        // Act:
        val task = ProjectTaskFactory.createTask(project, TaskSpecification("test-group", TestTask::class.java, "test-task", "test-description"))
        // Assert:
        assertThat(task, `is`(notNullValue()))
        assertThat(task, instanceOf(TestTask::class.java))
        assertThat(task.group, `is`("test-group"))
        assertThat(task.name, `is`("test-task"))
        assertThat(task.description, `is`("test-description"))
    }

    open class TestTask : DefaultTask()
}
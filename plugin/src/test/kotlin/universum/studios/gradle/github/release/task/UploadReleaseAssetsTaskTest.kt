/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import org.gradle.api.artifacts.PublishArtifact
import org.gradle.api.internal.artifacts.publish.DefaultPublishArtifact
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import universum.studios.gradle.github.extension.extensionResourceFile
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension
import java.io.File
import java.util.*

/**
 * @author Martin Albedinsky
 */
class UploadReleaseAssetsTaskTest : BuildDependentTest(extensionResourceFile("releases.groovy")) {

    companion object {

        val UPLOAD_RELEASE_TASK_SPECIFICATION = UploadReleaseTask.createSpecification(ReleaseTypeExtension("test").apply {
            tagName("test")
            releaseName("test")
            releaseBody("Test release body.")
            overwrite(true)
        })
    }

    @Test fun testCreateSpecification() {
        (1..10).forEach { index ->
            // Arrange:
            val releaseType = ReleaseTypeExtension("${index}Release").apply {
                tagName("$index.0.0")
                releaseName("$index.0.0")
                releaseBody("Release body for $index.0.0.")
            }
            // Act:
            val specification = UploadReleaseAssetsTask.createSpecification(releaseType)
            // Assert:
            assertThat(specification, `is`(notNullValue()))
            assertEquals(specification.type, UploadReleaseAssetsTask::class.java)
            assertThat(specification.name, `is`("githubUpload${releaseType.name}ReleaseAssets"))
            assertThat(specification.description, `is`("Uploads (updates) assets associated with '${releaseType.name}' release for the project's GitHub repository."))
        }
    }

    @Test fun testCreateAssetsForUpload() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("TestRelease").apply {
            tagName("1.0.0")
            releaseName("1.0.0")
            artifactId("test-artifact")
            artifactVersion("1.0.0")
        }
        val publishArtifacts = listOf<PublishArtifact>(
                DefaultPublishArtifact("artifact", "", "jar", "sources", Date(), File("")),
                DefaultPublishArtifact("artifact", "", "html", "javadoc", Date(), File(""))
        )
        // Act:
        val assets = UploadReleaseAssetsTask.createAssetsForUpload(releaseType, publishArtifacts, "https://github.com/artifacts")
        // Assert:
        assertThat(assets, `is`(notNullValue()))
        assertThat(assets.size, `is`(publishArtifacts.size))
        assertThat(assets[0].name, `is`("test-artifact-1.0.0-sources.jar"))
        assertThat(assets[0].baseUploadUrl, `is`("https://github.com/"))
        assertThat(assets[1].name, `is`("test-artifact-1.0.0-javadoc.html"))
        assertThat(assets[1].baseUploadUrl, `is`("https://github.com/"))
    }

    @Test fun testCreateAssetsForUnknownReleaseType() {
        // Arrange:
        val publishArtifacts = listOf<PublishArtifact>(
                DefaultPublishArtifact("artifact#1", "", "jar", "sources", Date(), File("")),
                DefaultPublishArtifact("artifact#2", "", "html", "javadoc", Date(), File(""))
        )
        // Act:
        val assets = UploadReleaseAssetsTask.createAssetsForUpload(null, publishArtifacts, "")
        // Assert:
        assertThat(assets, `is`(notNullValue()))
        assertThat(assets.size, `is`(publishArtifacts.size))
        assertThat(assets[0].name, `is`("artifact#1-sources.jar"))
        assertThat(assets[0].baseUploadUrl, `is`(""))
        assertThat(assets[1].name, `is`("artifact#2-javadoc.html"))
        assertThat(assets[1].baseUploadUrl, `is`(""))
    }

    @Test fun testCreateAssetsForUploadForEmptyPublishArtifacts() {
        // Arrange:
        val releaseType = ReleaseTypeExtension("TestRelease").apply {
            tagName("1.0.0")
            releaseName("1.0.0")
            artifactId("test.artifact")
            artifactVersion("1.0.0")
        }
        // Act:
        val assets = UploadReleaseAssetsTask.createAssetsForUpload(releaseType, emptyList(), "")
        // Assert:
        assertThat(assets, `is`(notNullValue()))
        assertThat(assets.isEmpty(), `is`(true))
    }

    @Ignore("Improperly configured...")
    @Test fun testPerform() {
        // Arrange:
        applyPlugin()
        // todo:
    }

    @Ignore("Improperly configured...")
    @Test fun testPerformForNotExistingRelease() {
        // Arrange:
        applyPlugin()
        // todo:
    }

    private fun deleteTestRelease() {
        prepareBuild().withArguments("tasks", DeleteTaggedReleaseTask.SPECIFICATION.name, "-Ptag=test").build()
    }
}
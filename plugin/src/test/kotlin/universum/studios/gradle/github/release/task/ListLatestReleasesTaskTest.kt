/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import org.assertj.core.api.Assertions
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertEquals
import org.junit.Test
import universum.studios.gradle.github.extension.extensionResourceFile
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * @author Martin Albedinsky
 */
class ListLatestReleasesTaskTest : BuildDependentTest(extensionResourceFile("releases.groovy")) {

    @Test fun testSpecification() {
        // Arrange:
        val specification = ListLatestReleaseTask.SPECIFICATION
        // Act + Assert:
        assertThat(specification, `is`(notNullValue()))
        assertThat(specification.group, `is`(TaskSpecification.GROUP))
        assertEquals(specification.type, ListLatestReleaseTask::class.java)
        assertThat(specification.name, `is`("githubListLatestRelease"))
        assertThat(specification.description, `is`("Lists the latest published full release for the project's GitHub repository."))
        assertThat(specification.dependsOn, `is`(emptyList()))
    }

    @Test fun testPerform() {
        // Arrange:
        applyPlugin()
        // Act:
        val buildResult = prepareBuild().withArguments("tasks", ListLatestReleaseTask.SPECIFICATION.name).build()
        // Assert:
        Assertions.assertThat(buildResult.output).contains("Latest release for '${REPOSITORY.path()}':")
    }
}
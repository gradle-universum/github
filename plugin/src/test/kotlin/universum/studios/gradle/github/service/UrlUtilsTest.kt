/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.service

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.junit.Test

/**
 * @author Martin Albedinsky
 */
class UrlUtilsTest {

    @Test fun testContract() {
        // Assert:
        assertThat(UrlUtils.BASE_URL_FORMAT, `is`("%s://%s/"))
    }

    @Test fun testParseBaseUrl() {
        // Act + Assert:
        assertThat(UrlUtils.parseBaseUrl("https://www.google.com?query=beautiful_images"), `is`("https://www.google.com/"))
        assertThat(UrlUtils.parseBaseUrl("https://www.google.com"), `is`("https://www.google.com/"))
        assertThat(UrlUtils.parseBaseUrl(""), `is`(""))
    }
}
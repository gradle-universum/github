/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.task

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Test
import org.mockito.Mockito.*
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.inner.ProjectDependentTest
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * @author Martin Albedinsky
 */
class TaskManagerTest : ProjectDependentTest() {

    @Test fun testInstantiation() {
        // Act:
        val manager = TaskManager(project)
        // Assert:
        assertThat(manager.project, `is`(project))
    }

    @Test fun testRegisterTaskConfigurator() {
        // Arrange:
        val repository = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_repo"
            this.accessToken = "test-access-token"
        }
        val defaultConfig = DefaultConfigExtension()
        val manager = TaskManager(project)
        manager.setRepository(repository)
        manager.setDefaultConfig(defaultConfig)
        val mockConfigurator = mock(TestConfigurator::class.java)
        // Act:
        manager.registerTaskConfigurator(TaskSpecification(
                group = "test",
                type = TestTask::class.java,
                name = "test"
        ), mockConfigurator)
        // Assert:
        verify(mockConfigurator).setRepository(repository)
        verify(mockConfigurator).setDefaultConfig(defaultConfig)
        verifyNoMoreInteractions(mockConfigurator)
    }

    @Test fun testCreateAndConfigureTask() {
        // Arrange:
        val repository = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_repo"
            this.accessToken = "test-access-token"
        }
        val defaultConfig = DefaultConfigExtension()
        val manager = TaskManager(project)
        manager.setRepository(repository)
        manager.setDefaultConfig(defaultConfig)
        val taskSpecification = TaskSpecification(
                group = "test",
                type = TestTask::class.java,
                name = "test"
        )
        manager.registerTaskConfigurator(taskSpecification, BaseTaskConfigurator())
        // Act:
        val task = manager.createAndConfigureTask(taskSpecification)
        // Assert:
        assertThat(task.getRepository(), `is`(repository))
    }

    @Test fun testConfigureTaskWithoutRegisteredConfigurator() {
        // Arrange:
        val repository = RepositoryExtension().apply {
            this.owner = "test-owner"
            this.name = "test_repo"
            this.accessToken = "test-access-token"
        }
        val defaultConfig = DefaultConfigExtension()
        val manager = TaskManager(project)
        manager.setRepository(repository)
        manager.setDefaultConfig(defaultConfig)
        // Act:
        val task =  manager.createAndConfigureTask(TaskSpecification(
                group = "test",
                type = TestTask::class.java,
                name = "test"
        ))
        // Assert:
        assertThat(task, `is`(notNullValue()))
    }

    open class TestConfigurator : BaseTaskConfigurator<TestTask>()

    open class TestTask : BaseTask() {

        override fun onPerform() {}
    }
}
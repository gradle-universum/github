/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.task

import org.assertj.core.api.Assertions
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertEquals
import org.junit.Test
import universum.studios.gradle.github.extension.extensionResourceFile
import universum.studios.gradle.github.inner.BuildDependentTest
import universum.studios.gradle.github.label.service.api.LabelsApiProvider
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * @author Martin Albedinsky
 */
class CreateLabelsTaskTest : BuildDependentTest(extensionResourceFile("labels.groovy")) {

    @Test fun testSpecification() {
        // Arrange:
        val specification = CreateLabelsTask.SPECIFICATION
        // Act + Assert:
        assertThat(specification, `is`(notNullValue()))
        assertThat(specification.group, `is`(TaskSpecification.GROUP))
        assertEquals(specification.type, CreateLabelsTask::class.java)
        assertThat(specification.name, `is`("githubCreateLabels"))
        assertThat(specification.description, `is`("Creates specified issue labels for the project's GitHub repository."))
        assertThat(specification.dependsOn, `is`(emptyList()))
    }

    @Test fun testPerform() {
        // Arrange:
        applyPlugin()
        // Delete all labels.
        prepareBuild().withArguments("tasks", DeleteLabelsTask.SPECIFICATION.name).build()
        // Act:
        val buildResult = prepareBuild().withArguments("tasks", CreateLabelsTask.SPECIFICATION.name).build()
        // Assert:
        Assertions.assertThat(buildResult.output).contains(
                "Successfully created labels for '${REPOSITORY.path()}' in count '1'."
        )
        // Verify that the labels are created on the GitHub server.
        val response = LabelsApiProvider.getApi(REPOSITORY).getLabels().execute()
        assertThat(response.isSuccessful, `is`(true))
        assertThat(response.body()!!.size, `is`(1))
        // Cleanup:
        // Create default labels to return to the default state.
        prepareBuild().withArguments("tasks", CreateDefaultLabelsTask.SPECIFICATION.name).build()
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.api

import retrofit2.Call
import universum.studios.gradle.github.label.service.model.RemoteLabel
import universum.studios.gradle.github.service.api.Api

/**
 * Interface declaring layer that may be used to perform calls upon the GitHub Labels API.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface LabelsApi : Api {

    /**
     * Creates a call for request to GET all issue labels for repository specified for this api.
     *
     * @return Call ready to be executed.
     */
    fun getLabels(): Call<List<RemoteLabel>>

    /**
     * Creates a call for request to GET a single issue label with a desired *name* for repository
     * specified for this api
     *
     * @param name The name for which to obtain its associated label.
     * @return Call ready to be executed.
     */
    fun getNamedLabel(name: String): Call<RemoteLabel>

    /**
     * Creates a call for request to CREATE a single issue label for repository specified for this api.
     *
     * @param label The desired label to be created.
     * @return Call ready to be executed.
     */
    fun createLabel(label: RemoteLabel): Call<RemoteLabel>

    /**
     * Creates a call for request to DELETE a single issue label for repository specified for this api.
     *
     * @param label The desired label to be deleted.
     * @return Call ready to be executed.
     */
    fun deleteLabel(label: RemoteLabel): Call<Void>
}
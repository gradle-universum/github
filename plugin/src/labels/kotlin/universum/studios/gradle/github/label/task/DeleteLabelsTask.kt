/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.task

import universum.studios.gradle.github.label.manage.LabelManager
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * A [LabelsTask] implementation which deletes all issue labels on the GitHub server for the specified
 * [repository].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class DeleteLabelsTask : LabelsTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Specification for [DeleteLabelsTask].
         */
        val SPECIFICATION = TaskSpecification(
                type = DeleteLabelsTask::class.java,
                name = "githubDeleteLabels",
                description = "Deletes all issue labels for the project's GitHub repository."
        )
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPerform() {
        val repository = getRepository()
        when (getLabelManager().deleteAllLabels()) {
            LabelManager.NO_RESULT -> project.logger.quiet("No labels for '${repository.path()}' found to be deleted.")
            else -> project.logger.quiet("Labels for '${repository.path()}' has been successfully deleted.")
        }
    }

    /*
     * Inner classes ===============================================================================
     */
}
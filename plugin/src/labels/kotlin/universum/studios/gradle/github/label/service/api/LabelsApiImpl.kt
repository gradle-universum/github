/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.api

import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.label.service.model.RemoteLabel
import universum.studios.gradle.github.service.api.BaseApi

/**
 * Implementation of [LabelsApi].
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param repository The repository for which may be performed service calls through the api.
 * @constructor Creates a new instance of LabelsApiImpl.
 */
internal class LabelsApiImpl(repository: RepositoryExtension) : BaseApi<LabelsServices>(repository, LabelsServices::class.java), LabelsApi {

    /*
     */
    override fun getLabels() = services.getLabels(
            repository.owner,
            repository.name
    )

    /*
     */
    override fun getNamedLabel(name: String) = services.getNamedLabel(
            repository.owner,
            repository.name,
            name
    )

    /*
     */
    override fun createLabel(label: RemoteLabel) = services.postLabel(
            repository.owner,
            repository.name,
            label
    )

    /*
     */
    override fun deleteLabel(label: RemoteLabel) = services.deleteLabel(
            repository.owner,
            repository.name,
            label.name
    )
}
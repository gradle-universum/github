/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.task

import universum.studios.gradle.github.label.data.model.Label
import universum.studios.gradle.github.label.data.model.LabelModelMappers
import universum.studios.gradle.github.label.extension.LabelItemExtension
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * A [LabelsTask] implementation which creates a collection of specified labels on the GitHub
 * server for the specified [repository].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class CreateLabelsTask : LabelsTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Specification for [CreateLabelsTask].
         */
        val SPECIFICATION = TaskSpecification(
                type = CreateLabelsTask::class.java,
                name = "githubCreateLabels",
                description = "Creates specified issue labels for the project's GitHub repository."
        )
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Collection of labels that should be created.
     */
    var labels = emptyList<Label>()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPerform() {
        val repository = getRepository()
        if (labels.isNotEmpty()) {
            project.logger.info("Creating labels for '${repository.path()}' ...")
            val result = getLabelManager().createLabels(labels)
            project.logger.quiet("Successfully created labels for '${repository.path()}' in count '$result'.")
        } else {
            project.logger.quiet("No labels specified for '${repository.path()}' to be created.")
        }
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [LabelsTask.BasicConfigurator] implementation that may be used for configuration of
     * [CreateLabelsTask]s based on the supplied collection of *label* items.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @property labels Collection of label items for which to create issue labels.
     * @constructor Creates a new instance of Configurator with the specified *labels*.
     */
    class Configurator(val labels: Collection<LabelItemExtension>) : BasicConfigurator<CreateLabelsTask>() {

        /*
         */
        override fun configureTask(task: CreateLabelsTask): CreateLabelsTask {
            super.configureTask(task)
            task.labels = labels.map { LabelModelMappers.LABEL_ITEM_TO_LABEL.map(it) }
            return task
        }
    }
}
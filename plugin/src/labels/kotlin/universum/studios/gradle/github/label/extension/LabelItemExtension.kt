/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.extension

/**
 * Extension used to define a desired label item.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param name Name of the label item.
 * @constructor Creates a new instance of LabelItemExtension with the specified *name*.
 */
open class LabelItemExtension(var name: String) {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Color used to represent the label graphically.
     */
    private var color: String = "ffffff"

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets a name for the label.
     *
     * @param name The desired label name.
     * @return This extension to allow methods chaining.
     */
    fun name(name: String): LabelItemExtension {
        this.name = name
        return this
    }

    /**
     * Returns the name specified for this label item.
     *
     * @return This label item's name.
     */
    fun name() = name

    /**
     * Sets a color used to represent the label graphically.
     *
     * Default value: *ffffff (white)*
     *
     * @param color The desired color string in hexadecimal format without *#* symbol.
     * @return This extension to allow methods chaining.
     */
    fun color(color: String): LabelItemExtension {
        this.color = color
        return this
    }

    /**
     * Returns the color specified for this label item.
     *
     * @return This label item's color string in hexadecimal format.
     */
    fun color() = color

    /*
     * Inner classes ===============================================================================
     */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.api

import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.label.service.api.LabelsApiProvider.getApi
import universum.studios.gradle.github.release.service.api.ReleasesApi

/**
 * Simple class which provides access to implementation of [ReleasesApi] which may be used for
 * invocation of services provided via GitHub Releases API. Use [getApi] to gain access to the API
 * implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object LabelsApiProvider {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Api instances created by this provider mapped to theirs associated repositories.
     */
    private val apis = hashMapOf<RepositoryExtension, LabelsApi>()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Provides an instance of LabelsApi implementation for the specified *repository*.
     *
     * @param repository The repository for which may be performed service calls through the api.
     * @return Implementation of the API ready to be used.
     */
    fun getApi(repository: RepositoryExtension): LabelsApi {
        var api = apis[repository]
        if (api == null) {
            api = LabelsApiImpl(repository)
            this.apis[repository] = api
        }
        return api
    }

    /*
     * Inner classes ===============================================================================
     */
}
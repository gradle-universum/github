/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.data.model

import universum.studios.gradle.github.data.model.ModelMapper
import universum.studios.gradle.github.label.extension.LabelItemExtension
import universum.studios.gradle.github.label.service.model.RemoteLabel

/**
 * Static repository for [ModelMapper] implementations used across the plugin for purpose of
 * **labels** extension.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object LabelModelMappers {

    /**
     * A [ModelMapper] implementation which may be used to map [LabelItemExtension] model to
     * [Label] model.
     */
    val LABEL_ITEM_TO_LABEL = object : ModelMapper<LabelItemExtension, Label> {

        /*
         */
        override fun map(from: LabelItemExtension) = Label(
                name = from.name(),
                color = from.color()
        )
    }

    /**
     * A [ModelMapper] implementation which may be used to map [Label] model to [RemoteLabel] model.
     */
    val LABEL_TO_REMOTE_LABEL = object : ModelMapper<Label, RemoteLabel> {

        /*
         */
        override fun map(from: Label) = RemoteLabel(from.id).apply {
            this.name = from.name
            this.color = from.color
        }
    }
}
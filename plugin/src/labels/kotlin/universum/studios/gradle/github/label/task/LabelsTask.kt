/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.task

import universum.studios.gradle.github.label.manage.LabelManager
import universum.studios.gradle.github.label.service.api.LabelsApi
import universum.studios.gradle.github.task.BaseTask
import universum.studios.gradle.github.task.BaseTaskConfigurator

/**
 * A [BaseTask] implementation that is used as base for all tasks provided by
 * [LabelsExtension][universum.studios.gradle.github.label.extension.LabelsExtension]
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class LabelsTask : BaseTask() {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * API implementation that may be used by this task to perform requests to the GitHub Labels API.
     */
    private var api: LabelsApi? = null

    /**
     * Manager that may be used by inheritance hierarchies of this task to create, update, delete
     * issue labels on the GitHub server.
     */
    private lateinit var labelManager: LabelManager

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets an API implementation which should be used by this task to perform requests to the
     * GitHub Labels API.
     *
     * @param api The API implementation to be used by this task.
     */
    internal fun setApi(api: LabelsApi) {
        this.api = api
        onApiChanged(api)
    }

    /**
     * Invoked whenever the specified *api* has been changed for this task.
     *
     * @param api The api that has been attached to this task.
     */
    internal open fun onApiChanged(api: LabelsApi) {
        this.labelManager = LabelManager(project, api)
        // Inheritance hierarchies may perform here operations related to the changed api.
    }

    /**
     * Provides access to the implementation of [LabelsApi] which may be used to perform requests
     * targeting the GitHub Labels API.
     *
     * @return The API implementation.
     * @throws IllegalStateException If there is no API attached to this task.
     */
    internal fun getApi(): LabelsApi {
        return api ?: throw IllegalStateException("No API attached!")
    }

    /**
     * Returns the label manager which may be used to create, update or delete labels for the
     * repository specified for this task.
     *
     * @return Label manager ready to be used.
     */
    internal fun getLabelManager() = labelManager

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [BaseTaskConfigurator] implementation that may be used for basic configuration of tasks that
     * extend from [LabelsTask].
     *
     * This configurator along base configuration also attaches an implementation of [LabelsApi]
     * to each task passed to [configureTask] so the task may perform requests to the
     * GitHub Labels API.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    open class BasicConfigurator<T : LabelsTask> : BaseTaskConfigurator<T>() {

        /*
         */
        override fun configureTask(task: T): T {
            super.configureTask(task)
            task.setApi(getRepository().findApi(LabelsApi::class) ?: throw IllegalStateException("No LabelsApi found!"))
            return task
        }
    }
}
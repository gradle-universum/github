/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.manage

import org.gradle.api.Project
import universum.studios.gradle.github.label.data.model.Label
import universum.studios.gradle.github.label.data.model.LabelModelMappers
import universum.studios.gradle.github.label.service.api.LabelsApi

/**
 * Manager which may be used for issue labels management (uploading, editing, deleting).
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property project The project to which is the parent plugin applied.
 * @property api The api that should be used by the manager to perform calls to the GitHub Labels API.
 * @constructor Creates a new instance of LabelManager with the specified *api*.
 */
internal class LabelManager(val project: Project, val api: LabelsApi) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Constant used to identify no result.
         */
        const val NO_RESULT = -1
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /**
     * Creates all the given *labels* on the GitHub server.
     *
     * @param labels List of labels to be created.
     * @return Count of the successfully created labels.
     */
    fun createLabels(labels: List<Label>): Int {
        var result = 0
        if (labels.isNotEmpty()) labels.forEach {
            if (createLabel(it)) result++
        }
        return result
    }

    /**
     * Creates the specified *label* on the GitHub server.
     *
     * @param label The desired label to be created.
     * @return *True* if the requested label has been successfully created, *false* otherwise.
     */
    fun createLabel(label: Label): Boolean {
        val repository = api.getRepository()
        project.logger.info("Creating label '${label.name}' for '${repository.path()}' ...")
        val response = api.createLabel(LabelModelMappers.LABEL_TO_REMOTE_LABEL.map(label)).execute()
        when (response.isSuccessful) {
            true -> project.logger.quiet("Label '${label.name}' successfully created for '${repository.path()}'.")
            false -> project.logger.error("Failed to create label '${label.name}' for '${repository.path()}'!")
        }
        return response.isSuccessful
    }

    /**
     * Deletes all issue labels defined on the GitHub server.
     *
     * @return The count of successfully deleted labels or [NO_RESULT] if there were no labels to
     * be deleted.
     */
    fun deleteAllLabels(): Int {
        var result = NO_RESULT
        val response = api.getLabels().execute()
        if (response.isSuccessful) {
            val labels = response.body()
            if (labels != null && labels.isNotEmpty()) {
                result = 0
                val repository = api.getRepository()
                project.logger.info("Deleting all issue labels for '${repository.path()}' ...")
                labels.forEach {
                    val deleteResponse = api.deleteLabel(it).execute()
                    when (deleteResponse.isSuccessful) {
                        true -> {
                            result++
                            project.logger.debug("Label '${it.name}' successfully deleted for '$${repository.path()}'!")
                        }
                        false -> project.logger.error("Failed to delete label '${it.name}' for '${repository.path()}'!")
                    }
                }
            }
        }
        return result
    }

    /*
     * Inner classes ===============================================================================
     */
}
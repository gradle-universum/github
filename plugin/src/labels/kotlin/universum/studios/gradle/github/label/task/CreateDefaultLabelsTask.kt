/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.task

import universum.studios.gradle.github.label.data.model.Label
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * A [LabelsTask] implementation which creates a collection of default/predefined labels on the GitHub
 * server for the specified [repository].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class CreateDefaultLabelsTask : LabelsTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Specification for [CreateDefaultLabelsTask].
         */
        val SPECIFICATION = TaskSpecification(
                type = CreateDefaultLabelsTask::class.java,
                name = "githubCreateDefaultLabels",
                description = "Creates default (predefined) issue labels for the project's GitHub repository."
        )

        /**
         * Collection of deafult labels to be created by this labels task implementation.
         */
        internal val LABELS = listOf(
                Label(name = "bug", color = "f44336"),
                Label(name = "documentation", color = "03a9f4"),
                Label(name = "duplicate", color = "ff9800"),
                Label(name = "enhancement", color = "4caf50"),
                Label(name = "feature-request", color = "009688"),
                Label(name = "invalid", color = "e6e6e6"),
                Label(name = "performance", color = "f44336"),
                Label(name = "question", color = "ffc107"),
                Label(name = "release", color = "4caf50"),
                Label(name = "wontfix", color = "ffffff")
        )
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPerform() {
        val repository = getRepository()
        project.logger.info("Creating default labels for '${repository.path()}' ...")
        val result = getLabelManager().createLabels(LABELS)
        project.logger.quiet("Successfully created default labels for '${repository.path()}' in count '$result'.")
    }

    /*
     * Inner classes ===============================================================================
     */
}
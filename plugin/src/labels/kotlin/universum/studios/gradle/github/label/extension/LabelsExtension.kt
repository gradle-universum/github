/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.extension

import groovy.lang.Closure
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.Task
import universum.studios.gradle.github.extension.BaseProjectExtension
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.label.task.CreateDefaultLabelsTask
import universum.studios.gradle.github.label.task.CreateLabelsTask
import universum.studios.gradle.github.label.task.DeleteLabelsTask
import universum.studios.gradle.github.label.task.LabelsTask
import universum.studios.gradle.github.task.TaskManager
import universum.studios.gradle.github.util.PluginException
import java.io.File
import java.io.FileInputStream
import java.util.*

/**
 * An extension which is responsible for creation and configuration of tasks that may be used to
 * manage issue labels for a specific project hosted on **GitHub**.
 *
 * This extension provides only 'static' tasks that do not depend on any plugin configuration of
 * *labels* closure, except the collection of *labels.items* collection which just specifies which
 * labels should be created on the **GitHub** server.
 *
 * See [GitHub Issue Labels API][https://developer.github.com/v3/issues/labels/] documentation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of LabelsExtension.
 */
open class LabelsExtension : BaseProjectExtension() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object Contract {

        /**
         * Name of the closure block defining properties for [LabelsExtension].
         */
        const val NAME = "labels"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Manager that is used to create and configure tasks provided by this extension.
     */
    private lateinit var taskManager: TaskManager

    /**
     * Extension containing label items.
     */
    private lateinit var items: NamedDomainObjectContainer<LabelItemExtension>

    /**
     * All tasks that has been created by this extension.
     */
    private val tasks = hashSetOf<Task>()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPluginApplied(project: Project) {
        super.onPluginApplied(project)
        this.taskManager = TaskManager(project)
        this.taskManager.registerTaskConfigurator(CreateDefaultLabelsTask.SPECIFICATION, LabelsTask.BasicConfigurator())
        this.taskManager.registerTaskConfigurator(CreateLabelsTask.SPECIFICATION, LabelsTask.BasicConfigurator())
        this.taskManager.registerTaskConfigurator(DeleteLabelsTask.SPECIFICATION, LabelsTask.BasicConfigurator())
        this.items = project.container(LabelItemExtension::class.java)
    }

    /*
     */
    override fun onRepositoryChanged(repository: RepositoryExtension) {
        super.onRepositoryChanged(repository)
        this.taskManager.setRepository(repository)
        // Re-configure tasks due to changed repository.
        if (isProjectEvaluated()) {
            configureTasks()
        }
    }

    /*
     */
    override fun onDefaultConfigChanged(defaultConfig: DefaultConfigExtension) {
        super.onDefaultConfigChanged(defaultConfig)
        this.taskManager.setDefaultConfig(defaultConfig)
        // Re-configure tasks due to changed default configuration.
        if (isProjectEvaluated()) {
            configureTasks()
        }
    }

    /*
     */
    override fun onProjectEvaluated(project: Project) {
        super.onProjectEvaluated(project)
        this.createTasks()
        this.configureTasks()
    }

    /**
     * Creates all tasks provided by this extension.
     */
    private fun createTasks() {
        // Create static tasks.
        this.tasks.add(taskManager.createTask(CreateDefaultLabelsTask.SPECIFICATION))
        this.tasks.add(taskManager.createTask(CreateLabelsTask.SPECIFICATION))
        this.taskManager.registerTaskConfigurator(CreateLabelsTask.SPECIFICATION, CreateLabelsTask.Configurator(items))
        this.tasks.add(taskManager.createTask(DeleteLabelsTask.SPECIFICATION))
    }

    /**
     * Performs configuration for the tasks created by this extension.
     */
    private fun configureTasks() {
        this.tasks.forEach { taskManager.configureTask(it) }
    }

    /**
     * Specifies a path to the properties file containing collection of label items.
     *
     * @param filePath Path to the properties file with label items.
     */
    fun itemsFile(filePath: String) {
        itemsFile(File(filePath))
    }

    /**
     * Specifies a file containing collection of label items.
     *
     * @param file Properties file with label items.
     */
    fun itemsFile(file: File) {
        if (file.exists()) {
            val properties = Properties()
            properties.load(FileInputStream(file))
            properties.forEach {
                this.items.add(LabelItemExtension(it.key as String).apply { color(it.value as String) })
            }
        } else {
            throw PluginException("Items file at path '${file.path}' does not exist!")
        }
    }

    /**
     * Configures the label items of this extension.
     *
     * @param configureClosure The closure defining label items.
     */
    fun items(configureClosure: Closure<Any>) {
        this.items.configure(configureClosure)
    }

    /*
     * Inner classes ===============================================================================
     */
}
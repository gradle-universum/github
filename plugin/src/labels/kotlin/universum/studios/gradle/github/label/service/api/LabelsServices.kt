/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.api

import retrofit2.Call
import retrofit2.http.*
import universum.studios.gradle.github.label.service.model.RemoteLabel

/**
 * Declaration of services for access to the GitHub Labels API.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
internal interface LabelsServices {

    /**
     * Creates a call with request to GET all issue labels for the specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository of which labels to obtain.
     * @return Call ready to be executed.
     */
    @GET("repos/{owner}/{repo}/labels")
    fun getLabels(
            @Path("owner") owner: String,
            @Path("repo") repo: String
    ): Call<List<RemoteLabel>>

    /**
     * Creates a call with request to GET a single issue label with a desired *name* for the
     * specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository of which labels to obtain.
     * @param name Name of the desired label to be obtained.
     * @return Call ready to be executed.
     */
    @GET("repos/{owner}/{repo}/labels/{name}")
    fun getNamedLabel(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("name") name: String
    ): Call<RemoteLabel>

    /**
     * Creates a call with request to POST a single issue label for the specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository for which to post the label.
     * @param label The desired label to be posted.
     * @return Call ready to be executed.
     */
    @POST("repos/{owner}/{repo}/labels")
    fun postLabel(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Body label: RemoteLabel
    ): Call<RemoteLabel>

    /**
     * Creates a call with request to DELETE a single issue label with a desired *name* for the
     * specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository for which to delete the label.
     * @param name Name of the desired label to be deleted.
     * @return Call ready to be executed.
     */
    @DELETE("repos/{owner}/{repo}/labels/{name}")
    fun deleteLabel(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("name") name: String
    ): Call<Void>
}
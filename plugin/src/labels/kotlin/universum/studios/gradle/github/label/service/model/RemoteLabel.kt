/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.service.model

/**
 * RemoteAsset contains properties describing issue label that the GitHub server may return or receive.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property id Unique id of the label.
 * @property url Url of the label.
 * @property name Name of the label.
 * @property color Color representing the label graphically.
 * @property default Boolean flag indicating whether the label is a default one.
 * @constructor Creates a new instance of RemoteLabel with the specified properties.
 */
data class RemoteLabel(
        val id: Int = 0,
        var url: String? = null,
        var name: String = "",
        var color: String? = null,
        var default: Boolean? = null
)
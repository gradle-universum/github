/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.label.data.model

/**
 * Simple model containing properties describing issue label used locally by the GitHub plugin.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property id Id of the label.
 * @property name Name of the label.
 * @property color Color of the label in hexadecimal format without *#* symbol.
 * @constructor Creates a new instance of Label with the specified property values.
 */
data class Label(
        val id: Int = 0,
        val name: String,
        val color: String
)
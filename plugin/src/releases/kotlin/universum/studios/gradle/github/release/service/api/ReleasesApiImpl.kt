/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.api

import okhttp3.RequestBody
import retrofit2.Call
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.release.service.model.RemoteAsset
import universum.studios.gradle.github.release.service.model.RemoteRelease
import universum.studios.gradle.github.service.api.BaseApi

/**
 * Implementation of [ReleasesApi].
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param repository The repository for which may be performed service calls through the api.
 * @constructor Creates a new instance of ReleasesApiImpl.
 */
internal class ReleasesApiImpl(repository: RepositoryExtension) : BaseApi<ReleasesServices>(repository, ReleasesServices::class.java), ReleasesApi {

    /*
     */
    override fun getReleases() = services.getReleases(
            repository.owner,
            repository.name
    )

    /*
     */
    override fun getRelease(id: Int): Call<RemoteRelease> = services.getRelease(
            repository.owner,
            repository.name,
            id
    )

    /*
     */
    override fun getLatestRelease() = services.getLatestRelease(
            repository.owner,
            repository.name
    )

    /*
     */
    override fun getTaggedRelease(tag: String) = services.getTaggedRelease(
            repository.owner,
            repository.name,
            tag
    )

    /*
     */
    override fun createRelease(release: RemoteRelease) = services.postRelease(
            repository.owner,
            repository.name,
            release
    )

    /*
     */
    override fun editRelease(release: RemoteRelease) = services.patchRelease(
            repository.owner,
            repository.name,
            release.id,
            release
    )

    /*
     */
    override fun deleteRelease(release: RemoteRelease) = services.deleteRelease(
            repository.owner,
            repository.name,
            release.id
    )

    /*
     */
    override fun uploadAsset(uploadUrl: String, asset: RequestBody, assetContentType: String) = services.postAsset(
            uploadUrl,
            asset,
            assetContentType
    )

    /*
     */
    override fun deleteAsset(asset: RemoteAsset) = services.deleteAsset(
            repository.owner,
            repository.name,
            asset.id
    )
}
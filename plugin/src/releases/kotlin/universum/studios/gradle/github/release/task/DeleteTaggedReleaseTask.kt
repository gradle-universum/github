/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import universum.studios.gradle.github.service.api.ApiCallException
import universum.studios.gradle.github.task.specification.TaskSpecification
import universum.studios.gradle.github.util.PluginException

/**
 * A [ListReleasesTask] implementation which may be used to delete a single GitHub release for the
 * specified [repository] and *tag* project property supplied as *-Ptag=TAG*.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class DeleteTaggedReleaseTask : ListReleasesTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Specification for [DeleteTaggedReleaseTask].
         */
        val SPECIFICATION: TaskSpecification<DeleteTaggedReleaseTask> = TaskSpecification(
                type = DeleteTaggedReleaseTask::class.java,
                name = "githubDeleteTaggedRelease",
                description = "Deletes a single release associated with a specific tag for the project's GitHub repository."
        )
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPerform() {
        val repository = getRepository()
        val releaseTag = project.property("tag") as String? ?: throw PluginException("No tag specified via '-Ptag=DESIRED_TAG'!")
        val remoteRelease = getExistingRelease(releaseTag)
        if (remoteRelease == null) {
            project.logger.warn("No such release with tag '$releaseTag' for '${repository.path()}' found to be deleted.")
            return
        }
        project.logger.info("Deleting single release with tag '$releaseTag' for '${repository.path()}' ...")
        val response = getApi().deleteRelease(remoteRelease).execute()
        when (response.isSuccessful) {
            true ->  project.logger.quiet("Release with tag '$releaseTag' for '${repository.path()}' has been successfully deleted.")
            false -> throw ApiCallException(response)
        }
    }

    /*
     * Inner classes ===============================================================================
     */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.api

import okhttp3.RequestBody
import retrofit2.Call
import universum.studios.gradle.github.release.service.model.RemoteAsset
import universum.studios.gradle.github.release.service.model.RemoteRelease
import universum.studios.gradle.github.service.api.Api

/**
 * Interface declaring layer that may be used to perform calls upon the GitHub Releases API.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface ReleasesApi : Api {

    /**
     * Creates a call for request to GET all releases for repository specified for this api.
     *
     * @return Call ready to be executed.
     */
    fun getReleases(): Call<List<RemoteRelease>>

    /**
     * Creates a call with request to GET a single release with a desired *id* for repository
     * specified for this api.
     *
     * @param id The id for which to obtain its associated release.
     * @return Call ready to be executed.
     */
    fun getRelease(id: Int): Call<RemoteRelease>

    /**
     * Creates a call with request to GET the latest release for repository specified for this api.
     *
     * @return Call ready to be executed.
     */
    fun getLatestRelease(): Call<RemoteRelease>

    /**
     * Creates a call with request to GET a single release with a desired *tag* for repository
     * specified for this api.
     *
     * @param tag The tag for which to obtain its associated release.
     * @return Call ready to be executed.
     */
    fun getTaggedRelease(tag: String): Call<RemoteRelease>

    /**
     * Creates a call with request to CREATE a single release for repository specified for this api.
     *
     * @param release The desired release to be created.
     * @return Call ready to be executed.
     */
    fun createRelease(release: RemoteRelease): Call<RemoteRelease>

    /**
     * Creates a call with request to EDIT a single release for repository specified for this api.
     *
     * @param release The desired release to be edited.
     * @return Call ready to be executed.
     */
    fun editRelease(release: RemoteRelease): Call<RemoteRelease>

    /**
     * Creates a call with request to DELETE a single release for repository specified for this api.
     *
     * @param release The desired release to be deleted.
     * @return Call ready to be executed.
     */
    fun deleteRelease(release: RemoteRelease): Call<Void>

    /**
     * Creates a call with request to UPLOAD a single asset.
     *
     * @param uploadUrl The url where to upload the asset.
     * @param asset The desired asset body to be uploaded.
     * @param assetContentType Type of the content that the asset represents.
     * @return Call ready to be executed.
     */
    fun uploadAsset(
            uploadUrl: String,
            asset: RequestBody,
            assetContentType: String
    ): Call<RemoteAsset>

    /**
     * Creates a call with request to DELETE a single asset for repository specified for this api.
     *
     * @param asset The desired asset to be deleted.
     * @return Call ready to be executed.
     */
    fun deleteAsset(asset: RemoteAsset): Call<Void>
}
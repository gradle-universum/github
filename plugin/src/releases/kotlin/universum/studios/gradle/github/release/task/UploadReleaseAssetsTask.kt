/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import org.gradle.api.artifacts.PublishArtifact
import universum.studios.gradle.github.release.data.model.Asset
import universum.studios.gradle.github.release.data.model.ReleaseModelBinders
import universum.studios.gradle.github.release.data.model.ReleaseModelMappers
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension
import universum.studios.gradle.github.release.manage.AssetManager
import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.service.UrlUtils
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * A [ReleaseTask] implementation which may be used to upload all assets of an existing GitHub release
 * depending on the specified [ReleaseTypeExtension] configuration for the specified [repository].
 *
 * **Note that all of previous assets of an existing release are first deleted, and then, the new
 * ones are uploaded.**
 *
 * @author Martin Albedinsky
 * @since 1.2
 */
open class UploadReleaseAssetsTask : ReleaseTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Creates a new TaskSpecification for the given *releaseType*.
         *
         * @param releaseType Extension that defines the release type for which to create the desired
         * task specification.
         * @return Specification that may be used to create task that uploads all assets associated
         * with a specific release up to the GitHub cloud.
         */
        fun createSpecification(releaseType: ReleaseTypeExtension) = TaskSpecification(
                type = UploadReleaseAssetsTask::class.java,
                name = "githubUpload${releaseType.name.capitalize()}ReleaseAssets",
                description = "Uploads (updates) assets associated with '${releaseType.name}' release for the project's GitHub repository."
        )

        /**
         * Gathers all assets specified for this task to be uploaded for the specified *remoteRelease*.
         *
         * @param releaseType The desired type for which to create artifacts to be uploaded. Relevant
         * properties of the type are used when building name for each of assets. May be * `null` in
         * which case properties of respective publish artifacts will be used instead.
         * @param releaseAssetArtifacts Collection of publish artifacts for which to create assets
         * to be uploaded.
         * @param uploadUrl Url where are the assets to be uploaded.
         * @return Collection of assets ready to be uploaded. May be empty if the given `releaseAssetArtifacts`
         * are also empty.
         */
        internal fun createAssetsForUpload(releaseType: ReleaseTypeExtension?, releaseAssetArtifacts: Collection<PublishArtifact>, uploadUrl: String? = ""): List<Asset> {
            return if (releaseAssetArtifacts.isEmpty()) {
                emptyList()
            } else {
                releaseAssetArtifacts.map {
                    ReleaseModelMappers.PUBLISH_ARTIFACT_TO_ASSET.map(it).copy(
                            name = Asset.NameBuilder()
                                    .name(releaseType?.artifactId() ?: it.name)
                                    .version(releaseType?.artifactVersion() ?: releaseType?.releaseName())
                                    .classifier(it.classifier)
                                    .type(it.type)
                                    .build(),
                            baseUploadUrl = UrlUtils.parseBaseUrl(uploadUrl ?: "")
                    )
                }
            }
        }
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Manager used by this task to upload assets associated with a release up to the GitHub server.
     */
    private lateinit var assetManager: AssetManager

    /**
     * Type describing for which release to upload the assets.
     */
    var releaseType: ReleaseTypeExtension? = null

    /**
     * Set of asset artifacts to be uploaded.
     */
    internal var releaseAssetArtifacts: Collection<PublishArtifact> = emptyList()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onApiChanged(api: ReleasesApi) {
        super.onApiChanged(api)
        this.assetManager = AssetManager(project, api)
    }

    /*
     */
    override fun onValidateProperties(): PropertyValuesReport {
        val report = super.onValidateProperties()
        this.releaseType ?: report.missingValueFor("releaseType")
        val type = releaseType!!
        type.tagName() ?: report.missingValueFor("releaseType.tagName")
        return report
    }

    /*
     */
    override fun onPerform() {
        val type = releaseType!!
        if (releaseAssetArtifacts.isEmpty()) {
            project.logger.quiet("No assets specified for release '${type.releaseName()!!}' to be uploaded.")
            return
        }

        val repository = getRepository()
        val remoteRelease = getExistingRelease(type.tagName()!!)
        if (remoteRelease == null) {
            project.logger.error("No release '${type.releaseName()!!}' for '${repository.path()}' of which assets to upload found!")
        } else {
            val release = ReleaseModelBinders.RELEASE_TYPE_TO_RELEASE.bind(
                    from = type,
                    to = ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(remoteRelease)
            )
            project.logger.info("Uploading (updating) assets of '${release.name}' release for '${repository.path()}' ...")
            this.assetManager.deleteAllAssets(release)
            val assets = createAssetsForUpload(releaseType, releaseAssetArtifacts, remoteRelease.upload_url)
            this.assetManager.uploadAssets(release, assets)
            project.logger.quiet("Assets of release '${release.name}' for '${repository.path()}' has been successfully updated.")
        }
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [ReleaseTask.BasicConfigurator] implementation that may be used for configuration of
     * [UploadReleaseAssetsTask]s based on the supplied [ReleaseTypeExtension].
     *
     * @author Martin Albedinsky
     * @since 1.2
     *
     * @property releaseType Extension describing the release for which to upload its associated assets.
     * @property artifacts Collection of the asset artifacts to be uploaded.
     * @constructor Creates a new instance of Configurator with the specified *releaseType*.
     */
    class Configurator(
            private val releaseType: ReleaseTypeExtension,
            private val artifacts: Collection<PublishArtifact> = emptyList()
    ) : BasicConfigurator<UploadReleaseAssetsTask>() {

        /*
         */
        override fun configureTask(task: UploadReleaseAssetsTask): UploadReleaseAssetsTask {
            super.configureTask(task)
            task.releaseType = releaseType
            task.releaseAssetArtifacts = artifacts
            return task
        }
    }
}
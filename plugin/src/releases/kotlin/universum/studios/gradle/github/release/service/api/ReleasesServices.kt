/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.api

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import universum.studios.gradle.github.release.service.model.RemoteAsset
import universum.studios.gradle.github.release.service.model.RemoteRelease

/**
 * Declaration of services for access to the GitHub Releases API.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
internal interface ReleasesServices {

    /**
     * Creates a call with request to GET all releases for the specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository of which releases to obtain.
     * @return Call ready to be executed.
     */
    @GET("repos/{owner}/{repo}/releases")
    fun getReleases(
            @Path("owner") owner: String,
            @Path("repo") repo: String
    ): Call<List<RemoteRelease>>

    /**
     * Creates a call with request to GET a single release with a desired *id* for the specified
     * *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository of which release to obtain.
     * @param id Id for which to obtain its associated release.
     * @return Call ready to be executed.
     */
    @GET("repos/{owner}/{repo}/releases/{id}")
    fun getRelease(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("id") id: Int
    ): Call<RemoteRelease>

    /**
     * Creates a call with request to GET the latest release for the specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository of which release to obtain.
     * @return Call ready to be executed.
     */
    @GET("repos/{owner}/{repo}/releases/latest")
    fun getLatestRelease(
            @Path("owner") owner: String,
            @Path("repo") repo: String
    ): Call<RemoteRelease>

    /**
     * Creates a call with request to GET a single release with a desired *tag* for the specified
     * *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository of which release to obtain.
     * @param tag Tag for which to obtain its associated release.
     * @return Call ready to be executed.
     */
    @GET("repos/{owner}/{repo}/releases/tags/{tag}")
    fun getTaggedRelease(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("tag") tag: String
    ): Call<RemoteRelease>

    /**
     * Creates a call with request to POST a single release for the specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository for which to post the release.
     * @param release The release to be created.
     * @return Call ready to be executed.
     */
    @POST("repos/{owner}/{repo}/releases")
    fun postRelease(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Body release: RemoteRelease
    ): Call<RemoteRelease>

    /**
     * Creates a call with request to PATCH a single release for the specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository for which to patch the release.
     * @param releaseId Id of the release to update.
     * @param release Release to be updated.
     * @return Call ready to be executed.
     */
    @PATCH("repos/{owner}/{repo}/releases/{id}")
    fun patchRelease(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("id") releaseId: Int,
            @Body release: RemoteRelease
    ): Call<RemoteRelease>

    /**
     * Creates a call with request to DELETE a single release with the specified *id* for the
     * specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository for which to delete the release.
     * @param id Id of the release to be deleted.
     * @return Call ready to be executed.
     */
    @DELETE("repos/{owner}/{repo}/releases/{id}")
    fun deleteRelease(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("id") id: Int
    ): Call<Void>

    /**
     * Creates a call with request to POST a single asset for release with the specified *releaseId*
     * for the specified *owner/repository*.
     *
     * @param uploadUrl The url where to upload the asset.
     * @param asset The desired asset to be posted.
     * @param contentType Type of the content represented by the asset.
     * @return Call ready to be executed.
     */
    @POST
    fun postAsset(
            @Url uploadUrl: String,
            @Body asset: RequestBody,
            @Header("Content-Type") contentType: String
    ): Call<RemoteAsset>

    /**
     * Creates a call with request to DELETE a single asset with the specified *assetId* for the
     * specified *owner/repository*.
     *
     * @param owner The owner (username or organization name) of the repository.
     * @param repo Name of the repository for which to delete the asset.
     * @param id Id of the asset to be deleted.
     * @return Call ready to be executed.
     */
    @DELETE("repos/{owner}/{repo}/releases/assets/{id}")
    fun deleteAsset(
            @Path("owner") owner: String,
            @Path("repo") repo: String,
            @Path("id") id: Int
    ): Call<Void>
}
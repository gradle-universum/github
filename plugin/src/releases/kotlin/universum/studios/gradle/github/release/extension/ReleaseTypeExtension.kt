/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.extension

import groovy.lang.Closure
import org.gradle.api.NamedDomainObjectFactory
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.ArtifactHandler
import org.gradle.util.ConfigureUtil

/**
 * Extension used to define a desired release type.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param name Name of the release type.
 * @constructor Creates a new instance of ReleaseTypeExtension with the specified *name*.
 */
open class ReleaseTypeExtension(val name: String) {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Name of the tag to associate release with.
     */
    private var tagName: String? = null

    /**
     * Target branch/commit to associated release with.
     */
    private var targetCommitish: String? = null

    /**
     * Name of the release.
     */
    private var releaseName: String? = null

    /**
     * Body describing the release.
     */
    private var releaseBody: String? = null

    /**
     * Boolean flag indicating whether the release is a draft.
     */
    private var draft: Boolean? = null

    /**
     * Boolean flag indicating whether the release is a pre-release.
     */
    private var preRelease: Boolean? = null

    /**
     * Boolean flag indicating whether the release may be overridden.
     */
    private var overwrite: Boolean? = null

    /**
     * Handler used to configure assets part of this extension.
     */
    private lateinit var artifactHandler: ArtifactHandler

    /**
     * Id for all artifacts to be uploaded as assets for this release type.
     */
    private var artifactId: String? = null

    /**
     * Version for all artifacts to be uploaded as assets for this release type.
     */
    private var artifactVersion: String? = null

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Specifies a tag name to associate the release with.
     *
     * @param name The desired name of the tag.
     * @return This extension to allow methods chaining.
     */
    fun tagName(name: String?): ReleaseTypeExtension {
        this.tagName = name
        return this
    }

    /**
     * Returns the tag name specified for this release type.
     *
     * @return This type's tag name.
     */
    fun tagName() = tagName

    /**
     * Specifies a branch/commit to associated the release with.
     *
     * @param commitish The desired name of the branch or commit.
     * @return This extension to allow methods chaining.
     */
    fun targetCommitish(commitish: String?): ReleaseTypeExtension {
        this.targetCommitish = commitish
        return this
    }

    /**
     * Returns the target branch/commit specified for this release type.
     *
     * @return This type's target commit-ish.
     */
    fun targetCommitish() = targetCommitish

    /**
     * Specifies a name for the release.
     *
     * @param name The desired release name.
     * @return This extension to allow methods chaining.
     */
    fun releaseName(name: String?): ReleaseTypeExtension {
        this.releaseName = name
        return this
    }

    /**
     * Returns the name text specified for this release type.
     *
     * @return This type's release name.
     */
    fun releaseName() = releaseName

    /**
     * Specifies a body text describing the release.
     *
     * @param body The desired release body.
     * @return This extension to allow methods chaining.
     */
    fun releaseBody(body: String?): ReleaseTypeExtension {
        this.releaseBody = body
        return this
    }

    /**
     * Returns the body text specified for this release type.
     *
     * @return This type's release body.
     */
    fun releaseBody() = releaseBody

    /**
     * Specifies a boolean flag indicating whether the release is a draft.
     *
     * @param draft *True* to mark the release as draft, *false* otherwise.
     * @return This extension to allow methods chaining.
     */
    fun draft(draft: Boolean?): ReleaseTypeExtension {
        this.draft = draft
        return this
    }

    /**
     * Returns the draft flag specified for this release type.
     *
     * @return This type's draft flag.
     */
    fun draft() = draft

    /**
     * Specifies a boolean flag indicating whether the release is a pre-release.
     *
     * @param preRelease *True* to mark the release as pre-release, *false* otherwise which means
     * that the release will be published.
     * @return This extension to allow methods chaining.
     */
    fun preRelease(preRelease: Boolean?): ReleaseTypeExtension {
        this.preRelease = preRelease
        return this
    }

    /**
     * Returns the pre-release flag specified for this release type.
     *
     * @return This type's pre-release flag.
     */
    fun preRelease() = preRelease

    /**
     * Specifies a boolean flag indicating whether the release may be overridden.
     *
     * @param overwrite *True* to overwrite the existing release with a new/updated one, *false* otherwise.
     * @return This extension to allow methods chaining.
     */
    fun overwrite(overwrite: Boolean?): ReleaseTypeExtension {
        this.overwrite = overwrite
        return this
    }

    /**
     * Returns the overwrite flag specified for this release type.
     *
     * @return This type's overwrite flag.
     */
    fun overwrite() = overwrite

    /**
     * Specifies a handler that should be used by this extension when configuring its assets part.
     *
     * @param handler The handler to be used.
     * @return This extension to allow methods chaining.
     */
    internal fun artifactHandler(handler: ArtifactHandler): ReleaseTypeExtension {
        this.artifactHandler = handler
        return this
    }

    /**
     * Configures the assets of this extension.
     *
     * @param configureClosure The closure defining configuration of desired assets.
     * @return This extension to allow methods chaining.
     */
    fun assets(configureClosure: Closure<Any>): ReleaseTypeExtension {
        ConfigureUtil.configure(configureClosure, artifactHandler)
        return this
    }

    /**
     * Specifies an id to be used for all artifact assets to be uploaded for this release type.
     *
     * @param id The desired artifact id. If empty the original name of the asset artifacts
     * will be used as theirs name.
     * @return This extension to allow methods chaining.
     */
    fun artifactId(id: String?): ReleaseTypeExtension {
        this.artifactId = id
        return this
    }

    /**
     * Returns the artifact id specified for this release type.
     *
     * @return This type's artifact id.
     */
    fun artifactId() = artifactId

    /**
     * Specifies a version name to be used for all artifact assets to be uploaded for this release type.
     *
     * @param version The desired artifact version. If empty the name specified via [releaseName]
     * will be used.
     * @return This extension to allow methods chaining.
     */
    fun artifactVersion(version: String?): ReleaseTypeExtension {
        this.artifactVersion = version
        return this
    }

    /**
     * Returns the artifact version specified for this release type.
     *
     * @return This type's artifact version.
     */
    fun artifactVersion() = artifactVersion

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [NamedDomainObjectFactory] implementation which may be used to create instances of [ReleaseTypeExtension].
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @property project The project to which is the parent plugin applied. It is used to create
     * additional configurations specific for created release types.
     * @constructor Creates a new instance of Factory with the specified *project*.
     */
    class Factory(val project: Project) : NamedDomainObjectFactory<ReleaseTypeExtension> {

        /*
         */
        override fun create(name: String?): ReleaseTypeExtension {
            val type = ReleaseTypeExtension(name ?: throw IllegalArgumentException("Cannot create ReleaseTypeExtension without name!"))
            project.configurations.create(ReleasesExtension.Configuration.releaseAsset(name))
            return type.artifactHandler(project.artifacts)
        }
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.release.service.model.RemoteRelease
import universum.studios.gradle.github.task.BaseTask
import universum.studios.gradle.github.task.BaseTaskConfigurator

/**
 * A [BaseTask] implementation that is used as base for all tasks provided by
 * [ReleasesExtension][universum.studios.gradle.github.release.extension.ReleasesExtension]
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class ReleaseTask : BaseTask() {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * API implementation that may be used by this task to perform requests to the GitHub Releases API.
     */
    private var api: ReleasesApi? = null

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Sets an API implementation which should be used by this task to perform requests to the
     * GitHub Releases API.
     *
     * @param api The API implementation to be used by this task.
     */
    internal fun setApi(api: ReleasesApi) {
        this.api = api
        onApiChanged(api)
    }

    /**
     * Invoked whenever the specified *api* has been changed for this task.
     *
     * @param api The api that has been attached to this task.
     */
    internal open fun onApiChanged(api: ReleasesApi) {
        // Inheritance hierarchies may perform here operations related to the changed api.
    }

    /**
     * Provides access to the implementation of [ReleasesApi] which may be used to perform requests
     * targeting the GitHub Releases API.
     *
     * @return The API implementation.
     * @throws IllegalStateException If there is no API attached to this task.
     */
    internal fun getApi(): ReleasesApi {
        return api ?: throw IllegalStateException("No API attached!")
    }

    /**
     * Returns an existing remote release for the specified *releaseTag*.
     *
     * @param releaseTag The tag for which to query existing release.
     * @return Remote release associated with the specified tag or *null* if no such remote release
     * has been found on the GitHub server.
     */
    internal fun getExistingRelease(releaseTag: String): RemoteRelease? {
        val response = getApi().getTaggedRelease(releaseTag).execute()
        return if (response.isSuccessful) response.body() else null
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [BaseTaskConfigurator] implementation that may be used for basic configuration of tasks that
     * extend from [ReleaseTask].
     *
     * This configurator along base configuration also attaches an implementation of [ReleasesApi]
     * to each task passed to [configureTask] so the task may perform requests to the GitHub Releases API.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BasicConfigurator.
     */
    open class BasicConfigurator<T : ReleaseTask> : BaseTaskConfigurator<T>() {

        /*
         */
        override fun configureTask(task: T): T {
            super.configureTask(task)
            task.setApi(getRepository().findApi(ReleasesApi::class) ?: throw IllegalStateException("No ReleasesApi found!"))
            return task
        }
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.extension

import groovy.lang.Closure
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.PublishArtifact
import org.gradle.api.artifacts.dsl.ArtifactHandler
import org.gradle.util.ConfigureUtil
import universum.studios.gradle.github.extension.BaseProjectExtension
import universum.studios.gradle.github.extension.DefaultConfigExtension
import universum.studios.gradle.github.extension.RepositoryExtension
import universum.studios.gradle.github.release.task.*
import universum.studios.gradle.github.task.TaskManager

/**
 * An extension which is responsible for creation and configuration of tasks that may be used to
 * manage releases for a specific project hosted on **GitHub**.
 *
 * This extension provides as 'static' tasks that do not depend on any plugin configuration of
 * *releases* closure so 'dynamic' tasks that are created mainly for *releases.types* closure which
 * defines configurations of a desired release types that may be uploaded to the **GitHub** server.
 *
 * See [GitHub Releases API][https://developer.github.com/v3/repos/releases/] documentation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ReleasesExtension.
 */
open class ReleasesExtension : BaseProjectExtension() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object Contract {

        /**
         * Name of the closure block defining properties for [ReleasesExtension].
         */
        const val NAME = "releases"
    }

    /**
     * Contains configuration used by the [ReleasesExtension].
     */
    class Configuration private constructor() {

        /**
         */
        companion object {

            /**
             * Configuration which may be used to mark global release assets.
             */
            const val RELEASE_ASSET = "githubReleaseAsset"

            /**
             * Format for configuration which may be used to mark release assets for a concrete release type.
             */
            const val RELEASE_ASSET_FORMAT = "github%sReleaseAsset"

            /**
             * Creates a new release asset configuration for release type with the specified
             * *releaseTypeName*.
             *
             * @param releaseTypeName Name of the release type for which to create configuration name.
             * @return Configuration name for the specified release type name.
             */
            internal fun releaseAsset(releaseTypeName: String) = String.format(RELEASE_ASSET_FORMAT, releaseTypeName.capitalize())
        }

        /**
         */
        init {
            // Not allowed to be instantiated publicly.
            throw UnsupportedOperationException()
        }
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Manager that is used to create and configure tasks provided by this extension.
     */
    private lateinit var taskManager: TaskManager

    /**
     * Handler used to configure assets part of this extension.
     */
    private lateinit var artifactHandler: ArtifactHandler

    /**
     * Extension containing configurations of release types.
     */
    private lateinit var types: NamedDomainObjectContainer<ReleaseTypeExtension>

    /**
     * All tasks that has been created by this extension.
     */
    private val tasks = hashSetOf<Task>()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPluginApplied(project: Project) {
        super.onPluginApplied(project)
        project.configurations.create(Configuration.RELEASE_ASSET)
        this.taskManager = TaskManager(project)
        this.taskManager.registerTaskConfigurator(ListAllReleasesTask.SPECIFICATION, ReleaseTask.BasicConfigurator())
        this.taskManager.registerTaskConfigurator(ListLatestReleaseTask.SPECIFICATION, ReleaseTask.BasicConfigurator())
        this.taskManager.registerTaskConfigurator(ListTaggedReleaseTask.SPECIFICATION, ReleaseTask.BasicConfigurator())
        this.taskManager.registerTaskConfigurator(DeleteTaggedReleaseTask.SPECIFICATION, ReleaseTask.BasicConfigurator())
        this.artifactHandler = project.artifacts
        this.types = project.container(ReleaseTypeExtension::class.java, ReleaseTypeExtension.Factory(project))
    }

    /*
     */
    override fun onRepositoryChanged(repository: RepositoryExtension) {
        super.onRepositoryChanged(repository)
        taskManager.setRepository(repository)
        // Re-configure tasks due to changed repository.
        if (isProjectEvaluated()) {
            configureTasks()
        }
    }

    /*
     */
    override fun onDefaultConfigChanged(defaultConfig: DefaultConfigExtension) {
        super.onDefaultConfigChanged(defaultConfig)
        this.taskManager.setDefaultConfig(defaultConfig)
        // Re-configure tasks due to changed default configuration.
        if (isProjectEvaluated()) {
            configureTasks()
        }
    }

    /*
     */
    override fun onProjectEvaluated(project: Project) {
        super.onProjectEvaluated(project)
        this.createTasks()
        this.configureTasks()
    }

    /**
     * Creates all tasks provided by this extension.
     */
    private fun createTasks() {
        val project = getProject()
        // Create static tasks.
        this.tasks.add(taskManager.createTask(ListAllReleasesTask.SPECIFICATION))
        this.tasks.add(taskManager.createTask(ListLatestReleaseTask.SPECIFICATION))
        this.tasks.add(taskManager.createTask(ListTaggedReleaseTask.SPECIFICATION))
        this.tasks.add(taskManager.createTask(DeleteTaggedReleaseTask.SPECIFICATION))
        // Create dynamic tasks for release types.
        if (types.isNotEmpty()) {
            val globalReleaseAssetArtifacts = project.configurations.filter { it.name == Configuration.RELEASE_ASSET }[0].artifacts
            this.types.forEach {
                val releaseTypeAssetArtifacts = findAssetArtifactsForRelease(project, it)
                val releaseAssetArtifacts = if (releaseTypeAssetArtifacts.isEmpty()) globalReleaseAssetArtifacts else releaseTypeAssetArtifacts

                // Create upload task along with its configurator:
                val uploadSpecification = UploadReleaseTask.createSpecification(it)
                this.taskManager.registerTaskConfigurator(
                        uploadSpecification,
                        UploadReleaseTask.Configurator(it, releaseAssetArtifacts).apply { setDefaultConfig(getDefaultConfig()) }
                )
                this.tasks.add(taskManager.createTask(uploadSpecification))

                // Create separate upload assets task along with its configurator:
                val uploadAssetsSpecification = UploadReleaseAssetsTask.createSpecification(it)
                this.taskManager.registerTaskConfigurator(
                        uploadAssetsSpecification,
                        UploadReleaseAssetsTask.Configurator(it, releaseAssetArtifacts).apply { setDefaultConfig(getDefaultConfig()) }
                )
                this.tasks.add(taskManager.createTask(uploadAssetsSpecification))
            }
        }
    }

    /**
     * Performs configuration for the tasks created by this extension.
     */
    private fun configureTasks() {
        this.tasks.forEach { taskManager.configureTask(it) }
    }

    /**
     * Finds all asset artifacts that are associated with configuration of the given *releaseType*.
     *
     * @param project The project used to access all configurations.
     * @param releaseType Release type for which to find its associated asset artifacts.
     * @return Collection of asset artifacts for the specified release type or empty list if there
     * are not asset artifacts specified.
     */
    private fun findAssetArtifactsForRelease(project: Project, releaseType: ReleaseTypeExtension): Collection<PublishArtifact> {
        return project.configurations.filter { it.name == Configuration.releaseAsset(releaseType.name) }[0].artifacts
    }

    /**
     * Configures the global assets of this extension.
     *
     * @param configureClosure The closure defining configuration of desired assets.
     */
    fun assets(configureClosure: Closure<Any>) {
        ConfigureUtil.configure(configureClosure, artifactHandler)
    }

    /**
     * Configures the release types of this extension.
     *
     * @param configureClosure The closure defining configuration of desired release types.
     */
    fun types(configureClosure: Closure<Any>) {
        this.types.configure(configureClosure)
        this.types.forEach { it.artifactId(it.artifactId() ?: getDefaultConfig().artifactId) }
        this.types.forEach { it.artifactVersion(it.artifactVersion() ?: getDefaultConfig().artifactVersion) }
    }

    /*
     * Inner classes ===============================================================================
     */
}
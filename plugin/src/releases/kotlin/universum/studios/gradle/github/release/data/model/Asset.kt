/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import org.gradle.api.artifacts.PublishArtifact
import java.io.File

/**
 * Simple model containing properties describing asset associated with a specific release used locally
 * by the GitHub plugin.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property id Id of the asset.
 * @property name Name of the asset.
 * @property file File that the asset represents.
 * @property fileContentType Type of the content that the asset file represents.
 * @property baseUploadUrl Base url that should be used to build full url for upload.
 * @constructor Creates a new instance of Asset with the specified property values.
 */
data class Asset(
        val id: Int = 0,
        val name: String,
        val file: File,
        val fileContentType: String = Asset.CONTENT_TYPE,
        val baseUploadUrl: String = Asset.BASE_UPLOAD_URL) {

    /**
     */
    companion object Contract {

        /**
         * Default content type of asset file.
         */
        const val CONTENT_TYPE = "application/octet-stream"

        /**
         * Default value for base upload url.
         */
        const val BASE_UPLOAD_URL = "https://uploads.github.com/"
    }

    /**
     * Builder which may be used to build a name for a specific [Asset] using properties of the
     * corresponding [PublishArtifact] and of the specified release name.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    class NameBuilder {

        /**
         *
         */
        private var name: String? = null

        /**
         *
         */
        private var version: String? = null

        /**
         *
         */
        private var classifier: String? = null

        /**
         *
         */
        private var type: String? = null

        /**
         * Specifies a name of asset of which full name to build.
         *
         * @param name The desired name.
         * @return This builder to allow methods chaining.
         */
        fun name(name: String?): NameBuilder {
            this.name = name
            return this
        }

        /**
         * Specifies a version name of asset of which full name to build.
         *
         * @param version The desired version name.
         * @return This builder to allow methods chaining.
         */
        fun version(version: String?) : NameBuilder {
            this.version = version
            return this
        }

        /**
         * Specifies a classifier of asset of which full name to build.
         *
         * @param classifier The desired classifier.
         * @return This builder to allow methods chaining.
         */
        fun classifier(classifier: String?): NameBuilder {
            this.classifier = classifier
            return this
        }

        /**
         * Specifies a type of asset of which full name to build.
         *
         * @param type The desired type.
         * @return This builder to allow methods chaining.
         */
        fun type(type: String?): NameBuilder {
            this.type = type
            return this
        }

        /**
         * Builds name property for [Asset] from the properties specified for this builder.
         *
         * @return The appropriate asset name.
         */
        fun build(): String {
            var assetName = name ?: return ""
            if (!version.isNullOrEmpty()) {
                assetName += "-$version"
            }
            if (!classifier.isNullOrEmpty()) {
                assetName += "-$classifier"
            }
            if (!type.isNullOrEmpty()) {
                assetName += ".$type"
            }
            return assetName
        }
    }
}
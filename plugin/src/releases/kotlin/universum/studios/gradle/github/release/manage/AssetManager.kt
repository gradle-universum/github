/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.manage

import okhttp3.RequestBody
import org.gradle.api.Project
import universum.studios.gradle.github.release.data.model.Asset
import universum.studios.gradle.github.release.data.model.Release
import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.release.service.model.RemoteAsset

/**
 * Manager which may be used for release assets management (uploading, editing, deleting).
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property project The project to which is the parent plugin applied.
 * @property api The api that should be used by the manager to perform calls to the GitHub Releases API.
 * @constructor Creates a new instance of AssetManager with the specified *api*.
 */
internal class AssetManager(val project: Project, val api: ReleasesApi) {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Constant used to identify no result.
         */
        const val NO_RESULT = -1
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Uploads all the given *assets* up to the GitHub server.
     *
     * @param release Release for which to upload the specified assets.
     * @param assets List of assets to be uploaded.
     * @return Count of the successfully uploaded assets.
     */
    fun uploadAssets(release: Release, assets: List<Asset>): Int {
        var result = 0
        if (assets.isNotEmpty()) assets.forEach {
            if (uploadAsset(release, it)) result++
        }
        return result
    }

    /**
     * Uploads the specified *asset* up to the GitHub server.
     *
     * @param release Release for which to upload the specified asset.
     * @param asset The desired asset to be uploaded.
     * @return *True* if the requested asset has been successfully uploaded, *false* otherwise.
     */
    fun uploadAsset(release: Release, asset: Asset): Boolean {
        val repository = api.getRepository()
        project.logger.info("Uploading asset '${asset.name}' for release '${release.name}' ...")
        val response = api.uploadAsset(String.format(RemoteAsset.UPLOAD_URL_FORMAT,
                asset.baseUploadUrl,
                repository.owner,
                repository.name,
                release.id,
                asset.name
        ), RequestBody.create(null, asset.file), asset.fileContentType).execute()
        when (response.isSuccessful) {
            true -> project.logger.quiet("Asset '${asset.name}' successfully uploaded for release '${release.name}'.")
            false -> project.logger.error("Failed to upload asset '${asset.name}' for release '${release.name}'!")
        }
        return response.isSuccessful
    }

    /**
     * Deletes all assets stored on the GitHub server associated with release that has the specified
     * *releaseId*.
     *
     * @param release Release of which assets to delete.
     * @return The count of successfully deleted assets or [NO_RESULT] if there were no assets to
     * be deleted.
     */
    fun deleteAllAssets(release: Release): Int {
        var result = NO_RESULT
        val response = api.getRelease(release.id).execute()
        if (response.isSuccessful) {
            val assets = response.body()?.assets
            if (assets != null && assets.isNotEmpty()) {
                result = 0
                project.logger.info("Deleting all assets for release '${release.name}' ...")
                assets.forEach {
                    val deleteResponse = api.deleteAsset(it).execute()
                    when (deleteResponse.isSuccessful) {
                        true -> {
                            result++
                            project.logger.debug("Old asset '${it.name}' successfully deleted for release '${release.name}'.")
                        }
                        false -> project.logger.error("Failed to delete asset '${it.name}' for release '${release.name}'!")
                    }
                }
            }
        }
        return result
    }

    /*
     * Inner classes ===============================================================================
     */
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import universum.studios.gradle.github.release.service.model.RemoteRelease
import universum.studios.gradle.github.task.output.TaskOutputCreator

/**
 * A [ReleaseTask] implementation which is used as base for all release task which request a set or
 * single [RemoteRelease]/-s from the GitHub server.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class ListReleasesTask : ReleaseTask() {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Default output creator which may be used by inheritance hierarchies of this task class for
     * creation of logging output for received [RemoteRelease]s.
     */
    private val outputCreator = OutputCreator()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /**
     * Returns the default output creator for this task.
     *
     * @return Default output creator.
     */
    internal fun getOutputCreator(): OutputCreator {
        this.outputCreator.clear()
        return outputCreator
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [TaskOutputCreator] implementation which may be used by inheritance hierarchies of
     * [ListReleasesTask] to create logging output for received [RemoteRelease]s.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    internal open class OutputCreator : TaskOutputCreator {

        /**
         * List containing all releases added into this creator is its input.
         */
        private val releases = mutableListOf<RemoteRelease>()

        /**
         * Adds all the given *releases* as input into this creator.
         *
         * @param releases The desired list of releases to add as input.
         */
        fun addReleases(releases: List<RemoteRelease>) {
            this.releases.addAll(releases)
        }

        /**
         * Adds the given *release* as input into this creator.
         *
         * @param release The desired release to add as input.
         */
        fun addRelease(release: RemoteRelease) {
            this.releases.add(release)
        }

        /*
         */
        override fun clear() {
            this.releases.clear()
        }

        /*
         */
        override fun createOutput(): String {
            var output = ""
            val releasesIterator = releases.iterator()
            while (releasesIterator.hasNext()) {
                output += createReleaseOutput(releasesIterator.next())
                if (releasesIterator.hasNext()) {
                    output += ",\n"
                }
            }
            return output
        }

        /**
         * Creates an output string for the specified *release*.
         *
         * @param release The release for which to create output string.
         * @return The output string representing the given release.
         */
        fun createReleaseOutput(release: RemoteRelease) =
                "Release{\n" +
                "    tagName: ${release.tag_name},\n" +
                "    name: ${release.name},\n" +
                "    body: ${release.body},\n" +
                "    draft: ${release.draft},\n" +
                "    preRelease: ${release.prerelease},\n" +
                "    createdAt: ${release.created_at},\n" +
                "    publishedAt: ${release.published_at}\n" +
                "}"
    }
}
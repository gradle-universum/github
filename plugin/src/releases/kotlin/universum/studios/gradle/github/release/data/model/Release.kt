/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

/**
 * Simple model containing properties describing release used locally by the GitHub plugin.
 *
 * @property id Id of the release.
 * @property tagName Name of the tag to associate release with.
 * @property targetCommitish Name of the branch/commit to associate release with.
 * @property name Name for the release.
 * @property body Body for the release.
 * @property draft Boolean flag indicating whether the release is a draft.
 * @property preRelease Boolean flag indicating whether the release is a pre-release.
 * @property overwrite Boolean flag indicating whether this release may be overwritten.
 * @constructor Creates a new instance of Release with the specified property values.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
data class Release(
        val id: Int = 0,
        val tagName: String,
        val targetCommitish: String? = Release.TARGET_COMMITISH,
        val name: String? = "",
        val body: String? = "",
        val draft: Boolean = Release.DRAFT,
        val preRelease: Boolean = Release.PRE_RELEASE,
        val overwrite: Boolean = Release.OVERWRITE) {

    /**
     */
    companion object Contract {

        /**
         * Default value for [targetCommitish] property.
         */
        const val TARGET_COMMITISH = "master"

        /**
         * Default value for [draft] property.
         */
        const val DRAFT = false

        /**
         * Default value for [preRelease] property.
         */
        const val PRE_RELEASE = false

        /**
         * Default value for [overwrite] property.
         */
        const val OVERWRITE = false
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import universum.studios.gradle.github.data.model.ModelBinder
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension

/**
 * Static repository for [ModelBinder] implementations used across the plugin for purpose of
 * **release** extension.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object ReleaseModelBinders{

    /**
     * A [ModelBinder] implementation which may be used to bind data of [ReleaseTypeExtension] model
     * to [Release] model.
     */
    val RELEASE_TYPE_TO_RELEASE = object : ModelBinder<ReleaseTypeExtension, Release> {

        /*
         */
        override fun bind(from: ReleaseTypeExtension, to: Release) = to.copy(
                tagName = from.tagName() ?: to.tagName,
                targetCommitish = from.targetCommitish() ?: to.targetCommitish,
                name = from.releaseName() ?: to.name,
                body = from.releaseBody() ?: to.body,
                draft = from.draft() ?: to.draft,
                preRelease = from.preRelease() ?: to.preRelease,
                overwrite = from.overwrite() ?: to.overwrite
        )
    }
}
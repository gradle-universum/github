/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import universum.studios.gradle.github.service.api.ApiCallException
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * A [ListReleasesTask] implementation which outputs all GitHub releases for the specified
 * [repository].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class ListAllReleasesTask : ListReleasesTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Specification for [ListAllReleasesTask].
         */
        val SPECIFICATION = TaskSpecification(
                type = ListAllReleasesTask::class.java,
                name = "githubListAllReleases",
                description = "Lists all releases for the project's GitHub repository."
        )
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onPerform() {
        val repository = getRepository()
        project.logger.info("Fetching all releases for '${repository.path()}' ...")
        val call = getApi().getReleases()
        val response = call.execute()
        when (response.isSuccessful) {
            true -> {
                val responseBody = response.body()
                if (responseBody!!.isEmpty()) {
                    project.logger.quiet("No releases for '${repository.path()}' found.")
                } else {
                    val outputCreator = getOutputCreator()
                    outputCreator.addReleases(responseBody)
                    project.logger.quiet("Releases for '${repository.path()}':\n\n${outputCreator.createOutput()}")
                }
            }
            false -> throw ApiCallException(response)
        }
    }

    /*
     * Inner classes ===============================================================================
     */
}
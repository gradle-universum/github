/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.model

/**
 * RemoteAsset contains properties describing asset that the GitHub server may return or receive.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property id Unique id of the asset.
 * @property name Name of the asset.
 * @constructor Creates a new instance of RemoteAsset with the specified properties.
 */
data class RemoteAsset(val id: Int = 0, var name: String) {

    /**
     */
    companion object {

        /**
         * Format for url that may be used to upload a specific asset up to the GitHub server.
         *
         * Format: *:base_upload_url/repos/:owner/:repo/releases/:id/assets?name=:name*
         */
        const val UPLOAD_URL_FORMAT = "%srepos/%s/%s/releases/%d/assets?name=%s"
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.data.model

import org.gradle.api.artifacts.PublishArtifact
import universum.studios.gradle.github.data.model.ModelMapper
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension
import universum.studios.gradle.github.release.service.model.RemoteAsset
import universum.studios.gradle.github.release.service.model.RemoteRelease
import java.net.URLConnection

/**
 * Static repository for [ModelMapper] implementations used across the plugin for purpose of
 * **release** extension.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object ReleaseModelMappers {

    /**
     * Creates an exception that informs that value for the specified *argumentName* is missing.
     *
     * @return [IllegalArgumentException] ready to be thrown.
     */
    internal fun illegalArgumentValueException(argumentName: String) = IllegalArgumentException("Value for '$argumentName' is illegal or missing!")

    /**
     * A [ModelMapper] implementation which may be used to map [ReleaseTypeExtension] model to
     * [Release] model.
     */
    val RELEASE_TYPE_TO_RELEASE = object : ModelMapper<ReleaseTypeExtension, Release> {

        /*
         */
        override fun map(from: ReleaseTypeExtension) = Release(
                tagName = from.tagName() ?: throw illegalArgumentValueException("tagName"),
                targetCommitish = from.targetCommitish() ?: throw illegalArgumentValueException("targetCommitish"),
                name = from.releaseName() ?: throw illegalArgumentValueException("releaseName"),
                body = from.releaseBody(),
                draft = from.draft() ?: false,
                preRelease = from.preRelease() ?: false,
                overwrite = from.overwrite() ?: false
        )
    }

    /**
     * A [ModelMapper] implementation which may be used to map [Release] model to [RemoteRelease] model.
     */
    val RELEASE_TO_REMOTE_RELEASE = object : ModelMapper<Release, RemoteRelease> {

        /*
         */
        override fun map(from: Release) = RemoteRelease(from.id).apply {
            tag_name = from.tagName
            target_commitish = from.targetCommitish
            name = from.name
            body = from.body
            draft = from.draft
            prerelease = from.preRelease
        }
    }

    /**
     * A [ModelMapper] implementation which may be used to map [RemoteRelease] model to [Release] model.
     */
    val REMOTE_RELEASE_TO_RELEASE = object : ModelMapper<RemoteRelease, Release> {

        /*
         */
        override fun map(from: RemoteRelease) = Release(
                id = from.id,
                tagName = from.tag_name,
                targetCommitish = from.target_commitish ?: throw illegalArgumentValueException("target_commitish"),
                name = from.name ?: throw illegalArgumentValueException("name"),
                body = from.body ?: throw illegalArgumentValueException("body"),
                draft = from.draft ?: throw illegalArgumentValueException("draft"),
                preRelease = from.prerelease ?: throw illegalArgumentValueException("prerelease")
        )
    }

    /**
     * A [ModelMapper] implementation which may be used to map [PublishArtifact] model to [Asset] model.
     */
    val PUBLISH_ARTIFACT_TO_ASSET = object : ModelMapper<PublishArtifact, Asset> {

        /*
         */
        override fun map(from: PublishArtifact) = Asset(
                name = from.name,
                file = from.file,
                fileContentType = URLConnection.getFileNameMap().getContentTypeFor(from.file.name) ?: Asset.CONTENT_TYPE
        )
    }

    /**
     * A [ModelMapper] implementation which may be used to map [Asset] model to [RemoteAsset] model.
     */
    val ASSET_TO_REMOTE_ASSET = object : ModelMapper<Asset, RemoteAsset> {

        /*
         */
        override fun map(from: Asset) = RemoteAsset(id = from.id, name = from.name)
    }
}
/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.task

import org.gradle.api.artifacts.PublishArtifact
import universum.studios.gradle.github.release.data.model.Release
import universum.studios.gradle.github.release.data.model.ReleaseModelBinders
import universum.studios.gradle.github.release.data.model.ReleaseModelMappers
import universum.studios.gradle.github.release.extension.ReleaseTypeExtension
import universum.studios.gradle.github.release.manage.AssetManager
import universum.studios.gradle.github.release.service.api.ReleasesApi
import universum.studios.gradle.github.service.api.ApiCallException
import universum.studios.gradle.github.task.specification.TaskSpecification

/**
 * A [ReleaseTask] implementation which may be used to create a new GitHub release or to update an
 * existing one depending on the specified [ReleaseTypeExtension] configuration for the specified
 * [repository].
 *
 * **Note that if an existing release is to be updated (overwritten), all of its previous assets are
 * first deleted, and then, the new ones are uploaded.**
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
open class UploadReleaseTask : ReleaseTask() {

    /*
     * Companion ===================================================================================
     */

    /**
     */
    companion object {

        /**
         * Creates a new TaskSpecification for the given *releaseType*.
         *
         * @param releaseType Extension that defines the release type for which to create the desired
         * task specification.
         * @return Specification that may be used to create task that uploads a specific release up to
         * the GitHub cloud.
         */
        fun createSpecification(releaseType: ReleaseTypeExtension) = TaskSpecification(
                type = UploadReleaseTask::class.java,
                name = "githubUpload${releaseType.name.capitalize()}Release",
                description = "Uploads (creates or updates) '${releaseType.name}' release for the project's GitHub repository."
        )
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Manager used by this task to upload assets associated with a release to be created or updated
     * up to the GitHub server.
     */
    private lateinit var assetManager: AssetManager

    /**
     * Type describing which release to upload (create or update).
     */
    var releaseType: ReleaseTypeExtension? = null

    /**
     * Set of asset artifacts to be uploaded along with the release.
     */
    internal var releaseAssetArtifacts: Collection<PublishArtifact> = emptyList()

    /*
     * Constructors ================================================================================
     */

    /*
     * Methods =====================================================================================
     */

    /*
     */
    override fun onApiChanged(api: ReleasesApi) {
        super.onApiChanged(api)
        this.assetManager = AssetManager(project, api)
    }

    /*
     */
    override fun onValidateProperties(): PropertyValuesReport {
        val report = super.onValidateProperties()
        this.releaseType ?: report.missingValueFor("releaseType")
        val type = releaseType!!
        type.tagName() ?: report.missingValueFor("releaseType.tagName")
        type.releaseName() ?: report.missingValueFor("releaseType.releaseName")
        if (type.overwrite() == null) {
            type.overwrite(Release.OVERWRITE)
        }
        return report
    }

    /*
     */
    override fun onPerform() {
        val type = releaseType!!
        val existingRemoteRelease = getExistingRelease(type.tagName()!!)
        val releaseExists = existingRemoteRelease != null
        if (releaseExists && !type.overwrite()!!) {
            project.logger.warn("Release '${type.releaseName()}' already exists for '${getRepository().path()}'.")
            return
        }

        val release = ReleaseModelBinders.RELEASE_TYPE_TO_RELEASE.bind(
                from = type,
                to = if (releaseExists) ReleaseModelMappers.REMOTE_RELEASE_TO_RELEASE.map(existingRemoteRelease!!) else Release(tagName = type.tagName()!!)
        )
        if (releaseExists) onUpdateRelease(release) else onCreateRelease(release)
    }

    /**
     * Invoked to update an existing release according to the specified *release*.
     *
     * @param release The release describing how the exsiting release should be updated.
     */
    private fun onUpdateRelease(release: Release) {
        val repository = getRepository()
        project.logger.info("Uploading (updating) '${release.name}' release for '${repository.path()}' ...")
        val call = getApi().editRelease(ReleaseModelMappers.RELEASE_TO_REMOTE_RELEASE.map(release))
        val response = call.execute()
        when (response.isSuccessful) {
            true -> {
                val remoteRelease = response.body() ?: throw IllegalStateException("Received invalid response body!")
                this.assetManager.deleteAllAssets(release)
                val assets = UploadReleaseAssetsTask.createAssetsForUpload(releaseType, releaseAssetArtifacts, remoteRelease.upload_url)
                this.assetManager.uploadAssets(release, assets)
                project.logger.quiet("Release '${release.name}' for '${repository.path()}' has been successfully updated.")
            }
            false -> throw ApiCallException(response)
        }
    }

    /**
     * Invoked to create the specified *release* as a new release for the repository specified for
     * this task.
     *
     * @param release The release describing a new release to be created.
     */
    private fun onCreateRelease(release: Release) {
        val repository = getRepository()
        project.logger.info("Uploading (creating) '${release.name}' release for '${repository.path()}' ...")
        val call = getApi().createRelease(ReleaseModelMappers.RELEASE_TO_REMOTE_RELEASE.map(release))
        val response = call.execute()
        when (response.isSuccessful) {
            true -> {
                val remoteRelease = response.body() ?: throw IllegalStateException("Received invalid response body!")
                val assets = UploadReleaseAssetsTask.createAssetsForUpload(releaseType, releaseAssetArtifacts, remoteRelease.upload_url)
                this.assetManager.uploadAssets(release.copy(id = remoteRelease.id), assets)
                project.logger.quiet("Release '${release.name}' for '${repository.path()}' has been successfully created.")
            }
            false -> throw ApiCallException(response)
        }
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A [ReleaseTask.BasicConfigurator] implementation that may be used for configuration of
     * [UploadReleaseTask]s based on the supplied [ReleaseTypeExtension].
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @property releaseType Extension describing the release to be uploaded.
     * @property artifacts Collection of the asset artifacts to be uploaded along with the release.
     * @constructor Creates a new instance of Configurator with the specified *releaseType*.
     */
    class Configurator(
            private val releaseType: ReleaseTypeExtension,
            private val artifacts: Collection<PublishArtifact> = emptyList()
    ) : BasicConfigurator<UploadReleaseTask>() {

        /*
         */
        override fun configureTask(task: UploadReleaseTask): UploadReleaseTask {
            super.configureTask(task)
            task.releaseType = releaseType
            task.releaseAssetArtifacts = artifacts
            return task
        }
    }
}
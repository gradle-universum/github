/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.gradle.github.release.service.model

/**
 * RemoteRelease contains properties describing release that the GitHub server may return or receive.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @property id Unique id of the release.
 * @property tag_name Name of the tag the release is associated with.
 * @property target_commitish Name of the branch/commit the release is associated with.
 * @property name Name of the release.
 * @property body Body text describing the release.
 * @property draft Boolean flag indicating whether the release is a draft.
 * @property prerelease Boolean flag indicating whether the release is a pre-release.
 * @property created_at String representing date when the release has been created.
 * @property published_at String representing date when the release has been published.
 * @property upload_url Url which may be used to upload assets associated with the release.
 * @property assets Collection of assets associated with the release.
 * @constructor Creates a new instance of RemoteRelease with the specified properties.
 */
data class RemoteRelease(
        val id: Int = 0,
        var tag_name: String = "",
        var target_commitish: String? = null,
        var name: String? = null,
        var body: String? = null,
        var draft: Boolean? = null,
        var prerelease: Boolean? = null,
        var created_at: String? = null,
        var published_at: String? = null,
        var upload_url: String? = null,
        var assets: List<RemoteAsset>? = null
)
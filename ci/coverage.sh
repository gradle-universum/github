#!/usr/bin/env bash

./gradlew :plugin:createCoverageReport
./gradlew :plugin:uploadCoverageToCodacy
bash <(curl -s https://codecov.io/bash)
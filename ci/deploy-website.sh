#!/bin/bash

set -ex

# Setup script constants.
TEMP_DIR=temp
TEMP_DIR_WEBSITE=${TEMP_DIR}/website
IO_REPOSITORY="git@bitbucket.org:universum-studios/universum-studios.bitbucket.io.git"
PLUGIN_NAME=github
PLUGIN_ARTIFACT_NAME="${PLUGIN_NAME//_/-}-plugin"
PLUGIN_VERSION=1.3.0
PLUGIN_DIR_ARTIFACTS=../../artifacts/universum/studios/gradle/${PLUGIN_ARTIFACT_NAME}/${PLUGIN_VERSION}/
PLUGIN_JAVADOC_FILE_NAME="${PLUGIN_ARTIFACT_NAME}-${PLUGIN_VERSION}-javadoc.jar"
PLUGIN_DIR_TESTS=../../plugin/build/reports/tests/test/
PLUGIN_DIR_COVERAGE=../../plugin/build/reports/jacoco/
WEBSITE_FILES_VERSION="${PLUGIN_VERSION:0:1}".x
WEBSITE_DIR=gradle/plugin/${PLUGIN_NAME}
WEBSITE_DIR_REFERENCE=${WEBSITE_DIR}/reference/
WEBSITE_DIR_REFERENCE_VERSIONED=${WEBSITE_DIR_REFERENCE}${WEBSITE_FILES_VERSION}/
WEBSITE_DIR_TESTS=${WEBSITE_DIR}/tests/
WEBSITE_DIR_TESTS_VERSIONED=${WEBSITE_DIR_TESTS}${WEBSITE_FILES_VERSION}/
WEBSITE_DIR_COVERAGE=${WEBSITE_DIR}/coverage/
WEBSITE_DIR_COVERAGE_VERSIONED=${WEBSITE_DIR_COVERAGE}${WEBSITE_FILES_VERSION}/

# Delete left-over temporary directory (if exists).
rm -rf ${TEMP_DIR_WEBSITE}

#  Clone the current repo into temporary directory.
git clone --depth 1 ${IO_REPOSITORY} ${TEMP_DIR_WEBSITE}

# Move working directory into temporary directory.
mkdir -p ${TEMP_DIR_WEBSITE}/${WEBSITE_DIR}
cd ${TEMP_DIR_WEBSITE}

# Delete all files for the current version.
rm -rf ${WEBSITE_DIR_REFERENCE_VERSIONED}
rm -rf ${WEBSITE_DIR_TESTS_VERSIONED}
rm -rf ${WEBSITE_DIR_COVERAGE_VERSIONED}

# Copy files for documentation and reports for tests and coverage from the primary project module.
# Documentation:
mkdir -p ${WEBSITE_DIR_REFERENCE_VERSIONED}
cp ${PLUGIN_DIR_ARTIFACTS}${PLUGIN_JAVADOC_FILE_NAME} ${WEBSITE_DIR_REFERENCE_VERSIONED}${PLUGIN_JAVADOC_FILE_NAME}
unzip ${WEBSITE_DIR_REFERENCE_VERSIONED}${PLUGIN_JAVADOC_FILE_NAME} -d ${WEBSITE_DIR_REFERENCE_VERSIONED}
rm ${WEBSITE_DIR_REFERENCE_VERSIONED}${PLUGIN_JAVADOC_FILE_NAME}
# Tests report:
mkdir -p ${WEBSITE_DIR_TESTS_VERSIONED}
cp -R ${PLUGIN_DIR_TESTS}. ${WEBSITE_DIR_TESTS_VERSIONED}
# Coverage report:
mkdir -p ${WEBSITE_DIR_COVERAGE_VERSIONED}
cp -R ${PLUGIN_DIR_COVERAGE}. ${WEBSITE_DIR_COVERAGE_VERSIONED}

# Stage all files in git and create a commit.
git add . --all
git add -u
git commit -m "Website for ${PLUGIN_NAME} plugin at $(date)."

# Push the new website files up to the GitHub.
git push

# Delete temporary directory.
cd ../..
rm -rf ${TEMP_DIR}

# Keep the terminal open to indicate that everything was executed successfully.
echo "Website update finished successfully."
exec $SHELL
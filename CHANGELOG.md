Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.3.0](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 26.04.2020

- Update to use the latest stable **Gradle 6.3** and **Kotlin 1.3.72** versions.

### [1.2.1](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 09.09.2019

- Build with the latest stable **Gradle 5.6** and **Kotlin 1.3.50** versions.

### [1.2.0](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 02.07.2018

- Added new task `githubUploadReleaseAssets` for each configured release which may be used to
  upload/update only assets for that particular release.
- Small updates.

### [1.1.3](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 05.11.2017

- **Updated** to the latest **to date available** dependencies.

### [1.1.2](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 22.06.2017

- Fixed [Issue #26](https://github.com/universum-studios/gradle_github_plugin/issues/26).

### [1.1.1](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 22.06.2017

- Added `artifactVersion` property for *defaultConfig* and for *releaseType* clojure.

### [1.1.0](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 02.06.2017

- Added *RepositoryExtension* which may be configured via *repository* closure. This extension declares
  **owner**, **name** and **accessToken** properties that has been moved from the *DefaultConfigExtension*
  which is configured via *defaultConfig* closure. The default config extension now only declares
  **artifactId** property.

### [1.0.1](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 01.06.2016

- Allowed to specify **artifactId** property for *defaultConfig* closure and also for items defined
  in *releases.types* collection, which will be then used as name for all asset artifacts to 
  be uploaded along associated release.

### [1.0.0](https://bitbucket.org/gradle-universum/github/wiki/version/1.x) ###
> 31.05.2017

- First production release.